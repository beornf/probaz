/* Thunder head javascript */
var pause = 4400;
var slow = 1000;
var fast = 450;

function juiAlert(content) {
	$("<div><div class='middle'>" + content + "</div></div>").dialog({
		model: true,
		draggable: false,
		resizable: false,
		closeOnEscape: false,
		height: 170,
		title: "Support Notice",
		beforeClose: function (e, ui) {$("body").attr("id", "master")},
		buttons: {OK: function() {$(this).dialog("close")}},
	});
	$("body").attr("id", "alert");
}

function maxPhotos(element, max) {
	var checked = $(element).multiselect("widget").find("input:checked");
	var thumbs = $(element).closest(".input").find(".thumb");
	var pass = checked.length <= max || max == 0;
	if (pass && thumbs.length > 0) {
		checked.each(function(x) {photoRefresh(thumbs, x, this.value, this.title)});
		if (max) {
			for (var x = checked.length; x < max; x++) {
				thumbs.eq(x).addClass("hidden");
			}
		}
	}
	return pass;
}

function photoRefresh(thumbs, index, value, title) {
	thumb = thumbs.eq(index);
	large = index == 0;
	match = thumb.attr('class').match(/\d+/);
	if (match != value) {
		var name = "fid" + value;
		var prev = thumbs.filter("." + name);
		thumb.html(prev.length && prev.data("large") == large ? 
		prev.eq(0).html() : previewImage(value, title, large));
		thumb.addClass(name).removeClass("hidden");
		thumb.data("large", large);
		if (match) {
			thumb.removeClass("fid" + match);
		}
	} else {
		thumb.removeClass("hidden");
	}
}

function prependList(data, name, max) {
	var select = $("#system").find(".pick select");
	
	// append and load thumb
	imageLoad(function(src) {
		var multi = "#pick";
		var pick = $(multi).multiselect("widget").find("input:checked").length < max;
		$(multi).prepend('<option value="' + data.value + '" data-ext="' + data.ext + '" data-path="' 
			+ data.path + (pick ? '" selected="selected' : '') + '">' + data.display + '</option>');
		$(multi).multiselect("refresh");
		img = new Image();
		img.src = src;
		img.onload = function() {
			maxPhotos(multi, 3);
		}
	}, data.value, '1');
	
	// other thumb size
	imageLoad(function(src) {
		img = new Image();
		img.src = src;
	}, data.value, '0');
}

function wheel(e) {
	e = e || window.event;
	if (e.preventDefault) {
		e.preventDefault();
	}
	e.cancelBubble = true;
	e.returnValue = false;
}

function enableScroll() {
	if (window.removeEventListener) {
		window.removeEventListener('DOMMouseScroll', wheel, false);
	}
	window.onmousewheel = document.onmousewheel = null;
	$("body").css({"overflow": "inherit"});
}

function disableScroll(form, jui) {
	if (window.addEventListener) {
		window.addEventListener('DOMMouseScroll', wheel, false);
	}
	window.onmousewheel = document.onmousewheel = wheel;
	$("body").css({"overflow": "auto", "overflow-y": "hidden"});
}

function dateExpire(mysql, element, sig) {
	var time = String(mysql).split(/[- :]/);
	var until = new Date(time[0], time[1]-1, time[2], time[3], time[4], time[5]);
	$(element).countdown({until: until, significant: sig, description: 'left', expiryText: 'Ended', onExpiry: function() {if ($(this).hasClass('snippetDate')) {$.fn.yiiListView.update('snippets')}},
	serverSync: serverTime, layout: '{d<}{dn} {dl}{d>} {h<}{hn} {hl}{h>} {m<}{mn} {ml}{m>} {s<}{sn} {sl}{s>} {desc}'});
}

function imageLoad(func, file, large) {
	$.ajax({url: "file/view?rid=" + file + "&large=" + large, success: func});
}

function juiButton() {
	$(".juiButton").each(function() {
		var icon = "ui-icon-" + $(this).data("icon");
		$(this).button({
			icons: {primary: icon},
		});
	});
}

function cellMatch(element, model) {
	return model ? element.data("model").match(/\D+/) : element.data("model").match(/\d+/);
}

function rowNumber(element) {
	return $(element).attr("id").match(/\d+/);
}

function titleCase(text) {
	return text.charAt(0).toUpperCase() + text.substr(1);
}

function dialog(url) {
	var width = 675;
	var height = 361;
	var left = parseInt((screen.availWidth - width) / 2) - 25;
	var top = parseInt((screen.availHeight - height) / 2) - 10;
	var position = "width=" + width + ", height=" + height + 
	", left=" + left + ", top=" + top;
	window.open(url, "_blank", position);
}

// http://www.themaninblue.com/experiment/ResolutionLayout/
function getBrowserWidth() {
	if (window.innerWidth) {
		return window.innerWidth;
	} else if (document.documentElement && document.documentElement.clientWidth != 0) {
		return document.documentElement.clientWidth;
	} else if (document.body) {
		return document.body.clientWidth;
	}
	return 0;
}

function getBrowserHeight() {
	if (window.innerHeight) {
		return window.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight != 0) {
		return document.documentElement.clientHeight;
	} else if (document.body) {
		return document.body.clientHeight;
	}
	return 0;
}

function addEvent(obj, type, fn) {
	if (obj.addEventListener) {
		obj.addEventListener(type, fn, false);
	} else if (obj.attachEvent) {
		obj['e'+type+fn] = fn;
		obj[type+fn] = function() {obj['e'+type+fn](window.event)};
		obj.attachEvent('on'+type, obj[type+fn]);
	}
}

function dynamicLayout() {
	var wide = getBrowserWidth() > 1035;
	var type = 'wide'; var element = $(document.body);
	var found = element.hasClass(type);
	if (wide != found) {
		if (found) {
			element.removeClass(type);
		} else {
			element.addClass(type);
		}
		$.get("/session?" + type + "=" + (wide ? '1' : '0'));
	}
}

function popup(feed, href, text) {
	if (feed == "fb") {
		url = "http://www.facebook.com/sharer/sharer.php?u=";
	} else if (feed == "tw") {
		url = "http://twitter.com/share?text=" + text + "&url=";
	}
	var width = 432;
	var height = 326;
	var left = parseInt((screen.availWidth - width) / 2);
	var top = parseInt((screen.availHeight - height) / 2);
	window.open(url + href, "share", "width=" + width + ", height=" + height + 
	", left=" + left + ", top=" + top);
}

addEvent(window, 'load', dynamicLayout);
addEvent(window, 'resize', dynamicLayout);

/* Google analytics and Ajax */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-37212551-1']);
_gaq.push(['_trackPageview']);

(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

$.param.fragment.ajaxCrawlable(true);

$.ajaxSetup({
	cache: true,
	//cacheResponseTimer: 7*60*1000,
});
