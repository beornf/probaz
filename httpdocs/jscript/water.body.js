/* Water body javascript */
var restore = "site/list?agent=";
var crop = null;
var errorPause = null;
var fadeOut = 0.8;
var fadeRate = 'normal';
var fb = false;
var load = false;
var submit = false;
var thumbs = [];

function gridOpacity() {
	$("#grid ul li .cell, #grid .display img").opacityrollover({
		mouseOutOpacity: fadeOut,
		fadeSpeed: fadeRate,
		exemptionSelector: '.prime',
	});
}

$(".form").on("blur", "input", function() {
	setTimeout(showTip, 200, $(this));
});

$(".form").on("click", ".issue-tip", function(e) {
	e.stopPropagation();
	return false;
});

$(document).on("click", function() {
	var active = $(document.activeElement).filter(":input");
	var tip = active.length ? active.next() : null;
	$(".form").find(".issue-tip").not(tip).hide();
	if (active.filter(":submit").length) {
		submit = true;
		setTimeout(function() {
			submit = false;
		}, 200);
	}
	if (tip && tip.css("display") == "none") {
		var issue = tip.find(".issue");
		if (issue.css("display") != "none" && issue.text() != "") {
			tip.stop(0,1).fadeIn(fast);
		}
	}
});

function errorNode(error, tip, element) {
	if (error.text() != "" && error.css("display") != "none") {
		var active = element.is(document.activeElement) || submit;
		if (tip.css("display") == "none" && active) {
			tip.stop(0,1).fadeIn(fast);
		}
	} else {
		if (tip.css("display") != "none") {
			tip.stop(0,1).hide();
		}
	}
}

function hashAccount() {
	if ($("#unitBar .focus").length) {
		var hash = $("#unitBar .focus").attr("href");
		if ($.param.fragment().indexOf("my") == -1) {
			$.bbq.pushState(hash.substr(hash.indexOf("#")));
		}
	}
}

function hashPage(data, show) {
	var sys = $("#system"); hideMarket(1);
	var bar = $("#uploadBar").is(":visible");
	var page = data; var stop = '</script>';
	var sub = false;
	if (data.slice(-10, -1) != stop) {
		var pos = data.indexOf(stop) + 10;
		page = data.substr(0, pos);
		if ($("#uploadBar").length) {
			$("#uploadBar").html(data.slice(pos));
			if ($("#uploadBar").attr("class") == "hide") {
				$("#uploadBar").attr("class", "").hide();
			} else {
				sub = bar;
			}
		}
	} else if (bar) {
		hideSub();
	}
	sys.addClass("fixed"); $("#page").html(page).show();
	if (show) {
		sys.show(); $("#page").addClass("bold");
	} else {
		sys.hide(); $("#page").removeClass("bold");
	}
	if (sub) showSub();
}

function hideDesign() {
	$("#uploadBar").attr("class", "hide");
	$.get("site/session?upload=0");
	$(window).hashchange(); //crop.destroy();
}

function hideGrid() {
	window.location.hash = "#/my/design";
}

function hideSub() {
	$("#uploadBar").hide();
	$.get("site/session?upload=0");
}

function hideMarket(page) {
	if ($("#marketBar").length) {
		if (page == undefined) {
			if (load) $("#marketBar").show();
		} else if ($("#marketBar").is(":visible")) {
			$("#marketBar").hide();
		}
	}
	if (page == undefined && panel == 'b0') {
		if (load) $("#service").show();
	} else {
		$("#service").hide();
	}
	if (page == undefined && load && $("#uploadBar").is(":visible")) {
		hideSub();
	}
	if ($.param.fragment() == '/browse' && load) {
		$.scrollTo("#service", 600, {over: 0.835});
	}
	if (!load) load = true;
	if (page) scrollUp();
}

function hideUpload() {
	var grid = $(".grid").length;
	updateTitle(grid ? 'Submissions for ' + $(".grid span").text().replace(/"/g, '') : 
		$("#page .tag").text());
	$("#uploadBar").hide();
	$("#page").show();
	$.get("site/session?upload=0");
	if (!grid) $("#system").css("display", "");
}

function loadTab(model) {
	$.ajax({'url': model + "/view", 'success': function(data) {
	$("#page").html(data).show(); $("#system").hide()}, 
	'cache': true});
}

function refineSystem(order, model, set) {
	$.fn.yiiListView.update('system', {url: restore + panel + "&order=" + 
	order + (model ? "&model=" + model : '') + (set ? "&set=" + set : '')});
}

function routePage(url) {
	scrollUp();
	$.bbq.pushState(url !== undefined ? url : "#!/");
}

function scrollUp() {
	$(window).scrollTop(0);
}

function signUp() {
	$("#nav .signup a").click();
}

function saveCart(button, match) {
	$.getJSON("design/cart?rid=" + match, function(data) {
		if (data.success) {
			var desc = data.cart ? 'Added to cart' : 'I want this';
			var type = data.cart ? 'cart' : 'want';
			button.attr("class", type).text(desc);
			$("#page .cost").text('SGD ' + data.total);
			$("#buyDesign").attr("disabled", !data.total);
		} else {
			button.removeClass("focus");
		}
	});
}

function showTip(input, tip) {
	var tip = input.next();
	if (input.hasClass("issue") && tip.css("display") == "none" && tip.text() != "") {
		tip.fadeIn(fast);
	}
}

function showSub() {
	if ($("#uploadBar").length) {
		updateTitle("Submit your design");
		$("#uploadBar").show();
		$("#page").hide();
		$("#system").hide();
		if (typeof(ajaxUpload) != "undefined") ajaxUpload();
		$.get("site/session?upload=1");
	} else {
		signUp();
	}
}

function submitDesign() {
	if ($("#filename").val() != "") {
		$(".gloss").removeClass("hidden");
		$(".jcrop-holder").hide();
		$("#jcrop").attr("width", 582).attr("height", 582).attr("src", "/images/" + 
			"clear.png").css({'width': '', 'height': '', 'display': ''});
		$("#state span").html("Submitting design...");
	}
}

function setFocus(item) {
	if (!item.hasClass("focus")) {
		item.addClass("focus");
		if (!item.parent().is(":visible")) {
			item.parent().show();
		}
		$(".navItem").not(item).removeClass("focus");
	}
}

function setRefine(item) {
	$(".navRefine").each(function() {
		if ($(this).attr("href").indexOf(item) != -1) {
			$(this).parent().addClass("focus");
		} else {
			$(this).parent().removeClass("focus");
		}
	});
}

function updateDisplay(item)
{
	if (item == undefined) {
		item = $(".navItem.focus");
	}
	panel = $(item).data("panel");
	updateSystem(item);
}

function updateList() {
	if (!$("#system").hasClass("fixed")) {
		$("#page").hide();
		updateTitle($("#rootBar .focus"), $("li.focus a"));
	}
}

function updateSystem(item) {
	if (item === undefined) {
		item = $("#customerBar .focus");
	}
	var order = item.parent().attr("id") == "pbz" ? "&order=-1" : '';
	if (item.data("panel") !== undefined) {
		$.fn.yiiListView.update('system', {url: restore + panel + order});
	}
}

function updateTitle(desc, action) {
	if (typeof desc !== 'string') {
		desc = $(desc).text();
	}
	if (action !== undefined) {
		if ($(action).data("key") != -1) {
			desc += (desc > "" ? ' - ' : '') + $(action).text();
		} else {
			desc = false;
		}
	}
	if (desc) {
		document.title = titleCase(desc) + " - Professional Bazaar";
	} else {
		document.title = "Probaz | Professional Bazaar";
	}
}

$("#content").on("click", "a.expand", function(e) {
	e.preventDefault();
	$(this).closest("form").find(".token").show();
});

$("#content").on("click", ".navRefine", function(e) {
	e.preventDefault();
	if ($(this).data("type") !== undefined) {
		refineSystem($(this).data("key"));
	}
});

$("#content").on("click", "#sendLink", function(e) {
	e.preventDefault();
	if ($("form.loginForm").valid()) {
		var email = $("#Person_email").val();
		$.get("person/reset?email=" + email, function(data) {
			if (data) juiAlert("Reset instructions have been sent to " + email);
			$("#Person_email").val(""); $("#Person_password").val("");
			var token = $("#sendLink").attr("id", "saveHub").
			val("LOGIN").parent().removeClass("inline").parent();
			token.next().show().prev().prev().show();
		});
	}
});

$("#reset a").on("click", function(e) {
	e.preventDefault();
	var token = $(this).closest(".token"); token.hide();
	token.prev().prev().hide(); $("#Person_password").val("x");
	token.prev().find(".submit").addClass("inline").find("input").
	attr("id", "sendLink").val("SEND LINK");
});

$("#hide a").on("click", function(e) {
	e.preventDefault();
	$("#verify").addClass("hidden");
	$.get("site/session?hide=1");
});

$("#close a").on("click", function (e) {
	e.preventDefault();
	$("body").attr("id", "master");
});

$("#signin a").on("click", function(e) {
	e.preventDefault(); scrollUp();
	$("body").attr("id", "login");
});

$("#signup a, #service a, .signup a, a.touch").on("click", function(e) {
	e.preventDefault(); scrollUp();
	$("body").attr("id", "register");
});

$("#task").each(function() {
	var blank = this.value;
	$(this).keyup(function(e) {
		if (e.keyCode == 13) {
			$.bbq.pushState("#!/post&" + $(this).serialize());
		}
	});
	$(this).focus(function(e) {
		if (this.value == blank) {
			this.value = '';
		}
	});
	$(this).blur(function(e) {
		if (this.value == '') {
			this.value = blank;
		}
	});
});

$("body").on("click", ".dialog", function(e) {
	e.preventDefault();
	if (typeof FB != "undefined") {
		FB.login(function(response) {
			if (response.authResponse) window.location.reload();
		}, {scope: 'email, user_birthday, publish_actions'});
	}
});

$("body").on("hover", ".dialog", function() {
	if (!fb) {
		fb = true;
		$.getScript(document.location.protocol + '//connect.facebook.net/en_US/all.js', 
		function() {
			FB.init({
				appId: $('#fb-root').data("app"),
				cookie: true,
				oauth: true,
			});
		});
	}
});

$(window).bind("hashchange", function(e, init) {
	var hash = $.param.fragment();
	var url = hash.substr(1).split('/');
	var top = url[0];
	var item = false;
	var show = true;
	if (url.length == 1) {
		if (top.indexOf('post') != -1) {
			var url = 'task/create';
			$.each($.deparam.fragment(), function(need, val) {
				if (need == 'type') url += '?cat=' + val});
			$.ajax({'url': url, 'success': function(data) {hashPage(data, 
				false); updateTitle('Post Brief')}, 'cache': true});
		} else if (hash == '' || hash == '/' || hash == '/browse') {
			item = $("#pbz a");
			if (hash == '') {
				$.bbq.pushState("#!/");
			} else if (hash == '/' || !$("#marketBar").is(":visible")) {
				updateDisplay(item);
			} else {
				hideMarket();
			}
		} else if (hash[1] == hash[1].toUpperCase()) {
			$.ajax({'url': 'person/view?alias=' + top, 'success': 
				function(data) {hashPage(data, show)}, 'cache': true});
		}
	} else if (url.length == 2) {
		if (init && top == 'edit') {
			top = 'my'; hashAccount();
		}
		if (top == 'category') {
			item = $("#pbz a");
			panel = item.data("panel");
			refineSystem(url[1], "Category");
		} else if (top == 'my') {
			item = $(".navItem").filter(function() {
				return $(this).attr("href").indexOf(hash) != -1});
			if (item.data("panel") !== undefined) updateDisplay(item); 
			else if (url[1] == 'edit') $.ajax({'url': 'person/edit', 'success': 
				function(data) {hashPage(data, true)}, 'cache': true});
		} else if (top == 'edit' || top == 'job' || top == 'store') {
			var action = "/" + (top != 'job' ? top : 'view') + "?job=" + url[1];
			show = top == 'job';
			$.ajax({'url': 'task' + action, 'success': function(data) {
				hashPage(data, show)}, 'cache': true});
		}
	} else if (url.length == 3) {
		if (url[0] == 'message') {
			$.ajax({'url': 'design/view?job=' + url[1] + '&mirror=' + url[2], 'success': 
				function(data) {hashPage(data, false)}, 'cache': true});
		} else if (url[0] == 'prepare') {
			$.ajax({'url': 'design/prepare?job=' + url[1] + '&mirror=' + url[2], 'success': 
				function(data) {hashPage(data, false)}, 'cache': true});
		}
	}
	if (item) setFocus(item);
});

/*(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

window.fbAsyncInit = function() {
	FB.init({
		appId: $('#fb-root').data("app"),
		cookie: true,
		oauth: true,
	});
};*/
