-- Data tables structure
CREATE TABLE session (
	id CHAR(32),
	expire INTEGER NOT NULL,
	data TEXT,
	PRIMARY KEY (id),
	INDEX (expire)
) ENGINE=InnoDB;

CREATE TABLE country (
	row_id CHAR(2) NOT NULL,
	name VARCHAR(64) NOT NULL,
	flag TINYINT DEFAULT 0,
	PRIMARY KEY (row_id),
	UNIQUE KEY (name)
) ENGINE=InnoDB;

CREATE TABLE city (
	row_id INTEGER AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	country_code CHAR(2),
	confirm TINYINT DEFAULT 0,
	PRIMARY KEY (row_id),
	UNIQUE KEY (name),
	FOREIGN KEY (country_code) REFERENCES country (row_id)
) AUTO_INCREMENT = 1001
ENGINE=InnoDB;

CREATE TABLE hub (
	row_id CHAR(8) NOT NULL,
	name VARCHAR(48),
	dscpt VARCHAR(255),
	url VARCHAR(255),
	logo_id INT,
	country_code CHAR(2),
	garbage TINYINT DEFAULT 0,
	create_on TIMESTAMP NULL,
	PRIMARY KEY (row_id),
	UNIQUE KEY (name, country_code),
	FOREIGN KEY (country_code) REFERENCES country (row_id)
) AUTO_INCREMENT = 10001
ENGINE=InnoDB;

CREATE TABLE person (
	row_id INTEGER AUTO_INCREMENT,
	member ENUM('admin', 'business', 'customer') NOT NULL,
	hub_id CHAR(8) NOT NULL,
	name VARCHAR(64) NOT NULL,
	first_name VARCHAR(32) NOT NULL,
	last_name VARCHAR(32) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password CHAR(60),
	phone VARCHAR(16),
	profile_id INT,
	birthday DATE,
	city VARCHAR(64),
	confirm_key CHAR(38),
	facebook_id BIGINT,
	member_safe CHAR(22),
	new_email VARCHAR(255),
	paypal_email VARCHAR(255),
	reset_pass TINYINT DEFAULT 0,
	valid_card TINYINT DEFAULT 0,
	valid_email TINYINT DEFAULT 0,
	locked TINYINT DEFAULT 0,
	mirror SMALLINT DEFAULT 1,
	create_on TIMESTAMP NULL,
	active_on TIMESTAMP NULL,
	login_on TIMESTAMP NULL,
	PRIMARY KEY (row_id),
	UNIQUE KEY (email),
	UNIQUE KEY (phone),
	UNIQUE KEY (facebook_id),
	UNIQUE KEY (paypal_email)
) AUTO_INCREMENT = 100001
ENGINE=InnoDB;

ALTER TABLE person ADD FOREIGN KEY (hub_id) REFERENCES hub (row_id) ON DELETE CASCADE;

CREATE TABLE account (
	cardholderName VARCHAR(64),
	number VARCHAR(16),
	cvv SMALLINT,
	expirationMonth SMALLINT,
	expirationYear SMALLINT,
	paypal_email VARCHAR(255),
	new_email VARCHAR(255),
	password VARCHAR(60),
	current_pass VARCHAR(60),
	repeat_pass VARCHAR(60),
	setting VARCHAR(48)
) ENGINE=InnoDB;

CREATE TABLE category (
	row_id SMALLINT AUTO_INCREMENT,
	name VARCHAR(48),
	PRIMARY KEY (row_id),
	UNIQUE KEY (name)
) AUTO_INCREMENT = 101
ENGINE=InnoDB;

CREATE TABLE file (
	row_id INTEGER AUTO_INCREMENT,
	hub_id CHAR(8) NOT NULL,
	type ENUM ('design', 'image') NOT NULL,
	name VARCHAR(255) NOT NULL,
	ext VARCHAR(8) NOT NULL,
	salt CHAR(3),
	fb VARCHAR(255),
	copy SMALLINT DEFAULT 1,
	remove TINYINT DEFAULT 0,
	upload_on TIMESTAMP NULL,
	PRIMARY KEY (row_id),
	UNIQUE KEY (name, salt),
	UNIQUE KEY (fb),
	FOREIGN KEY (hub_id) REFERENCES hub (row_id) ON DELETE CASCADE
) AUTO_INCREMENT = 100001
ENGINE=InnoDB;

ALTER TABLE hub ADD FOREIGN KEY (logo_id) REFERENCES file (row_id) ON DELETE SET NULL;
ALTER TABLE person ADD FOREIGN KEY (profile_id) REFERENCES file (row_id) ON DELETE SET NULL;

CREATE TABLE task (
	row_id INTEGER AUTO_INCREMENT,
	hub_id CHAR(8) NOT NULL,
	person_id INT,
	name VARCHAR(48) NOT NULL,
	category_id SMALLINT NOT NULL,
	dscpt TEXT NOT NULL,
	price DECIMAL(9,2) NOT NULL,
	status ENUM ('open', 'closed', 'archived') DEFAULT 'open',
	mirror SMALLINT DEFAULT 1,
	create_on TIMESTAMP NULL,
	update_on TIMESTAMP NULL,
	phase_on TIMESTAMP NULL,
	PRIMARY KEY (row_id),
	UNIQUE KEY (person_id, name),
	FOREIGN KEY (hub_id) REFERENCES hub (row_id) ON DELETE CASCADE,
	FOREIGN KEY (person_id) REFERENCES person (row_id),
	FOREIGN KEY (category_id) REFERENCES category (row_id)
) AUTO_INCREMENT = 100001
ENGINE=InnoDB;

CREATE TABLE task_photo (
	task_id INT NOT NULL,
	photo_id INT NOT NULL,
	rank SMALLINT,
	PRIMARY KEY (task_id, photo_id),
	FOREIGN KEY (task_id) REFERENCES task (row_id) ON DELETE CASCADE,
	FOREIGN KEY (photo_id) REFERENCES file (row_id)
) ENGINE=InnoDB;

CREATE TABLE design (
	row_id INTEGER AUTO_INCREMENT,
	task_id INT NOT NULL,
	person_id INT NOT NULL,
	work_id INT NOT NULL,
	pX SMALLINT DEFAULT 0,
	pY SMALLINT DEFAULT 0,
	pW SMALLINT DEFAULT 0,
	pH SMALLINT DEFAULT 0,
	exactX SMALLINT,
	exactY SMALLINT,
	cart TINYINT DEFAULT 0,
	message TINYINT DEFAULT 0,
	ready TINYINT DEFAULT 0,
	stage ENUM ('push', 'sold', 'pull') DEFAULT 'push',
	mirror SMALLINT DEFAULT 1,
	create_on TIMESTAMP NULL,
	pull_on TIMESTAMP NULL,
	sold_on TIMESTAMP NULL,
	PRIMARY KEY (row_id),
	UNIQUE KEY (work_id),
	FOREIGN KEY (task_id) REFERENCES task (row_id),
	FOREIGN KEY (person_id) REFERENCES person (row_id),
	FOREIGN KEY (work_id) REFERENCES file (row_id) ON DELETE CASCADE
) AUTO_INCREMENT = 1000001
ENGINE=InnoDB;

CREATE TABLE mail (
	row_id INTEGER AUTO_INCREMENT,
	layout VARCHAR(32),
	query BLOB NOT NULL,
	subject VARCHAR(48) NOT NULL,
	stage ENUM ('push', 'sent', 'pull') DEFAULT 'push',
	person_id INT NOT NULL,
	thread TINYINT DEFAULT 0,
	queue_on TIMESTAMP NULL,
	PRIMARY KEY (row_id),
	FOREIGN KEY (person_id) REFERENCES person (row_id)
) AUTO_INCREMENT = 1000001
ENGINE=InnoDB;

CREATE TABLE payment (
	row_id INTEGER AUTO_INCREMENT,
	task_id INT NOT NULL,
	person_id INT NOT NULL,
	amount DECIMAL(9,2),
	debit TINYINT DEFAULT 1,
	fee DECIMAL(9,2) DEFAULT 0,
	mode ENUM ('bank', 'credit', 'paypal') DEFAULT 'paypal',
	commit TINYINT DEFAULT 0,
	paid_on TIMESTAMP NULL,
	PRIMARY KEY (row_id),
	FOREIGN KEY (task_id) REFERENCES task (row_id),
	FOREIGN KEY (person_id) REFERENCES person (row_id)
) AUTO_INCREMENT = 100001
ENGINE=InnoDB;

CREATE TABLE wall (
	row_id INTEGER AUTO_INCREMENT,
	design_id INT,
	task_id INT,
	person_id INT NOT NULL,
	message TEXT,
	type ENUM ('message', 'post', 'edit', 'sale') NOT NULL,
	block TINYINT DEFAULT 0,
	record_on TIMESTAMP NULL,
	PRIMARY KEY (row_id),
	UNIQUE KEY (type, task_id, person_id, record_on),
	FOREIGN KEY (design_id) REFERENCES design (row_id) ON DELETE CASCADE,
	FOREIGN KEY (task_id) REFERENCES task (row_id) ON DELETE CASCADE,
	FOREIGN KEY (person_id) REFERENCES person (row_id) ON DELETE CASCADE
) AUTO_INCREMENT = 1000001
ENGINE=InnoDB;

-- Custom defined procedures
DELIMITER //

CREATE PROCEDURE cleanSession()
BEGIN
	DELETE FROM session WHERE expire < unix_timestamp();
END //

CREATE FUNCTION fileCopy(_hub VARCHAR(8), _name VARCHAR(255), _ext VARCHAR(8), _copy SMALLINT)
RETURNS SMALLINT
BEGIN
	DECLARE ditto SMALLINT;
	SELECT IFNULL(MAX(copy), 0) + _copy INTO ditto FROM file WHERE hub_id = _hub AND 
	name = _name AND ext = _ext;
	RETURN ditto;
END //

CREATE PROCEDURE moveHub(IN guest VARCHAR(8), IN target VARCHAR(8), IN person INT)
BEGIN
	UPDATE file f INNER JOIN (SELECT row_id, fileCopy(target, name, ext, copy) 
	ditto FROM file WHERE hub_id = guest) t ON f.row_id = t.row_id SET 
	f.hub_id = target, f.copy = t.ditto;
	UPDATE wall w INNER JOIN task t ON w.task_id = t.row_id SET w.person_id = person 
	WHERE hub_id = guest;
	UPDATE task SET hub_id = target, person_id = person WHERE hub_id = guest;
END //

CREATE PROCEDURE publicHub()
BEGIN
	INSERT INTO hub (row_id, url) VALUES ('akamaihd', 'http://fbcdn-profile-a.akamaihd.net/hprofile-ak-snc4/') 
		ON DUPLICATE KEY UPDATE name = 'Facebook Pics';
	INSERT INTO hub (row_id) VALUES ('probaz17') ON DUPLICATE KEY UPDATE name = 'Image Hub';
	INSERT INTO file (hub_id, name, salt, ext, type) SELECT 'probaz17', 'igloss', 'nhk', 'png', 'design' 
		ON DUPLICATE KEY UPDATE copy = 1;
	INSERT INTO file (hub_id, name, salt, ext, type) SELECT 'probaz17', 'profile', 'p3n', 'png', 'image' 
		ON DUPLICATE KEY UPDATE copy = 1;
END //

DELIMITER ;
