ALTER TABLE task CHANGE COLUMN name name VARCHAR(48) NOT NULL;
ALTER TABLE task DROP COLUMN title;
ALTER TABLE task DROP COLUMN art;
ALTER TABLE task DROP COLUMN style;
ALTER TABLE task DROP COLUMN target;
ALTER TABLE task DROP COLUMN hate;
ALTER TABLE task DROP COLUMN scope;
ALTER TABLE task ADD COLUMN needs TEXT AFTER category_id;
ALTER TABLE task ADD COLUMN deadline_on TIMESTAMP NULL AFTER update_on;
ALTER TABLE category ADD COLUMN thumb VARCHAR(48) NOT NULL;
ALTER TABLE task DROP FOREIGN KEY task_ibfk_3;
ALTER TABLE task ADD FOREIGN KEY (category_id) REFERENCES category (row_id) ON UPDATE CASCADE;
