INSERT INTO category (name, thumb) VALUES ('Launcher Icons','launcher.png'),
('Outdoor Ads','outdoor.png'),('Print Ads','print.png'),('Photo Editing',
'camera.png'),('Annual Report','report.png'),('Facebook Cover Art','cover.png'),
('Mobile UI Design','mobile.png'),('Web Banner Ads','banner.png'),
('Packaging Design','package.png'),('Logo Design','logo.png'),
('Event Collateral','event.png'),('Other','other.png');
UPDATE task SET category_id = (SELECT row_id FROM category WHERE name = 'Launcher Icons');
DELETE FROM category WHERE name = 'iOS App Icon';

