<?php
	$this->pageTitle = title('Email Verified');
	if (state_new('name')) $this->redirect(app()->createUrl('mail/wrongUrl'));
?>

<h2>Email Verified</h2>
<p>Thanks <?php echo state('name'); ?>, your email was successfully confirmed.</p>
<!--Click here to <?php echo user()->isGuest ? 'login' : 'edit your profile'; ?>.-->
