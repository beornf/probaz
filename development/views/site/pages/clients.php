<?php 
	$this->pageTitle = title('FAQ For Clients');
	$post = $this->createUrl('site/post');
	/*$apple = 'http://developer.apple.com/library/ios/documentation/iphone/conceptual/'.
	'iphoneosprogrammingguide/App-RelatedResources/App-RelatedResources.html';*/
?>

<h1>For Clients</h1>
<h2>1. Where can I get started?</h2>
<p><?php echo CHtml::link('Post a brief here.', $post); ?> It's free! We cater to a 
wide range of business design services.</p>

<h2>2. How much does it cost to post a brief?</h2>
<p>For this limited period, it is absolutely free to post a brief. Give it a try. 
<?php echo CHtml::link('Post a brief now!', $post); ?></p>

<h2>3. Can I keep my brief private?</h2>
<p>All project briefs are kept anonymous. Please take note that project briefs 
that display or identify the client will be revised before getting posted.</p>

<h2>4. What should I look out for in the pitches?</h2>
<p>Before accepting a pitch, you should make sure that you peruse them carefully in 
order to ensure that it meets your 1) project requirements, 2) time requirements, 3) 
revision requirements and 4) budget etc.</p>
<p>The deliverables schedule for your project should be pitched clearly in the pitches 
you receive. If a pitch satisfies you, simply accept it and kick-start your project!</p>

<h2>5. Do I need to pay a deposit when I accept a pitch?</h2>
<p>Yes, you will be required to pay a 25% deposit of the accepted quote to kick-start 
your project. You will however be invoiced for the full amount of the accepted quote 
when you accept a pitch.</p>

<h2>6. Are there any extra charges?</h2>
<p>No, there are no other fees outside of your accepted quote. The quote provided to 
you by a designer, if accepted by you, is the net price you will pay for the project.</p>

<h2>7. How do I give comments and feedback for a submitted design?</h2>
<p>You will be able to have a private conversation with your designer about the project 
and submitted work. You can offer up your feedback and request for changes.</p>
<p>Keep in mind that the pitch you accept should have details about revisions and 
timeline for delivery in accordance with your project brief.</p>

<h2>8. What files do I get as deliverables?</h2>
<p>In your brief, you can specify if you require a .psd file, or an ai. file as working 
files. We currently support these 2 file formats.</p>

<h2>9. What is the design transfer agreement?</h2>
<p>When you kick-start your project (when you select a pitch and make the deposit), you 
also agree acquire all the copyrights and IP to that design, from the designer for that 
project, contingent on the completion of the project, for the amount of the quote 
accepted by you.</p>

<h2>10. When do I pay for the project?</h2>
<p>You only pay for the balance of the project at the end of the project deadline set by 
you. Take care to set a reasonable project deadline for your budget, as this will affect 
the pitches you receive.</p>

<h2>11. What if I misplaced the digital files that I have purchased?</h2>
<p>We continue to store securely the submissions for your projects so should you 
misplace your project file(s), you can always download them again.</p>

<h2>12. What should I do if I suspect that a design has been misappropriated?</h2>
<p>You should contact us at <?php echo support(); ?> so we can investigate the matter. 
Please note that abuse of this reporting channel may get you suspended on Probaz.</p>

<h2>13. Why did my project brief not post?</h2>
<p>If we think your brief is not clear, we may contact you to furnish more details. 
This is to ensure that the designers know with clarity what your requirements are.</p>
<p>If you require help in writing a project brief, please contact us and we will help 
you construct a project brief. <?php echo support(1); ?></p>

<h2>14. How do I make an enquiry to Probaz?</h2>
<p><?php echo support(1); ?><br>We will reply you much sooner than 24 hours.</p>

<!--<h2>6. Can I put in my brief asking submissions to imitate another app say, Instagram’s 
app icon?</h2>
<p>No you may not do that. We ask that you be respectful of other Companies and their 
creative rights. We know you can be creative too! :) Please refer to our terms of use if 
you are not sure about certain rules on probaz.</p>

<h2>7. What are Apple’s guidelines for app icons?</h2>
<p>Apple crops and treats the designs that are submitted to it, including adding the reflective effects 
and cropping (You can choose not to have them: please consult your developer or designer on 
this). We make the effort to make designs submitted to you realistic so that you can make a 
better-informed choice on your purchases. For detailed information please refer to:
<?php /*echo Chtml::link($apple, $apple, array('target'=>'_blank'));*/ ?></p>

<h2>8. Do I have to buy a design that is submitted to me?</h2>
<p>No, if no design satisfies you, you have no obligation to purchase any. However soon, we 
will enable you to guarantee that you will buy a design when you create a brief in our next 
version. This will encourage more designers to be interested in your brief and you will thus 
naturally receive more designs.</p>

<h2>9. How do I buy and download the deliverables?</h2>
<p>You can purchase a design anytime even before the submission deadline ends. You have up 
to 7 days to purchase the designs after the submission deadline. The submission duration 
depends on the package that you select when you create your brief. <?php echo support(1); 
?></p>-->
