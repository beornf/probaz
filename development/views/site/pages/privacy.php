<?php 
	$this->pageTitle = title('Privacy Policy');
?>

<h1>Privacy Policy</h1>
<h2>I. General Preliminaries</h2>
<p>This Privacy Policy governs the way in which probaz.com, a website fully owned and 
operated by UBERQUERY PTE. LTD., collects, uses and shares information gathered from 
users. The terms "we", "us", "our", the "company", “the website" refer to PROBAZ, and 
the terms "you", "your", a "user" refer to you: a user of probaz.com and our services.</p>
<p>By accessing probaz.com and/or using our services, you agree to the policies as laid 
out in this Privacy Policy, and consent to us collecting and using your information in 
certain ways detailed below. However we do not disclose your information to any 3rd 
parties except Google Analytics, unless required by law or explicitly consented by you. 
Please read this document IN FULL. If you do not agree with the Privacy Policy IN FULL, 
please send us your feedback, and we hope to see you back soon.</p>

<h2>II. Personal and Technical Information</h2>
<p>2.1 We gather voluntarily provided personal identification information from users when 
they register on the website, visit the website, submit a brief listing, upload a photo, 
make transactions, comment, or use any service or feature of the website. You can of 
course make changes and control your personal information on PROBAZ.</p>
<p>Information that users may be required to provide upon registration may include a 
username, email address, chosen password and in some cases, a company or business name. 
This information is only collected from users who provide them voluntarily at the point 
of registration or in subsequent time. Users are able to view and browse the website 
without registering, but will be unable to fully utilize the website’s features and the 
implicit services. You should also take care to update your personal information in case 
of any changes.</p>

<p>2.2 Some of this personal information may be available to fellow users of PROBAZ so 
that they can verify your identity and make necessary judgments regarding you.</p>
<p>2.3 We may gather technical information from users when they visit the Site. We employ 
the services of Google Analytics to collect certain technical information. Thus, the terms 
of use of this technical information are subject to Google Analytics' own Privacy Policy, 
and are not covered under our Privacy Policy. Please visit and read Google Analytics’ 
Privacy Policy at <?php echo ref_link('https://www.google.com/intl/en/analytics/privacy'.
'overview.html'); ?> for more information.</p>
<p>2.4 We may also collect information based on your viewing habits on PROBAZ using cookies, 
in order to keep a record of how many times the website is visited, for how long a period, 
and other related technical information. You can configure your browser to disable cookies 
or otherwise delete such cookies.</p>

<p>2.5 Other technical information include, but are not limited to, the type of browser 
used, the operating system of the users' computer, IP address, location, the device on 
which the website is accessed from, the referral URL and other similar information. This 
information is used to improve your experience on the website, for our internal analyses 
of data trends and may be used to improve our operations in order to better serve you.</p>
<p>2.6 We collect certain transactional information due to the nature of PROBAZ, such as 
Paypal information. We do not currently collect your credit card information.</p>
<p>2.7 Because of the nature of PROBAZ, digital files uploads are an essential part of our 
providing our service. Such digital files are stored securely and indefinitely. However we 
do not have any rights in the designs. All briefs that clients may post are also stored 
indefinitely. Your interactions with other users on PROBAZ are likewise, also stored 
indefinitely. Feedback, public and private communication between users on probaz.com is 
also stored in order to provide us with information to improve our services.</p>

<p>2.8 Should you opt to sign into PROBAZ using 3rd party platforms such as Facebook, we 
may have access to certain information that are provided by such 3rd party platforms.</p>
<p>2.9 PROBAZ is not to be used by persons under the age of 13. Therefore we will never 
intentionally collect any information from any persons under the age of 13. If you use 
PROBAZ while under the age of 13, you are violating our terms of use.</p>
<p>2.10 You may encounter links on PROBAZ via which you may access websites that are not 
under our purview and control. Doing so subjects you to that website’s policies and we are 
not responsible for your accessing a 3rd party website from probaz.com.</p>

<h2>III. Summary</h2>
<p>3.1 The Company takes our users' privacy very seriously. We do not sell or trade your 
personal information to any 3rd parties. Your personal information is only used in these 
ways:</p>
<ol type="i">
	<li>To personalize and enhance your experience on the website, such as to customize 
	displayed content;</li>
	<li>To protect our users and the Company in the event of any legal or financial 
	disputes;</li>
	<li>To conduct analyses anonymously. We may display personal information relevant to 
	your account, such as a profile photo, the number of briefs you have completed, or the 
	number of submissions you have made etc.;</li>
	<li>To enable other users to identify, verify and make judgments regarding you and 
	your submissions;</li>
	<li>To ensure that users that have been blacklisted do not manage to signup again;</li>
	<li>To communicate with you electronically;</li>
	<li>For accounting, billing purposes, internal business processes and regulatory 
	obligations but only to the necessary extent;</li>
	<li>To investigate complaints or issues pertaining to the policies governing PROBAZ.</li>
</ol>

<h2>IV. Is Your Information Secure?</h2>
<p>4.1 All information and files we collect and receive respectively, are stored on 3rd 
party servers. We take precautions to prevent unauthorized access, alteration and disclosure 
by third parties. However, any information transmitted through the Internet can never be 
fully secure, and while we make every effort to protect your information, you acknowledge 
that there are security and privacy risks on any website that are beyond our control, and 
that these risks are not completely eliminable. By using PROBAZ, you acknowledge that you 
are aware of such risks.</p>

<p>4.2 Your account on PROBAZ is protected by a password of your choosing of which even we 
do not possess. Thus you are responsible for the security of your account by protecting 
your password appropriately and ensuring that you have logged off from your account after 
accessing it. We encourage you to never give away your account passwords and login 
information to anyone else.</p>
<p>4.3 We do not sell or trade your personal information to 3rd parties. We also do not 
allow advertisers to collect data from you on our website.</p>

<h2>V. Changes To This Policy</h2>
<p>5.1 We reserve the right to amend this privacy policy from time to time as we grow. We 
will notify you about such changes via email or by issuing a notice on the website. By 
continuing to use the Site after the amendments, you acknowledge and agree that you are 
bound by any modifications to this Privacy Policy. Likewise, by not explicitly responding 
to any changes to this Privacy Policy, you indicate your awareness and consent to any such 
updates to this Privacy Policy.</p>

<h2>Contact Us</h2>
<p>Should you have any concerns and enquiries, please feel free to contact us at 
<?php echo support(); ?> and we will gladly attend to you.</p>
<p>This policy was last updated on the 19th of December 2012.</p>
