<?php 
	$this->pageTitle = title('Terms of Use');
?>

<h1>User Terms of Use</h1>
<p>This document governs the use of probaz.com (“PROBAZ”, “us”, “we”, “our”, “the 
website”), a website fully owned and operated by UBERQUERY PTE. LTD. Please read IN FULL 
the following, which constitutes an agreement between “you”, a user of probaz.com and our 
services, and us (“PROBAZ”).</p>
<p>By accessing and using probaz.com and its services, you agree that you are considered 
a user of probaz.com, and thus agree to abide and be bound by these terms of use for as 
long as you continue to be a user. If you do not agree to them in FULL, please give us 
your feedback and we hope to see you back soon. Please also read IN FULL the privacy 
policy, FAQ and other guidelines that govern probaz.com. If you do not agree with them, 
do not use probaz.com.</p>

<h2>I. General Terms</h2>
<p>1.1 By using probaz.com, you affirm that:</p>
<ol type="i">
	<li>You are at least the age of 18, or otherwise have parental or guardian consent 
	to do so;</li>
	<li>You are not under the age of 13;</li>
	<li>That you are solely responsible for your legal conduct on probaz.com;</li>
	<li>That you are solely responsible for making judgments in your interactions and 
	procurement on probaz.com;</li>
	<li>That you are legally able to agree to these terms of use in full. Otherwise, you 
	must not use probaz.com.</li>
</ol>

<p>1.2 For all intents and purposes of this agreement, and all related policies and 
guidelines of probaz.com, if you create a brief to solicit design submissions, you are 
deemed to be a “client”. If you make design submissions to any brief, you are deemed to 
be a “designer”. If you do not fall into either category but are nonetheless a registered 
user, you are simply referred to as a user. “Users”, for all intents and purposes also 
generally refer to all registered persons or businesses on probaz.com, including clients 
and designers, as well as any persons who access probaz.com but who have not registered 
for an account.</p>

<p>1.3 We reserve the right to make altercations to this agreement and other policies 
and guidelines of ours, at any time, without any prior notice. You may contact us at <?php 
echo support(); ?> should you have any concerns or queries and we will attend to you.</p>
<p>1.4 We reserve the right to make altercations to probaz.com and its services at any 
time, without prior notice.</p>
<p>1.5 Any inability to exercise or enforce any rights and provisions in these terms of 
use does not constitute a waiver of any such rights or provisions in these terms of use.</p>
<p>1.6 Should any part of these terms of use, for any reason held to be invalid and/or 
unenforceable in any jurisdiction(s), the outstanding provisions shall nonetheless remain 
valid.</p>

<p>1.7 Users must respect the copyrights of any other persons and business entities, 
pursuant to any acts in the Singapore Copyright Act (Cap. 63). You must not post briefs 
that ask for designs to be similar and to replicate in part or whole, the creative and 
intellectual property of other persons or business entities. Doing so would violate our terms 
of use. We trust that you are creative enough to come up with your own inspirations.</p>
<p>1.8 We take misappropriation seriously. Should you suspect that your work might be so 
misappropriated, please do email us at <?php echo support(); ?>. Please be specific about 
any such claims and provide all relevant contact information and verification of such 
claims so as to assist us in our investigations. Refer to 7.2.</p>
<p>1.9 You may not assign, transfer or sublicense these terms of use in whole or part. We 
however reserve to right to do so without restriction(s).</p>

<h2>II. Scope of Our Service</h2>
<p>2.1 PROBAZ provides a platform that is a marketplace to connect people who require design 
work done, with designers who are able to provide such work.</p>
<p>2.2 PROBAZ is only a platform that facilitates the connection, interaction and 
fulfillment of digital files between users. PROBAZ does not partake in the negotiations 
and interactions between users; we merely provide the platform, communicative tools and 
various electronic methods to enable such negotiations interactions and fulfillments to 
occur efficiently.</p>
<p>2.3 As such, PROBAZ has no obligation(s) to involve itself or to resolve any disputes 
that may occur between users.</p>

<h2>III. Membership and Use of Service Terms</h2>
<p>3.1 User accounts on Probaz.com can be created by (a) individuals, and also (b) business 
entities, including companies. If you are representing a business entity, we encourage you 
to specify in your user account settings – the username - the name of the business entity 
you are representing. This is especially with regards to individuals who use PROBAZ to 
procure design work for the business entity they are associated with.</p>
<p>3.2 To fully utilize our services, you must be able to form legally binding contracts 
in your jurisdiction. If you do not satisfy these conditions, you must not use PROBAZ or 
its services.</p>

<p>3.3 Clients who post a brief must furnish valid credit card details as mode of payment. 
By supplying these credit card details to pay for any purchase, any such users acknowledge 
that:</p>
<ol type="i">
	<li>PROBAZ may debit automatically from that credit card the stipulated amount in the 
	brief, that is payable by you to the designer from whom you have made such purchases;</li>
	<li>He/she has sufficient credit to make good payment for any purchase(s) on 
	probaz.com.</li>
</ol>
<p>N.B: PROBAZ does not store any credit card information.</p>

<p>3.4 Designers retain all their rights in their designs for as long as a client has not 
purchased such designs. We do not have any rights to your designs, but merely to store 
them for sale. Designs that you submit however will be stored securely indefinitely.</p>
<p>3.5 Your account on PROBAZ cannot be transferred to any 3rd party for any reason. So 
long as you are a user of PROBAZ, you must maintain full control of your PROBAZ account 
at all times. Do not furnish your account details to any other persons. Your account is 
your sole responsibility.</p>

<p>3.6 You agree that you will not manipulate your user account in any way, such as to 
falsify information in your interactions on PROBAZ in exchange for monetary gain or favor 
from any client(s). You agree also not to set up multiple accounts so as to manipulate 
such accounts in any way just described, or for any other purposeful reasons.</p>
<p>3.7 You agree that you will not collect information of other users on PROBAZ using any 
means, such as bots. We however consent to search engine bots.</p>
<p>3.8 You grant us the right to display and utilize any content and media that you may 
upload on probaz.com, including their derivatives, for marketing purposes on probaz.com 
or off.</p>

<p>3.9 Designers agree that all content and digital files you submit to us may not be used 
on 3rd party sites or for other business purposes without PROBAZ’s prior permission. 
Designers acknowledge that such design works, accompanied by your rights, submitted on 
PROBAZ may be sold to the client and thus cannot be submitted to any other 3rd parties, 
as such actions would violate the client’s exclusive use of the purchased design(s).</p>
<p>3.10 We reserve the right to remove any submissions, briefs or design work at any time, 
without prior notice should they be deemed in appropriate (Please refer to the FAQ).</p>

<h2>IV. Termination and Refusal of Service</h2>
<p>4.1 We reserve the right to terminate or refuse at any time our services to you should 
you be found to be:</p>
<ol type="i">
	<li>Breaching any of our terms, policies and guidelines (See FAQ);</li>
	<li>Misappropriating content, design, any intellectual property from and on probaz.com;</li>
	<li>Misappropriating content, designs or any intellectual property from other users 
	(See FAQ);</li>
	<li>Found to be misappropriating content and media from any other 3rd party sources and 
	using them on probaz.com;</li>
	<li>Reverse engineering any component(s) of probaz.com;</li>
	<li>Misrepresenting your identity to us and other users to the extent deemed to obstruct 
	the function of a healthy online marketplace community;</li>
	<li>Creating multiple accounts for any reason;</li>
	<li>Providing fictitious, spurious or purposefully deceptive information to us or other 
	users when creating briefs, in submitting any design work or in your interactions with 
	other users;</li>
	<li>A serious nuisance or harassment to other users of probaz.com when using the 
	private messaging service such as using libelous or threatening language. You must be 
	respectful of other users at all times;</li>
	<li>Encouraging unlawful or questionable activities such as promoting illicit substance 
	and their use, advertising sexual or escort services and those activities generally 
	deemed immoral and so on. Briefs that pertain to the legitimate business activities 
	such as of alcohol sales and escort services are of course permitted. Do however take 
	note of APPLE’s app store policies regarding such applications. We cannot be responsible 
	for the viability of the mobile application itself to which the brief pertains;</li>
	<li>Propagating sales or advertising material to other users, including also any contact 
	information;</li>
	<li>Soliciting contact information from other users for any reason;</li>
	<li>Risking harm to PROBAZ or any other person regardless of whether such person(s) are 
	a user of PROBAZ. Harm is defined as, but not limited to economic loss to PROBAZ, its 
	directors, officers or employees or any other person again, regardless of whether 
	he/she is a user of probaz.com;</li>
	<li>Risking harm to minors;</li>
	<li>Suspected or found to be distributing any computer viruses, worms, malware or any 
	software that could put us and other users at risk;</li>
	<li>Suspected or found to be attempting to gain unauthorized access to any of our 
	databases. We also reserve the right to take further discretionary action in any such 
	scenario.</li>
</ol>
<p>4.2 Should our services be terminated or refused to you, PROBAZ is not and cannot be 
liable for any direct or indirect economic loss or non-economic loss that you may occur 
as a result of not being able to utilize our services.</p>

<h2>V. 3rd Party Tools and Identity</h2>
<p>5.1 You acknowledge that we may include tools to assist users to verify the identity of 
other user(s) such as integration(s) with 3rd party websites sites such as Facebook and 
Twitter. You however affirm that these verification tools may not be fully accurate as 
they are still entirely reliant on user-supplied information. These tools are provided so 
that you are able to better make judgments about other users when interacting and making 
decisions about them. PROBAZ does not accept any responsibility for the use of its 3rd 
party verification tools.</p>
<p>5.2 We do take necessary steps to ensure that users on probaz.com provide accurate 
personal information. However, you affirm that there are inherent risks in such online 
communications and transactions.</p>

<h2>VI. Fees, Payment and Refunds</h2>
<p>6.1 There is no fee to become a PROBAZ Member. There is also no fee for a user to post a 
brief.</p>
<p>6.2 We charge a flat commission rate of 25 percent on all transactions on probaz.com. 
The commission rate is applied to all sale prices. We also charge Paypal’s fees in addition 
to the commission. There are no further charges.</p>

<p>6.3 All clients pay only the amount for which they have priced their briefs.</p>
<p>6.4 All payments on probaz.com are in Singapore Dollars.</p>
<p>6.5 You affirm that PROBAZ is not responsible for any declaration of income tax on any 
remuneration that you may be liable for as a result of using probaz.com. PROBAZ is also 
not responsible and liable for any kinds of service tax that you may be liable to collect 
in your jurisdiction as a result of your using probaz.com. You must be aware of the relevant 
obligations you may have in your jurisdiction before using probaz.com.</p>

<p>6.6 All refunds are on a case-by-case basis. To be considered for a refund, you should 
prove that the digital file is “faulty”. “Faulty” here means that (i) the working file 
itself has to be proven to be unusable, (ii) the designer and the client agree that such 
design work is unusable for various reasons, (iii) the designer admits that the submitted 
design work has been misappropriated. However such refund claims are subject to a review 
by PROBAZ, and we reserve the right to approve or reject any refund claims. Please ensure 
prior to purchasing any designs, that you are aware that all purchases are final. 
Administrative fees may apply in any refund.</p>

<p>6.7 We currently use Paypal to disburse funds to designers. You shall receive your 
payment less our commission within 5 days of the transaction date. Please ensure to keep 
your account information up to date at all times. We are not responsible for your providing 
timely and accurate and true information to us.</p>
<p>6.8 When a client decides on purchasing a particular design, he/she will make payment 
to PROBAZ for the amount of the brief, which had been set by the client. PROBAZ will then 
proceed to make payment to the designer, less fees, after payment by the client had been 
cleared. Designer must read the FAQ carefully to ensure that their submissions accord with 
our prescriptions.</p>

<h2>VII. Disputes</h2>
<p>7.1 You agree that we are under no obligation to facilitate, mediate, take part in or 
settle any disputes that you may become involved in the course of your dealings on 
probaz.com. You agree that you are fully aware that your interactions and conduct with 
other users are solely your responsibility. We however take due care to maintain the 
integrity and transparency of the community on probaz.com. PROBAZ takes necessary steps 
to ensure that you receive the best experience we can possibly provide at any point in 
time.</p>
<p>7.2 You agree that we cannot make judgment calls regarding ambiguous claims regarding 
any alleged misappropriated content, such as those about perceived similarities in 
concepts, functions, shapes, colors and so on, and those derivative works that are 
allegedly misappropriated from another work that are not obviously apparent to any human 
being.</p>

<p>7.3 You are not to use the public comments tool to make claims regarding any suspected 
infringement(s); in doing so you could be defaming other users. You instead, should report 
such suspected infringements to us at <?php echo support(); ?>, so that we can investigate 
such complains. As mentioned before, you should provide sufficient and specific information 
in your claims.</p>

<h2>VIII. Warranties and Limitations</h2>
<p>8.1 The designer warrants that at the time of submitting any designs on PROBAZ, he has 
all necessary rights to such submissions so that in the event of being purchased by the 
client, such rights can be transferred to the client. Designer must read and agree with 
the “Design Transfer Agreement” and abide by the FAQ before submitting any designs. 
Designers warrant that he/she shall disclose any ongoing licenses in any submissions. 
Further, no submissions must be made if the designer is unsure about the originality of 
any piece of work he is considering submitting to PROBAZ.</p>

<p>8.2 You affirm that you are FULLY aware that we do not make any warranties regarding 
any digital files, design concepts, and the rights that come with such digital files. 
Designers must ensure that their submissions do not put in risk any other persons or 
businesses in any way; doing so is a blatant violation of our policies. Designers must 
disclose any designs, part or whole that are not original.</p>
<p>8.3 You affirm that you are aware that we do not make any warranties regarding the 
ability of any submissions or the aggregate submissions in relation to any brief(s) on 
PROBAZ, to satisfy your requirements, tastes, intended results, or obligations that you 
may have with other 3rd parties.</p>

<p>8.4 You affirm that PROBAZ does not make any claims or guarantees as to the accuracy 
of any information provided by any user, including information on their user profiles 
and/or design briefs and/or any user’s ability to pay for any purchased work. Again, we 
take necessary steps to verify the authenticity of the identity of a user using emails 
and 3rd party tools such as Facebook. However, you acknowledge that this does not eliminate 
all the risks that are inherent in such online marketplaces to do with identity 
authenticity, not limited to mere identification but also extended to any information such 
persons behind any presumed identities, to post and make claims about their capabilities, 
associations or make promises, implicit or not to you. Thus, any information supplemented 
by any user must be subject solely to your good judgment before making any decision(s) on 
probaz.com.</p>

<p>8.5 You affirm that we cannot be liable for any economic loss or otherwise that you may 
suffer as a result of the conduct of any other user(s) on probaz.com.</p>
<p>8.6 You affirm that PROBAZ cannot be liable for any confidential information that you 
may inadvertently post on probaz.com without the consent of any 3rd party with whom you 
may have a foregoing contractual agreement with. You must take care not to divulge any 
such information of which it is impossible we have any knowledge of.</p>
<p>8.7 You affirm that in no event PROBAZ, its directors, officers, employees, and any 3rd 
party vendors, contractors and suppliers can be liable to you for any kinds of losses that 
you may incur because we do not warrant the accuracy of any user content, claims and 
information on probaz.com. Neither do we make any guarantees as to the reliability of any 
users to perform any tasks on probaz.com.</p>

<h2>IX. Indemnification</h2>
<p>9.1 You agree to hereby and irrevocably indemnify probaz.com, its owners, directors, 
officers and employees, and any of our vendors, contractors and suppliers from any 
liabilities including but not limited to economic costs, legal costs and any damages that 
you may incur from your voluntary use of probaz.com and its accompanying services. We 
cannot be responsible for any such losses that you may incur as a result of your voluntarily 
using probaz.com. You acknowledge that there are inherent risks involved in using any 
online marketplace.</p>

<h2>X. Copyright</h2>
<p>10.1 No portion of probaz.com may be copied, reproduced, distributed or used in any way 
or purpose(s) such as in derivative works and so on, without our explicit consent.</p>

<h2>XI. Jurisdiction</h2>
<p>11.1 The laws of Singapore govern these terms of use and other policies on probaz.com, 
thus all disputes with regards to these terms of use and other policies on probaz.com shall 
be heard in the courts of Singapore.</p>

<p>These terms of use were last updated on the 19th of December 2012.</p>
