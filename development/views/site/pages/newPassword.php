<?php 
	$this->pageTitle = title('Undo Password Change');
?>

<h2>Unathorized Password Change</h2>
<p>Thanks <?php echo state('name'); ?>, this action has been reported. Your account 
has been locked and reset instructions sent to <?php echo state('email'); ?>.</p>
