<?php 
	$this->pageTitle = title('Undo Email Change');
?>

<h2>Unauthorized Email Change</h2>
<p>Thanks <?php echo state('name'); ?>, this action has been reported. Your account 
email has been reset to <?php echo state('email'); ?>.</p>
