<?php 
	$this->pageTitle = title('Email Updated');
	if (state_new('name')) $this->redirect(app()->createUrl('mail/wrongUrl'));
?>

<h2>Email Updated</h2>
<p>Thanks <?php echo state('name'); ?>, your email was successfully updated.</p>
<!--Click here to <?php echo user()->isGuest ? 'login' : 'edit your profile'; ?>.-->
