<?php 
	$this->pageTitle = title('Expired Code');
	if (state_new('name')) $this->redirect(app()->createUrl('mail/wrongUrl'));
?>

<h2>Expired Code</h2>
<p>Sorry <?php echo state('name'); ?>, that code has already expired.</p>
