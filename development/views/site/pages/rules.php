<?php 
	$this->pageTitle = title('Marketplace Rules');
?>

<h1>Marketplace Rules &amp; Guidelines</h1>
<h2>General</h2>
<ol>
	<li><p>Users of Probaz are expected to conduct themselves appropriately, with dignity 
	and to respect fellow bazzers (hereafter referring to users of Probaz.com)</p></li>
	<li><p>All bazzers should above the age of 18</p></li>
	<li><p>Bazzers ascertain that they will take all necessary steps to ensure that they
	conform to their local laws</p></li>
	<li><p>Bazzers should not publicly post sensitive information such as their home 
	address (we take steps to ensure this is not displayed accurately on a map within
	a job posting until you have assigned the job to a particular person(s)), telephones
	numbers, personal identification numbers, birthdates and credit card details etc.
	Nor should they post such sensitive information of other person(s)</p></li>
	<li><p>Bazzers should use the private messaging system for all one-to-one communication, 
	such as the exchange and arrangement of job details and sensitive information. This 
	is for their own safety, as well as protection in the event of any dispute(s)</p></li>
</ol>

<h2>Strictly Disallowed Content</h2>
<p>Certain types of public content – job postings and comments - are against our terms
and conditions and they include:</p>
<ol>
	<li><p>Sexually explicit content regardless of solicitation, expression or allusion to</p></li>
	<li><p>All illegal substances regardless of solicitation, encouragement or allusion to</p></li>
	<li><p>Personal Information including phone numbers and emails</p></li>
	<li><p>All services or activities that incite religious or racial disharmony</p></li>
	<li><p>All escort services that may or may not allude to sexual services</p></li>
	<li><p>Advertising for any 3rd party business entities. Linking to business services
	in your profile is acceptable</p></li>
	<li><p>All illegal activities or encouragement of illegal activities</p></li>
	<li><p>Spam and/or coercive content public or private</p></li>
</ol>
<p>All disallowed content will be removed. Bazzers can notify us of any suspicious
activities by emailing us. Please kindly refer to our terms and conditions for a full</p>

<h2>Dispute and Verification</h2>
<p>Should you feel that you might have been the victim of a crime in your dealings with a
counterparty, you are strongly encouraged to lodge a police report. You should notify us 
via contact us for record and assistance. We may require your provision of the police 
report.</p>
<p>Probaz.com is a website that facilitates transactions between counterparties. As such,
transactions on Probaz are irrevocable and we do not provide refunds.</p>
<p>Probaz takes all necessary steps to ensure the safety of all our users. The following
should be of note to all bazzers:</p>
<ol>
	<li><p>Should bazzers discover that identities or credentials have been malignantly
	misrepresented, they should report the incident to us using the “report user” button
	on the user’s profile. Of course, innocent use of non-human entities as your profile
	picture such as a cat or Marina Bay Sands, are permitted. Jennifer Lopez is permitted
	too.</p></li>
	<li><p>We use facebook and linkedin to assist in authenticating our users for your
	benefit. Verified profiles via facebook or linkedin are, all things equal, probably
	safer than an unverified profile. However, there is no guarantee on the authenticity
	of any individual on the Internet</p></li>
</ol>
<p>The Probaz team wishes you a great experience. Again, you can contact us here for 
any queries.</p>
