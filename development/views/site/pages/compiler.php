<?php
	if (host() == '127.0.0.1') {
		$path = Yii::getPathOfAlias('application.views.builds');
		$all = file_get_contents("$path/all.php"); $sep = "'";
		$base = new CController('base'); $base->layout = 'email';
		preg_match_all("/case $sep.+$sep/", $all, $matches);
		foreach ($matches[0] as $match) {
			$action = trim(strstr($match, $sep), $sep);
			list($base->action, $view) = explode('/', $action);
			$html = $base->render('//builds/all', array('action'=>$action), true);
			$web = "$path/$view.html";
			if (!file_exists($web) || $html != file_get_contents($web)) {
				$raw = $html;
				preg_match_all('/<a href="([^"]+)">([^<]+)<\/a>/', $html, $matches, 
				PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
				$del = 0;
				foreach ($matches as $link) {
					$start = $link[0][1]-$del; $len = strlen($link[0][0]);
					$new = $link[2][0].': '.$link[1][0];
					$html = substr($html, 0, $start).$new.substr($html, $start+$len);
					$del += $len-strlen($new);
				}
				file_put_contents($web, $html);
				serverProc('webview', $path, array($view, 'html'));
				file_put_contents($web, $raw);
			}
		}
	}
	$this->redirect(app()->homeUrl);
?>
