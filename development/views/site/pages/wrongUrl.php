<?php 
	$this->pageTitle = title('Wrong URL');
?>

<h2>Wrong URL</h2>
<p>Malformed email link was entered. Please copy/paste the URL fully.</p>
