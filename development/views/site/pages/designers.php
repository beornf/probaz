<?php 
	$this->pageTitle = title('FAQ for Designers');
	/*$apple = 'http://developer.apple.com/library/ios/documentation/iphone/conceptual/'.
	'iphoneosprogrammingguide/App-RelatedResources/App-RelatedResources.html';*/
?>

<h1>For Designers</h1>
<h2>1. Who should join Probaz – Professional Bazaar for design services?</h2>
<p>We are looking for the freelance creative professional, the 3-man design studio and 
the boutique agency to join us! If you fit the bill sign up here!</p>

<h2>2. Why should I use Probaz?</h2>
<p>Unlike other platforms, you do not submit final works until you are hired. We 
believe “no spec work” favors you, the designer.</p>
<p>Instead you submit .pdf pitches selling your or your business’s experience, CV, 
timeline for delivery (within the client’s requirements), vision for the project 
(including concept art, sketches etc) and quote for the project (within the client’s 
requirements) etc. You may include other relevant details that make you more attractive 
to the client. <?php echo CHtml::link('View a recommended pitch format', 
'/images/probaz.png'); ?>.</p>
<!--<p>If we receive any complains or disputes regarding originality of design work from 
clients over purchased work, we will investigate the matter. To prevent such hassle, 
please ensure that all submissions are original works. Further, ensure that you do not 
submit similar designs to different clients or to copy other submissions, or works from 
elsewhere.</p>-->

<h2>3. Why was my pitch removed?</h2>
<p>We reserve the right to remove pitches that are of unacceptable quality, or those that 
are deemed disrespectful to the client, any minority group, or children. If you feel that 
your pitch has been unfairly removed, please feel free to contact us at <?php 
echo support(); ?>.</p>

<h2>4. What if my pitch does not get chosen?</h2>
<p>Do not worry! There are other projects you can pitch to.</p>

<h2>5. When do I get paid for the project?</h2>
<p>You will be paid in full at the end of a project via Paypal, less Probaz’s fee, which 
is 25 percent. We will deposit the funds into your Paypal account within 7 working days 
of the end of the project.</p>
<p>We currently only do transfers via Paypal. You must update your Paypal information 
before you can submit a pitch. You may create a <?php echo CHtml::link(
'Paypal account here', 'https://www.paypal.com/sg/cgi-bin/webscr?cmd=_registration-run', 
array('target'=>'_blank')); ?> if you do not have one.</p>

<h2>6. What happens should (after) the client choose my pitch?</h2>
<p>Your account will then allow you to submit designs to the client’s account. You will 
then be able to collaborate, discuss details of the project and submissions, and submit 
revisions of the deliverables for the project.</p>

<h2>7. What is the Design Transfer Agreement?</h2>
<p>When you submit a working file, you are required to accept the Design Transfer 
Agreement (DTA). This agreement affirms that the IP rights to your, (or the entity you 
represent’s) submission is transferred to the client, contingent on the completion and 
payment of the project.</p>
<p>You also affirm that you (or the entity you represent) are the creator and owner of 
the design(s) you submit. We do not own the IP to any of your submitted designs, pitches, 
concepts and ideas at any point.</p>

<h2>8. What file formats should I use when I submit designs?</h2>
<p>The client may or may not specify the requirements for file formats for the 
deliverables so take care to follow the requirements of the project brief. However we 
currently only accept .psd files and .ai files.</p>

<h2>9. When does the project end?</h2>
<p>It ends at the time stipulated on the client’s project brief. He/she will make payment 
then, after which you will be paid within 7 days.</p>

<h2>10. I still have more questions. How do I make an enquiry?</h2>
<p><?php echo support(1); ?><br>We will reply you in lightning fast time.</p>

<!--<h2>10. What canvas sizes should I use in my submissions?</h2>
<p>To generate the relevant .png files, we require that you submit 1024 X 1024 .psd files. 
If you are designing in illustrator you do not need to worry about this however as a rule 
of thumb please submit works also of 1024 X 1024. Note that you need not round the edges 
of the artwork when submitting your designs. For detailed information about Apple’s 
regulations for the app store icons, please refer to: <?php /*echo Chtml::link($apple, $apple, 
array('target'=>'_blank'));*/ ?></p>-->
