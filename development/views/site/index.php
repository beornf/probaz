<?php
	$this->pageTitle = title();
	$action = $this->action->id;
	if ($action == 'index') list($action, $data) = state('ajax');
	
	// debugging
	if (!user()->isGuest) {
		//$subject = 'Verify your account'; $type = 'verifyEmail';
		//sendMail($subject, $type, array('confirm'=>confirmCode($type)));
		//print_r(getimagesize('./uploads/r1rubl3s/example_ejl_print.png.png'));
		// 7 character for security token
		/*$key = encrypt('d50d5a3,68930413');
		echo substr($key, 0, 22);
		echo 'Decode: '.decrypt($key).', size: '.strlen($key);
		sendMail('Unauthorized email 2!', false, array('Email change stopped', 
		'Pirates be near, shiver me timbers!'));*/
		//1347818173 - substr(time(), 0, 6)
		/*$start = microtime(true);
		echo 'Time:'.((microtime(true) - $start)*1000).' ';*/
		/*echo encrypt(array('100095', '1', 'yonghui.yow@gmail.com'), false, true);
		print_r(decrypt('c596d73fREr6-JFXuZ6yxPfUwrE53w', false, true));
		print_r(decrypt('c596d734REr6-JFXuZ6yxPfUwrE53w', false, true));*/
		/*$file = File::model()->findByPk(100482);
		cs()->registerScript('test', JCrop::Script($file->path.'.png'));*/
		/*$action = 'job'; $data = 'listo-app-icon';
		cs()->registerScript('submit', "jQuery('#jcrop').attr('src', './uploads/r1rubl3s/test_2.png').".
		"attr('width', 582).attr('height', 534); ".
		"crop = $.Jcrop('#jcrop', {'allowSelect':false,'allowChange':false,".
		"'trueSize':[1166,1024],'setSelect':[72,1,1094,1023],'onChange':moveCoords,'onSelect':saveCoords}); ".
		"jQuery('.jcrop-holder').css('margin-top', 23); jQuery('#jcrop-thumb').attr('src', ".
		"'./uploads/r1rubl3s/test_2.png').attr('width', 163).attr('height', 143);");*/
		/*"jQuery('.jcrop-holder').css('margin-top', -40).css('margin-bottom', -40)"*/
	} else {
		//$this->widget('application.components.JuiWidget');
		/*$design = Design::model()->findByPk(1000044);
		$task = Task::model()->findByPk(100026);
		$desc = 'Check them out and grab your own beautiful icon.';
		$link = $this->createAbsoluteUrl('grid/'.hash_route(null, $task, 'task'));
		$uri = $this->createAbsoluteUrl('');
		$actions = json_encode(array('name'=>'Post Brief', 'link'=>$this->createAbsoluteUrl('/post')));
		$request = array('app_id'=>param('fbId'), 'display'=>'popup', 'link'=>$link, 'picture'=>'http://bidmygoods.com/test_round.png', 
		'name'=>'I bought a new handmade icon', 'caption'=>param('dscpt'), 'actions'=>$actions, 
		'description'=>$desc, 'redirect_uri'=>$uri, 'ref'=>'invoice');
		$share = 'https://www.facebook.com/dialog/feed?'.http_build_query($request, '', '&');
		$request = array('url'=>$link, 'text'=>$desc, 'via'=>$uri);
		$tweet = 'https://twitter.com/intent/tweet?'.http_build_query($request, '', '&');
		sendMail(array('report'=>'http://127.0.0.1/uber/report?entry=1ac9a8f7fACQ-Qj15TJZHSbCLtjaV'.
		'-hg1-_MRiAUSG9eRUVVCO5X-Bm8BIpbqPm7_Lgfk8vd2'), 'Email change notice', 'oldEmail', 100001);*/
	}
	
	$options = array('hidden'=>!in_array($action, array('index', 'category')));
	if (isset($class)) {
		$auth = $class == 'auth'; $this->pageTitle = $auth ? title($action) : title();
		echo openDiv('#system').closeDiv().($auth ? auth($action, $model) : openDiv('#approve').
		openDiv('.login').Plan::Model('Approve', Plot::Edit, new Mail)->html()).closeDiv().closeDiv();
	} else {
		if (user()->isGuest) {
			echo openDiv('#marketBar', $options);
			$count = Person::model()->count();
			?>
				<!--Get Custom Photos From Local Photographers-->
				<h1>Brief <?php echo $count; ?> Creative Professionals Instantly.</h1>
				<!--Receive Perfect Photos For Your Creative Projects-->
				<h2>Find The Perfect Creative For Your Business. Get Pitched Now.</h2>
				<div class="signup ocean"><?php echo CHtml::link("Let’s Get Started!", '#'); ?></div>
				<div class="claim">It is free to submit your brief!</div>
				<div class="divider"></div>
				<div id="howto">
					<h2>How It Works.</h2>
					<div class="unit">
						<img src="/images/invite.png" />
						<span>1. We Invite Curated Designers</span>
						<div class="signup"><a href="#">I’m A Creative</a></div>
					</div>
					<div class="unit">
						<img src="/images/pitch.png" />
						<span>2. You Receive Quality Samples</span>
						<!--<div id="signup" class="business"><a href="#">Try Now</a></div>-->
					</div>
					<div class="unit">
						<img src="/images/select.png" />
						<span>3. Select Favourite Creative</span>
					</div>
					<div class="unit">
						<img src="/images/receive.png" />
						<span>4. Receive Your Deliverables</span>
					</div>
				</div>
				<!--<div class="group">
					<h1>Redesign your App launcher icons</h1>
					<div class="sector"><div id="service" class="tree">
						<?php echo CHtml::link("I'm a designer", '#'); ?>
					</div></div>
					<h2>Your App Icon <span>Matters.</span></h2>
					<div id="post"><a href="<?php echo hash_route('', 'post'); ?>">
					Post Work</a></div>
				</div>
				<div class="group">
					<img src="/images/iphone_cover.png" />
				</div>-->
			<?php echo openDiv('.raise').closeDiv().closeDiv();
		} else if (verify() && !state('hide')) {
			$js = '$("a.resend").replaceWith("<strong>Email sent!</strong>")';
			$link = verify(1) ? EHtml::ajaxLink('#content', $js, array('class'=>'resend'), 
			app()->createUrl('site/resend'), '', array(), 'Send again!') : '';
			$type = user()->person->new_email ? 'new email' : 'email and receive proposals';
			echo openDiv('#verify').CHtml::openTag('span')."Please verify your $type by ".
			"clicking the link we sent you. $link".CHtml::closeTag('span').closeDiv();
			/*openDiv('#hide').CHtml::link('Hide', '#').closeDiv().*/
		}
		echo openDiv('#service', $options).CHtml::openTag('h1').'Choose A <span>Service</span> '.
		'and Submit Your <span>Project Brief</span>.'.CHtml::closeTag('h1').openDiv('.project');
		foreach (Category::model()->findAll() as $category) {
			list($name, $thumb) = intersect_model($category, array('name', 'thumb'));
			$route = hash_route('post'.'&type='.$category->row_id); $open = true;
			echo openDiv('.panel').($open ? CHtml::openTag('a', array('href'=>$route)) : 
			CHtml::image('/images/soon.png')).openDiv('.select', array('class'=>!$open ? 
			'soon' : false)).CHtml::image('/images/'.$thumb).closeDiv().($open ? 
			CHtml::closeTag('a') : '').openDiv('.note').$name.closeDiv().closeDiv();
		}
		echo closeDiv().openDiv('.raise').closeDiv().closeDiv();
		$agent = Agent::Browse; $hide = false; $page = $upload = '';
		if ($action == 'category') {
			$this->pageTitle = title(updateRefine($agent, $data, 'Category'));
		} else if ($action == 'post') {
			$page = Plan::Model('Task', Plot::Create, new Task)->html();
			$this->pageTitle = title('Post Brief'); $hide = true;
		} else if ($action == 'job') {
			$task = match_like('Task', $data);
			if ($task) {
				$page = Plan::Model('Task', Plot::View, $task)->html();
				$upload = Plan::Model('Upload', Plot::Edit, $task)->html();
				$this->pageTitle = title(state('upload') ? 'Submit your design' : $task->name);
			}
		} else if ($action == 'message') {
			$task = match_like('Task', $data[0]);
			$design = $task ? Design::model()->find('task_id=:tid AND mirror=:mirror', 
			array(':tid'=>$task->row_id, ':mirror'=>$data[1])) : false;
			if ($hide = $design) {
				$page = Plan::Model('Message', Plot::View, $design)->html();
				$this->pageTitle = 'Discussion on Submission #'.(int)$data[1];
			}
		} else if ($action == 'store') {
			$task = match_like('Task', $data);
			if ($hide = $task) {
				$page = Plan::Model('Grid', Plot::Edit, $task)->html();
				$upload = Plan::Model('Upload', Plot::Edit, $task)->html();
				$this->pageTitle = title(state('upload') ? 'Submit your design' : 
				'Submissions for '.$task->name);
			}
		} else if ($action == 'account' && !user()->isGuest) {
			$agent = Agent::Accounts;
			$this->pageTitle = title(Panel::Title($agent));
		} else if ($action == 'edit' && !user()->isGuest) {
			$page = openDiv('.profile').Plan::Model('Profile', Plot::Edit, 
			user()->person)->html().closeDiv();
			$this->pageTitle = title('Edit Profile');
		} else if ($action == 'profile') {
			if ($data !== null) {
				$person = match_like('person', $data);
				if ($person) {
					$page = openDiv('.profile').Plan::Model('Profile', Plot::View, 
					$person)->html().closeDiv();
					$this->pageTitle = title($person->name);
				}
			} else if (!user()->isGuest) {
				$agent = Agent::Profile;
				$this->pageTitle = title(Panel::Title($agent));
			}
		} else if ($action == 'space' && !user()->isGuest) {
			$agent = Agent::Space;
			$this->pageTitle = title(Panel::Title($agent));
		}
		$panel = Panel::Agent($agent);
		echo blockDiv('#page', $page == '' || state('upload')).$page.closeDiv();
		if (!user()->isGuest) echo blockDiv("#uploadBar", !state('upload')).$upload.closeDiv();
		echo $panel->display($page != '', isset($route), $hide || state('upload'));
		cs()->registerScript('setPanel', 'var panel = "'.$agent.'";', CClientScript::POS_END);
	}
	$load = openDiv('.load');
	foreach (array('arrow', 'back_orange', 'close', 'facebook', 'submit') as $file) {
		$load .= CHtml::image("/images/$file.png");
	}
	echo $load.closeDiv();
