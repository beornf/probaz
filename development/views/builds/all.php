<?php switch ($action) {
	case 'design/invoice': ?>
		<h1>Thanks <span>{$client}</span> for Purchasing a design from <span>{$artist}</span></h1>
		<a href="{$store}" target="_blank">View Your Purchase</a><img alt="Design" class="thumb" 
		width="195" height="195" src="{$thumb}" /><h2>{$currency} {$price}</h2><div class="break">
		</div><h2 class="norm">Share Your Purchase</h2><a href="{$fb}" target="_blank"><img alt="Share" 
		width="92" height="35" src="<?php echo imageBase('fb_link.png'); ?>" /></a><a href="{$tw}" 
		target="_blank"><img alt="Tweet" width="92" height="35" src="<?php echo imageBase('tw_link.png'
		);?>" /></a><div class="break"></div><a href="<?php echo root().'/post'; ?>" target="_blank">
		<img alt="Post Brief" width="156" height="37" src="<?php echo imageBase('post_link.png'); ?>" />
		</a><p>Contact Us at <?php echo SUPPORT; ?></p><?php break;
	case 'formal/resetPassword': ?>
		<p>Dear {$person->full}, you have recently reset your password. Please login now with 
		this temporary password:<br><br>{$reset}<br><br>After logging in please change to 
		your desired password at my account.<?php break;
	case 'formal/newPassword': ?>
		<p>Dear {$person->full}, you have recently changed your password. If this was not your 
		action <a href="{$report}" target="_blank">report here</a>.<?php break;
	case 'formal/newEmail': ?>
		<p>Dear {$person->full}, you have recently updated your email to here. To confirm the 
		change <a href="{$confirm}" target="_blank">click here</a>.<?php break;
	case 'formal/oldEmail': ?>
		<p>Dear {$person->full}, you have recently updated your email to {$person->new_email}. If 
		this was not your intention <a href="{$report}" target="_blank">report here</a>.<?php break;
	case 'formal/verifyEmail': ?>
		<p>Hello {$person->first_name},<br><br>Thanks for signing up for Probaz.
		Your're one step away from commanding the ranks.<br><br>
		<a href="{$confirm}" target="_blank">Verify your account</a><?php break;
}
