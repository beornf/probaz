<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
	"http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php if ($this->action == 'design') : ?>
		<!-- mail style {literal}-->
		<style type="text/css">
			body { font-family: Arial; font-size: 90%; color: #333; }
			table { background-color: #f2f2f2; }
			table td { border-collapse: collapse; }
			h1 span { color: #f7931e; }
			h1 { width: 70%; font-size: 16.5pt; }
			h2 { margin: 9px 0; font-size: 13.5pt; }
			h2.norm { margin: 5px 0 0; font-size: 11pt; font-weight: normal; }
			a { margin: 7px 7px 5px; color: #1155cc; display: inline-block; }
			p { margin: 29px 0; }
			a, p { font-size: 8pt; }
			body, h1 { margin: 0; }
			img.logo { margin-left: 35px; }
			img.thumb { vertical-align: middle; display: table-cell; }
			span.footer { margin-right: 78px; font-family: Georgia; font-size: 6pt; }
			.backdrop { background-color: #333; font-weight: bold; color: #fff; }
			.break { margin: 3px 0 5px; width: 48%; border-top: 1px dotted #b0b0b0; }
		</style>
	</head>
	<body>
	<center><table width="600" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class=
	"backdrop" height="90" align="left"><img class="logo" src="<?php echo imageBase('logo.png'); 
	?>" /></td></tr><tr><td><table width="100%" cellspacing="0" cellpadding="16" border="0">
		<!-- mail content {/literal}-->
		<tr><td align="center">
<?php echo $content."\n"; ?>
		</td></tr>
	</table></td></tr>
	<tr><td class="backdrop" height="91" align="right"><span class="footer">
	&copy; <?php echo copyright(); ?></span></td></tr>
	</tbody></table></center>
<?php elseif ($this->action == 'formal') : ?>
		<!-- mail style {literal}-->
		<style type="text/css">
			/*body { margin: 0; padding: 0; }*/
		</style>
	</head>
	<body>
		<!-- mail content {/literal}-->
<?php echo $content; $tw = 'http://twitter.com/share?text='.param('dscpt').'&url=';
$fb = 'http://www.facebook.com/sharer/sharer.php?u='; ?><br><br>Cheers,<br>Probaz Squad</p>
		<p>Share to <a href="<?php echo $fb.DOMAIN; ?>" target="_blank">Facebook</a> and 
		<a href="<?php echo $tw.DOMAIN; ?>" target="_blank">Twitter</a></p>
		<div>&copy; Probaz All Rights Reserved 2012</div>
<?php endif; ?>
	</body>
</html>
