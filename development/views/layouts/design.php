<?php
	$cs = cs(); echo pageHeader();
	if (host() == '127.0.0.1') {
		$cs->mergeCssFile((!param('export') ? '/css/' : '').'design.css');
	} else {
		$cs->registerCssFile('/css/'.(DEV ? 'cache' : 'live').'/design.css');
	}
	$cs->registerCoreScript('jquery');
	$cs->registerCssFile('/css/jquery.Jcrop.css');
	$cs->registerScriptFile('/jscript/jquery.Jcrop.min.js');
	$cs->registerScriptFile('/jscript/jquery.autosize.min.js');
	$cs->registerScriptFile('/jscript/jquery.ba-bbq.js');
	$cs->registerScriptFile('/jscript/jquery.scrollto.min.js');
	$cs->registerScriptFile('/jscript/thunder.head.js');
	if (param('export')) $cs->mergeScriptFile('/jscript/jquery.min.js');
	$keywords = array('Probaz', 'App', 'Design', 'Marketplace', 'Icons');
	$dscpt = 'Probaz is a design marketplace of iPhone and iPad app icons for '.
	'businesses and individuals to get quality designs from talented designers.';
	$sizes = array(0, 72); //114, 144
	foreach ($sizes as $size) {
		$options = $size ? array('sizes'=>$size.'x'.$size) : null;
		$cs->registerLinkTag('apple-touch-icon', null, '/images/icons/apple_touch'.
			($size ? "_$size" : '').'.png', null, $options);
	}
	$cs->registerLinkTag('apple-touch-icon', null, '/images/icons/apple_touch.png');
	$path = '/images/thumb.png';
	$cs->registerLinkTag('image_src', null, $path);
	$thumb = $this->createAbsoluteUrl($path);
	if (app()->controller->action->id == 'index') {
		$graph = array('url'=>DOMAIN, 'type'=>'website', 'title'=>$this->pageTitle, 
		'image'=>$thumb, 'description'=>param('dscpt'), 'site_name'=>host());
		foreach ($graph as $prop=>$text) {
			echo "\t".'<meta property="og:'.$prop.'" content="'.$text.'" />'."\n";
		}
	}
?>
	<meta property="fb:app_id" content="<?php echo param('fbId'); ?>" />
	<meta name="author" content="Uberquery Pte Ltd" />
	<meta name="description" content="<?php echo $dscpt; ?>" />
	<meta name="keywords" content="<?php echo implode(', ', $keywords); ?>" />
	<meta name="language" content="en" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
	<script type="text/javascript">
		function previewImage(value, title, full) {
			var item = $("#pick").find("option[value='" + value + "']");
			<?php echo 'var size = full ? "'.implode('_', Thumbs::Large()).'" : "'.
				implode('_', Thumbs::Medium()).'";' ?>
			var path = item.data("path") + "_" + size + "." + item.data("ext");
			return '<img alt="' + title + '" src="' + path + '" />';
		}
		<?php if (!DEV) : ?>
			var uvOptions = {};
			(function() {
				var uv = document.createElement('script'); uv.type = 'text/javascript'; uv.async = true;
				uv.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'widget.uservoice.com/v1XuaV0IY9tL5nPja6mw.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(uv, s);
			})();
			
			var __insp = __insp || [];
			__insp.push(['wid', 2539743893]);
			(function() {
				function __ldinsp(){var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://www.inspectlet.com/inspect_nojquery/2539743893.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); }
				if (window.attachEvent){
					window.attachEvent('onload', __ldinsp);
				}else{
					window.addEventListener('load', __ldinsp, false);
				}
			})();
		<?php endif;
		$script = ''; $href = DOMAIN; $text = param('dscpt');
		foreach (array('fb', 'tw') as $feed) {
			$script .= '$("#'.$feed.' a").on("click", function(e) {e.preventDefault(); '.
			"popup('$feed', '$href', '$text')}); ";
		}
		$cs->registerScript('share', $script);
		if ($this->action->id == 'index' && !app()->params['export']) $cs->registerScript(
		'triggerHash', '$(window).trigger("hashchange", true);', CClientScript::POS_READY);
		if (host() == '127.0.0.1') {
			require_once(Yii::getPathOfAlias('ext.dynamicRes').'/JavaScriptPacker.php');
			$files = array('css'=>'css/cache/design', 'js'=>'jscript/water.body');
			foreach ($files as $type=>$file) {
				$file = web_root()."/$file";
				$build = file_get_contents("$file.$type");
				if ($type == 'css') {
					$search = array("'/", ';', '{', '}', ',');
					$replace = array("'./", ";\n\t", " {\n\t", ";\n}\n", ', ');
					$build = str_replace($search, $replace, $build);
					file_put_contents(web_root().'/design.css', $build);
				} else if ($type == 'js') {
					$jsp = new JavaScriptPacker($build);
					file_put_contents("$file.min.js", $jsp->pack());
				}
			}
		} ?>
	</script>
</head>
<?php $wide = state_new('wide') || state('wide') ? 'wide' : '';
$action = $this->action->id; $auth = array('login', 'register'); 
$parent = in_array($action, $auth) ? $action : 'master'; ?>
<body id="<?php echo $parent; ?>" class="<?php echo $wide; ?>">
	<div class="back"></div>
	<div id="customerBar">
		<div id="topBar">
			<div id="pbz" class="lower"><?php
				$panel = Panel::Agent(Agent::Browse);
				echo $panel->menu('Professional Bazaar'); //BRAND
			?></div>
			<div id="pivot" class="lower">
				<?php
					/*$item = 'post'; $params = array('class'=>$item); //!DEV: 'class'=>'touch'
					echo $sep.CHtml::link('Post a brief', hash_route($item), $params);*/
					$sep = ' / '; echo $sep.CHtml::link('Browse Briefs', hash_route('browse'), array('class'=>'browse'));
					if (host() == '127.0.0.1') echo $sep.CHtml::link('Mail Compiler', app()->createUrl('site/compiler'));
				?>
			</div>
			<div id="nav" class="lower">
				<?php
					if (user()->isGuest) {
						$param = array('class'=>'dialog'); //Photographer
						echo openDiv('.signup').CHtml::link("I’m a Designer", '#').closeDiv();
						echo openDiv('#connect').CHtml::link('Connect with FB', '#', $param).closeDiv();
					} else {
						$photo = user()->person->profile !== null ? user()->person->profile : photoDefault();
						$welcome = 'Welcome, <br><br>'.user()->person->first_name.' '.user()->person->last_name;
						echo openDiv('#profile', array('class'=>'nowrap')).$welcome.closeDiv().thumbnail($photo);
					}
				?>
			</div>
		</div>
		<div id="unitBar">
			<?php
				$client = !user()->isGuest; //Commercial Photography
				$unit = openDiv('#tag').'Your Professional Bazaar For <span>Creative Services</span>'.
				closeDiv().openDiv('#signin', array('hidden'=>$client)).CHtml::link('Login', '#').closeDiv().
				openDiv('#signout', array('hidden'=>!$client)).CHtml::link('Sign Out', app()->createUrl(
				'site/logout')).closeDiv();
				if (!user()->isGuest) {
					$agent = state('agent');
					foreach (Panel::Group() as $item) {
						$unit .= $item->menu(null, $item->agent == $agent);
					}
				}
				echo $unit;
			?>
		</div>
	</div>
	<?php if (!in_array($action, $auth) && user()->isGuest) $content .= auth();
	echo openDiv('#content').$content.closeDiv(); /*$style = !DEV && app()->controller->
	action->id == 'index' ? array('style'=>'padding-bottom: 0') : array();*/ ?>
	<div id="footerBar">
		<div id="follow"><?php
			//$span = CHtml::openTag('span').'Share Us On:'.CHtml::closeTag('span');
			echo openDiv('#fb').CHtml::link('Post', '#').closeDiv().
			openDiv("#tw").CHtml::link('Tweet', '#').closeDiv();
		?></div>
		<?php
			$nav = array('navLeft'=>array('designers'=>'For Designers', 
			'clients'=>'For Clients', 'privacy'=>'Privacy Policy', 
			'terms'=>'Terms of Use', 'contact'=>'Contact Us'), 
			'navRight'=>array('post'=>'Post Brief', 'account'=>'Account', 
			'design'=>'My Design', 'profile'=>'My Profile', 'login'=>'Login', 
			'register'=>'Register', 'logout'=>'Sign Out'));
			foreach ($nav as $div=>$struct) {
				$right = $div == 'navRight'; $page = $right ? array_slice(array_keys(
				$struct), -3) : array_keys($struct);
				if ($right) {
					$remove = user()->isGuest ? array('account', 'design', 'profile', 
					'logout') : array('login', 'register');
					//if (!DEV) $remove = array_merge($remove, array('post', 'login'));
					foreach ($remove as $key) {
						unset($struct[$key]);
					}
				}
				$range = openDiv("#$div").CHtml::openTag('ul');
				foreach ($struct as $action=>$desc) {
					$range .= CHtml::openTag('li');
					if ($action != 'contact') {
						$url = app()->createUrl((!in_array($action, $page) ? '#!/'.($action != 
						'post' ? 'my/' : '') : '').$action);
						$range .= CHtml::link($desc, $url, array('class'=>$action.'Nav'));
					} else {
						$range .= support($desc);
					}
					$range .= CHtml::closeTag('li');
				}
				echo $range.CHtml::closeTag('ul').closeDiv();
			}
		?>
		<div id="copyright">&copy; <?php echo copyright(); ?></div>
		<div class="drop"></div>
	</div>
	
	<?php $water = (param('export') ? '.' : '').'/jscript/water.body.min.js'; ?>
	<div id="fb-root" data-app="<?php echo app()->params['fbId']; ?>"></div>
	<script type="text/javascript" src="<?php echo $water; ?>"></script>
</body>
</html>