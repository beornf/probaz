<?php
	echo CHtml::beginForm().CHtml::endForm();
	$join = $wall->task->method != 'paypal' ? 'by' : 'via';
?>
<h3>You are about to request payment</h3><div>You've earned: $<?php 
	echo currency($wall->task->price)." $join ".$wall->task->method; ?></div>
<br><div>By clicking below, you confirm that 
	you’ve completed the obligations of this task.</div>
