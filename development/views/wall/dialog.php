<?php
	$credit = $action == 'credit';
	$offer = $action == 'bid';
	$paypal = $action == 'paypal';
	echo EHtml::beginForm();
	/*EHtml::setOptions(array(
		'errorContainer'=>"js: '.lid'+rowNumber(this)",
		'errorLabelContainer'=>"js: '.lid'+rowNumber(this)+' ul'",
		'wrapper'=>'li',
		'errorClass'=>'text',
		'errorElement'=>'span',
	));*/
	$person = user()->person;
	$header = false;
	if ($credit) {
		$customer = braintree_card(false, $person);
		$valid = $person->valid_card ? count($customer->creditCards) > 0 : false;
		$header = $valid ? 'Confirm credit card details' : 'Add credit card details';
		$fee = price($bid * app()->params['profit']);
		$bid = price($bid);
		$card = new Credit;
	} else if ($offer) {
		$header = $fixed ? 'You are about to make a bid' : 'You are about to make an offer';
	} else if ($paypal) {
		$header = 'Add paypal details';
	}
	if ($header) {
		echo CHtml::openTag('h3').$header.CHtml::closeTag('h3');
	}
	if ($paypal || $credit) {
		echo EHtml::activeHiddenField($wall, 'bid', array('value'=>$bid));
	}
	if ($credit) : ?>
		<div>If you're assigned as the Runner for this task, you'll earn $<?php echo $bid; ?> and 
		you will be charged a fee of $<?php echo $fee; ?> (net $<?php echo round($bid-$fee); ?>).</div>
		<br><div>To process the fee we'll need your payment details. You're not charged anything 
		unless you're assigned as the Runner for this task.</div>
		<br><br><?php echo EHtml::hiddenField('credit', 1);
		if ($valid) : ?>
			<?php 
				foreach ($customer->creditCards as $master) {
					if ($master->isDefault()) {
						break;
					}
				}
				$pay = 'Pay via '.$master->cardType.' card xxxx-'.$master->last4.
				" (expires on $master->expirationDate) {";
			?>
			<div class="new"><?php echo $pay; ?><a href="#" class="editCard">Change</a>}</div>
			<?php echo EHtml::endForm(); ?>
			<script type="text/javascript">
				$(".editCard").click(function(e) {
					e.preventDefault();
					var credit = $(this).closest(".ui-dialog-content").find(".credit");
					$(this).closest(".new").html(credit.show());
				});
			</script>
		<?php endif; ?>
		<?php echo openDiv('.credit', array('hidden'=>$valid)); ?>
		<div>Full cardholder name: <?php echo EHtml::activeTextField($card, 'cardholderName'); ?></div>
		<div>Credit card number: <?php echo EHtml::activeTextField($card, 'number'); ?></div>
		<div>CVC number: <?php echo EHtml::activeTextField($card, 'cvv'); ?></div>
		<div>Card expiry: <?php foreach(array('Month'=>12, 'Year'=>14) as $time=>$count) {
			$values = array();
			for ($i = 1; $i <= $count; $i++) {
				$key = $time == 'Month' ? sprintf("%02d", $i) : date('Y')+$i-1;
				$values[$key] = $time == 'Month' ? "$i - ".date('F', strtotime("2000-$i")) : $key;
			}
			$this->widget('ext.multiselect.EMultiSelect', array('model'=>$card, 'class'=>$time, 
			'listModel'=>array('values'=>$values), 'column'=>"expiration$time", 'multiple'=>false, 
			'header'=>"Select $time", 'config'=>array('minWidth'=>'125')));
		} ?></div>
		<?php echo closeDiv(); if (!$valid) echo EHtml::endForm(); ?>
	<?php elseif ($offer) : ?>
		<?php if ($fixed) : ?>
			<div>You'll make: $<?php echo $task->price; ?></div>
			<?php echo EHtml::activeHiddenField($wall, 'bid', array('value'=>$task->price)); ?>
		<?php else : ?>
			<div>Your price: $<?php echo EHtml::activeTextField($wall, 'bid'); ?></div>
		<?php endif; ?>
		<br><div>By clicking below, you confirm that you’ll 
		complete this task if assigned as a Runner.</div>
	<?php elseif ($paypal) : ?>
		<div>The fee for this task will be paid to your PayPal account. 
		Please add the account where you want your fee to go.</div>
		<br><div>Paypal email: <?php echo EHtml::activeTextField($person, 'paypal_email'); ?></div>
	<?php endif;
if (!$credit) echo EHtml::endForm(); ?>
