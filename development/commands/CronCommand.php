<?php

class CronCommand extends CConsoleCommand
{
	private $loop = 2.2;
	private $repeat = 0;
	private $start;
	
	public function actionExport() {
		Yii::import('vendors.*');
		require_once('Zend/Loader.php');
		Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
		Zend_Loader::loadClass('Zend_Gdata_Spreadsheets');
		$service = Zend_Gdata_Spreadsheets::AUTH_SERVICE_NAME;
		$client = Zend_Gdata_ClientLogin::getHttpClient(EMAIL, PASSWORD, $service);
		$spreadsheet = new Zend_Gdata_Spreadsheets($client);
		$work_id = 'od7';
		
		$query = new Zend_Gdata_Spreadsheets_ListQuery();
		$query->setSpreadsheetKey(param('docKey'));
		$query->setWorksheetId($work_id);
		$query->setOrderBy('column:rowid');
		$query->setReverse('true');
		$feed = $spreadsheet->getListFeed($query);
		$rid = count($feed->entries) ? $feed->entries[0]->getCustomByName('rowid') : 0;
		$people = Person::model()->findAll('row_id > :rid', array(':rid'=>$rid));
		foreach ($people as $user) {
			$insert = array('name'=>$user->first_name.' '.$user->last_name, 
			'country'=>$user->hub->countryCode->name, 'rowid'=>$user->row_id, 
			'emailaddress'=>$user->email, 'joined'=>$user->create_on, 
			'born'=>$user->birthday ? $user->birthday : '');
			$spreadsheet->insertRow($insert, param('docKey'), $work_id);
		}
		//$feed = $spreadsheet->getWorksheetFeed($query);
		//echo basename($feed->entries[0]->id);
	}
	
	public function actionMail($thread, $debug=false) {
		$max = 120 - $thread; set_time_limit($max);
		$begin = 0.2 + ($thread * $this->loop / 2);
		chdir(base_path(1));
		$this->wait($begin);
		$this->start = microtime(true);
		
		$total = floor(($max - $begin) / $this->loop) + 1;
		for ($i = 0; $i < $total; $i++) {
			if ($i) $this->wait();
			$mail = Mail::model()->find(array('order'=>'thread '.($thread ? 
			'DESC' : 'ASC').', queue_on', 'condition'=>'sent = 0'));
			if ($mail) {
				$mail->sent = 1;
				$mail->sent_on = new CDbExpression('NOW()');
				$mail->save();
				$data = $this->data($mail->query, $mail->layout);
				sendMail($data, $mail->subject, $mail->layout, $mail->person_id);
				$mail->sent = 2;
				$mail->save();
			}
			if ($debug) echo $thread.' '.$this->measure()."\n";
		}
	}
	
	private function data($query, $layout)
	{
		$output = unserialize($query);
		if ($layout == 'invoice') {
			$design = Design::model()->findByPk($output['design_id']);
			$task = Task::model()->findByPk($output['task_id']);
			$path = ImageHelper::thumb(Thumbs::Invoice, $design->work, $design);
			$pos = strrpos($path, '/'); $dir = strstr(substr($path, 0, $pos), '/uploads');
			serverProc('compose', param('root').strstr($dir, '/'), substr($path, $pos + 1));
			$link = DOMAIN.'store/'.hash_route(null, $task->name, 'task');
			$dscpt = 'Check them out and grab your own beautiful icon.';
			$name = 'I bought a new handmade icon'; $thumb = root().$path;
			$actions = json_encode(array('link'=>DOMAIN.'post', 'name'=>'Post Brief'));
			$params = array('app_id'=>param('fbId'), 'display'=>'popup', 'picture'=>$thumb, 
			'link'=>$link, 'description'=>$dscpt, 'name'=>$name, 'caption'=>param('dscpt'), 
			'actions'=>$actions, 'redirect_uri'=>DOMAIN, 'ref'=>'invoice');
			$request = array('url'=>$link, 'text'=>$dscpt, 'via'=>'theprobaz');
			$fb = 'https://www.facebook.com/dialog/feed?'.http_build_query($params, '', '&');
			$tw = 'https://twitter.com/intent/tweet?'.http_build_query($request, '', '&');
			$output = array('price'=>currency($task->price), 'store'=>$link.'?login=1', 
			'artist'=>$design->person->name, 'client'=>$task->person->name, 'thumb'=>$thumb, 
			'currency'=>param('currency'), 'fb'=>$fb, 'tw'=>$tw);
		}
		return $output;
	}

	private function measure()
	{
		return round(microtime(true) - $this->start, 7);
	}
	
	private function wait($secs=0)
	{
		if (!$secs) {
			$this->repeat++;
			$secs = $this->loop * $this->repeat - $this->measure();
		}
		if ($secs > 0) usleep($secs * 1000000);
	}
}