<?php

class TaskController extends Controller
{
	/**
	 * Defines the access control rules
	 */
	public function accessRules() 
	{
		return array(
			array('allow', 
				'actions'=>array('update', 'edit'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('add', 'create', 'store', 'view'),
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Check price in range
	 */
	/*private function priceSet($model, $pre=0)
	{
		foreach (array('scope'=>'moderate', 'price'=>300) as $attr=>$default) {
			if ($model->$attr === null) $model->$attr = $default;
		}
		list($min, $max, $time) = $model->scope == 'moderate' ? array(
			300, 650, 21) : array(900, 1500, 14);
		if ($pre) $min = $pre; else $model->phase_on = sql_date("+$time day", 'Y-m-d');
		return $model->price >= $min && $model->price <= $max ? $model : false;
	}*/
	
	/**
	 * Add new task
	 */
	public function actionAdd()
	{
		if (app()->request->isAjaxRequest) {
			$gain = false;
			$result = array('invalid'=>array());
			if (isset($_POST['Task'])) {
				$post = $_POST['Task'];
				$model = mirror($this->loadModel(), $post);
				$model->hub_id = hubId();
				$login = !user()->isGuest;
				$model->category_id = $post['category'];
				$model->deadline_on = sql_date('+'.$post['deadline'].' day', 'Y-m-d');
				$model->person_id = $login ? user()->id : Person::model()->find(
				'member="admin"')->row_id;
				if (!Task::model()->count('name=:name AND person_id=:pid', array(
				':name'=>$model->name, ':pid'=>$model->person_id))) {
					if ($gain = $model->save()) {
						$files = isset($post['files']) ? $post['files'] : array();
						for ($i = 0; $i < count($files); $i++) {
							$file = new TaskPhoto;
							$file->task_id = $model->row_id;
							$file->photo_id = $files[$i];
							$file->rank = $i;
							$file->save();
						}
						if ($login) $result['notice'] = $model->name.' has been '.
						'listed for $'.currency($model->price);
					} else {
						//print_r($model->getErrors());
					}
					//if ($model = $this->priceSet($model)) {
					//} else {
						//$result['invalid'][] = 'Task_price';
					//}
				} else {
					$result['invalid'][] = 'Task_name';
				}
			}
			echo response($gain, $result);
		}
	}
	
	/**
	 * Update existing task
	 */
	public function actionUpdate()
	{
		if (app()->request->isAjaxRequest) {
			$gain = false;
			$result = array();
			if (isset($_POST['Task'])) {
				$post = $_POST['Task'];
				$model = $this->loadModel($post['row_id']); $pre = $model->price;
				$model->attributes = $post;
				if ($model = $this->priceSet($model, $pre)) {
					if ($gain = $model->save()) {
						$photos = TaskPhoto::model()->findALl('task_id=:rid', array(':rid'=>
						$model->row_id));
						$list = isset($post['files']) ? $post['files'] : array();
						$exist = array();
						foreach ($photos as $file) {
							$photo = $file->photo_id;
							if (in_array($photo, $list)) {
								$exist[] = $photo;
								$file->rank = array_search($photo, $list);
								$file->save();
							} else {
								$file->delete();
							}
						}
						for ($i = 0; $i < count($list); $i++) {
							if (!in_array($list[$i], $exist)) {
								$file = new TaskPhoto;
								$file->task_id = $model->row_id;
								$file->photo_id = $list[$i];
								$file->rank = $i;
								$file->save();
							}
						}
						$result['notice'] = $model->name.' brief has been updated.';
					}
				} else {
					$result['invalid'][] = 'Task_price';
				}
			}
			echo response($gain, $result);
		}
	}
	
	/**
	 * Create new task
	 */
	public function actionCreate($cat=false)
	{
		if (app()->request->isAjaxRequest) {
			$task = $this->loadModel();
			if ($cat) $task->category = $cat;
			state('ajax', array('post', null));
			$html = Plan::Model('Task', Plot::Create, $task)->html();
			unsetScript('addTask');
			echo $html.renderJs();
		}
	}
	
	/**
	 * Edit existing task
	 */
	public function actionEdit($job)
	{
		if (app()->request->isAjaxRequest) {
			$task = match_like('Task', $job);
			smartTitle('Edit Brief - '.$task->name);
			$html = Plan::Model('Task', Plot::Edit, $task)->html();
			unsetScript('saveTask');
			echo $html.renderJs();
		}
	}
	
	/**
	 * View store page
	 */
	public function actionStore($job)
	{
		if (app()->request->isAjaxRequest) {
			$task = match_like('Task', $job);
			state('ajax', array('store', $job));
			smartTitle('Submissions for '.$task->name);
			$grid = Plan::Model('Grid', Plot::Edit, $task)->html();
			$sub = Plan::Model('Upload', Plot::Edit, $task)->html();
			unsetScript(array('addDesign', 'buyDesign'));
			echo $grid.renderJs().$sub;
		}
	}
	
	/**
	 * View task page
	 */
	public function actionView($job)
	{
		if (app()->request->isAjaxRequest) {
			$task = match_like('Task', $job);
			$category = hash_route(null, $task->category->name);
			cs()->registerScript('refine', "setRefine('$category', 'Category')");
			state('ajax', array('job', $job));
			smartTitle($task->name);
			$brief = Plan::Model('Task', Plot::View, $task)->html();
			$sub = Plan::Model('Upload', Plot::Edit, $task)->html();
			unsetScript('addDesign');
			echo $brief.renderJs().$sub;
		}
	}
	
	/**
	 * Load model based on task id
	 */
	public function loadModel($rid=null)
	{
		return $rid !== null ? Task::model()->findByPk($rid) : new Task;
	}
}