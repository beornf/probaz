<?php

class WallController extends Controller
{
	/*
	 * Defines the access control rules
	 */
	public function accessRules() 
	{
		return array(
			array('allow', 
				'actions'=>array('assign', 'dialog', 'list', 'message', 'paid', 'view'),
				'users'=>array('@'),
			),
		);
	}
	
	/**
	 * Assign job runner
	 */
	public function actionAssign($rid)
	{
		if (app()->request->isAjaxRequest) {
			$gain = false;
			$result = array();
			$wall = $this->loadModel($rid);
			$person = $wall->person;
			if (!$wall->phase && $person->valid_card) {
				braintree_card();
				$amount = $wall->task->price * app()->params['profit'];
				$result = Braintree_Transaction::sale(array(
					'amount'=>$amount,
					'customerId'=>$person->row_id,
					'customFields'=>array(
						'task_name'=>$wall->task->name,
						'wall_id'=>$wall->row_id,
					),
				));
				if ($gain = $result->success) {
					$wall->task->edit = false;
					$wall->task->status = 'active';
					$wall->task->save();
					$wall->phase = 1;
					$wall->save();
					wallLog($wall->task_id, 'assign', $wall->person_id);
					/*$output['token'] = array('type'=>'info', 'message'=>$person->name.
					' has been assigned to '.$wall->task->name);*/
				} else {
					$person->valid_card = 0;
					$person->save();
					braintree_response($result);
					/*$output['token'] = array('type'=>'error', 'message'=>$person->name.
					' was flagged for invalid payment details. Please try again later');*/
				}
			}
			echo response($gain, $result);
		}
	}
	
	/**
	 * Display bid dialog
	 */
	public function actionDialog($rid, $fixed=true)
	{
		if (app()->request->isAjaxRequest) {
			$task = $this->loadModel($rid, true);
			$wall = new Wall;
			$action = 'bid';
			if (isset($_POST['Wall'])) {
				$bid = $_POST['Wall']['bid'];
				$gain = false; $valid = true;
				$result = array();
				$person = user()->person;
				if (isset($_POST['Credit'])) {
					$result = braintree_card($_POST['Credit'], $person);
					$valid = $person->valid_card = $result->success;
					if ($valid) {
						$person->save();
					} else {
						$result = $this->cardResponse($result);
					}
				} else if (isset($_POST['Person'])) {
					$person->paypal_email = $_POST['Person']['paypal_email'];
					if (!$person->save() || $person->paypal_email === null) {
						$valid = false;
					}
				}
				if ($valid) {
					$credit = isset($_POST['credit']) && $person->valid_card;
					$paypal = $person->paypal_email != null;
					if ($credit && $paypal) {
						$wall->action = $action;
						$wall->bid = $bid;
						$wall->task_id = $rid;
						$wall->person_id = user()->id;
						if ($gain = $wall->save()) {
							$result['notice'] = 'Placed a bid on '.$task->name.' for $'.$bid;
							/*$output['token'] = array('type'=>'info', 'message'=>'Placed a bid on '.
							$task->name.' for $'.$bid);
							$output['bid'] = $bid;*/
						}
					} else {
						$action = !$paypal ? 'paypal' : 'credit';
						$result['html'] = $this->renderPartial('dialog', array('wall'=>$wall, 
						'action'=>$action, 'bid'=>$bid), true);
						$result['status'] = 'follow';
					}
				}
				echo response($gain, $result);
			} else {
				$options = array();
				foreach (get_defined_vars() as $id=>$var) {
					$options[$id] = $var;
				}
				$this->renderPartial('dialog', $options);
			}
		}
	}
	
	/**
	 * Display comments list view
	 */
	public function actionList($agent, $row)
	{
		if (app()->request->isAjaxRequest) {
			$this->widget('application.components.ListView', array(
				'agent'=>$agent, 'row'=>$row,
			));
		}
	}
	
	/**
	 * Post wall message
	 */
	public function actionMessage($rid, $member)
	{
		if (app()->request->isAjaxRequest && isset($_POST['Wall'])) {
			$wall = new Wall;
			$wall->attributes = $_POST['Wall'];
			if ($member == 'design') {
				$design = Design::model()->findByPk($rid);
				$design->message = user()->id != $design->person_id ? 1 : 2;
				$design->save();
			}
			$wall["{$member}_id"] = $rid;
			$wall->person_id = user()->id;
			$wall->type = 'message';
			$gain = $wall->message != '' && $wall->save();
			echo response($gain);
		}
	}
	
	/**
	 * Request job payment
	 */
	public function actionPaid($rid, $record=false)
	{
		if (app()->request->isAjaxRequest) {
			$wall = $this->loadModel($rid);
			if ($record) {
				$mode = $wall->task->mode;
				$wall->phase = 2;
				$wall->save();
				wallLog($wall->task_id, 'sell', $wall->person_id);
				$payment = new Payment;
				$payment->task_id = $wall->task_id;
				$payment->person_id = $wall->task->person_id;
				$payment->peer_id = user()->id;
				$payment->amount = $wall->task->price;
				$payment->fee = $wall->task->price * app()->params['profit'];
				$payment->mode = $mode;
				$payment->save();
				echo response(true);
			} else {
				$this->renderPartial('paid', array('wall'=>$wall));
			}
		}
	}
	
	/**
	 * View private messages
	 */
	public function actionView($hub, $job)
	{
		if (app()->request->isAjaxRequest) {
			$person = match_like('Person', $hub);
			$task = match_like('Task', $job);
			state('ajax', array('message', array($hub, $job)));
			smartTitle($task->name.' - Message');
			$walls = Wall::model()->findAll('person_id=:rid AND task_id=:tid', 
			array(':rid'=>$person->row_id, ':tid'=>$task->row_id));
			foreach ($walls as $wall) {
				if ($wall->work) {
					break;
				}
			}
			$html = Plan::Model('Message', Plot::View, $wall)->html();
			echo $html.renderJs();
		}
	}
	
	/**
	 * Load model based on task id
	 */
	public function loadModel($rid, $task=false)
	{
		return $task ? Task::model()->findByPk($rid) : Wall::model()->findByPk($rid);
	}
}