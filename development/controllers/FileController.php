<?php

class FileController extends Controller
{
	/**
	 * Defines the access control rules
	 */
	public function accessRules() 
	{
		return array(
			array('allow', 
				'actions'=>array('large', 'pack', 'private'),
				'users'=>array('@'),
			),
			array('allow', 
				'actions'=>array('upload', 'view'),
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Upload a file to hub webspace
	 */
	public function actionUpload($type, $tid=null)
	{
		$allowedExt = null; $hid = hubId();
		if ($type == 'design') {
			$allowedExt = app()->params['designExt'];
		} else if ($type == 'image') {
			$allowedExt = app()->params['imageExt'];
		}
		$result = array('gain'=>false, 'error'=>'Server error! Your design '.
		'could not be uploaded.');
		if (!user()->isGuest || $type == 'image') {
			$uploader = new qqFileUploader($allowedExt, app()->params['sizeLimit']);
			$filename = $uploader->file->getName();
			$name = ucwords(strstr($filename, '.', true));
			$ext = strtolower(substr(strstr($filename, '.'), 1));
			$model = new File;
			$model->hub_id = $hid;
			$model->type = $type;
			$model->name = $name;
			$model->ext = $ext;
			$model->salt = randomKey(3, File::model(), array('name'=>$name));
			$files = File::model()->findAll(array('order'=>'copy DESC', 
				'condition'=>'hub_id=:hid AND name=:name AND ext=:ext', 'params'=>
				array(':hid'=>$hid, ':name'=>$name, ':ext'=>$ext)));
			if (count($files) > 0) {
				$model->copy = $files[0]['copy'] + 1;
			}
			if ($model->validate()) {
				$dir = param('root').($tid ? "/private/$tid/" : "/uploads/$hid/");
				if (!is_dir($dir)) {
					mkdir($dir, 0755);
				}
				$upload = $uploader->handleUpload($dir, $model->filename, $model->ext);
				if ($upload['success']) {
					if ($type == 'design') {
						$sizes = serverProc('sizes', $dir, array($upload['file'], $ext));
						if ($bounds = explode('x', $sizes)) {
							if ($bounds[0] >= Thumbs::Stage && $bounds[1] >= Thumbs::Stage) {
								if ($model->save() && $script = designPreview($tid, $ext, $dir, 
								$upload['file'])) {
									$result = array('gain'=>true, 'script'=>$script);
								}
							} else {
								if (is_file($upload['save'])) unlink($upload['save']);
								$result['error'] = 'Icon canvas is '.$sizes.'. Please upload '.
								'at least a '.Thumbs::Stage.' wide square.';
							}
						}
					} else if ($type == 'image' && $model->save()) {
						$result = array(
							'gain'=>true,
							'value'=>$model->row_id,
							'ext'=>$model->ext,
							'display'=>$model->display,
							'path'=>param('baseDir').$model->path,
						);
					}
				} else {
					$result['error'] = $upload['error'];
				}
			} else {
				//print_r($model->getErrors());
			}
		}
		echo CJSON::encode($result);
	}
	
	public function actionLarge($rid)
	{
		$design = Design::model()->findByPk($rid);
		if (user()->admin && $design && $design->ready) {
			$path = param('root').'/private/'.$design->task->row_id.'/'.$design->
			work->filename.'_icon.png';
			if (file_exists($path)) {
				header('Content-Type: image/png');
				echo file_get_contents($path);
				exit;
			}
		}
		throw new CHttpException(404);
	}
	
	public function actionPack($tid, $name)
	{
		$error = 404; $mirror = strrchr($name, '-');
		$task = Task::model()->findByPk($tid);
		if ($mirror && $task->hub_id == user()->hid) {
			$mirror = strstr(substr($mirror, 1), '.', true);
			$design = Design::model()->find('task_id=:tid AND mirror=:mirror', array(
			':tid'=>$task->row_id, ':mirror'=>$mirror));
			if ($design->sold == 1) {
				$design->sold = 2;
				$design->save();
			}
			$path = param('root')."/private/$tid/$name";
			if (file_exists($path)) {
				header('Content-Type: application/zip');
				echo file_get_contents($path);
				exit;
			}
		} else if ($mirror) {
			$error = 403;
		}
		throw new CHttpException($error);
	}

	public function actionPrivate($tid, $name)
	{
		$error = 404; $pos = strrpos($name, '_');
		$file = $pos !== false ? File::model()->find('name RLIKE :name AND salt=:salt', 
		array(':name'=>str_replace('_', '[ _]', substr($name, 0, $pos)), ':salt'=>
		substr($name, $pos + 1))) : null;
		if ($file && ($file->hub_id == user()->hid || user()->admin)) {
			$path = param('root')."/private/$tid/$name.png";
			if (file_exists($path)) {
				header('Content-Type: image/png');
				echo file_get_contents($path);
				exit;
			}
		} else if ($file) {
			$error = 403;
		}
		throw new CHttpException($error);
	}

	public function actionView($rid, $large=false)
	{
		if (app()->request->isAjaxRequest) {
			$file = File::model()->findByPk($rid);
			if ($file->type == 'image') {
				$size = $large ? Thumbs::Large() : Thumbs::Medium();
				$thumb = ImageHelper::thumb($size, $file);
				echo $thumb;
			}
		} else {
			$this->render('view');
		}
	}
}