<?php

class SiteController extends Controller
{
	/**
	 * Defines the access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('edit', 'resend', 'message'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('index', 'list', 'login', 'logout', 'page', 'register', 
					'session', 'validate', 'confirm', 'report', 'approve', 'error', 
					'category', 'job', 'person', 'post', 'store', 'stats7'),
				'users'=>array('*'),
			),
		);
	}
	
	protected function accessDenied($user, $message)
	{
		throw new CHttpException(403, $message);
	}
	
	/**
	 * Declares class-based actions
	 */
	public function actions()
	{
		return array(
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions
	 */
	public function actionError($code=null)
	{
		$error = app()->errorHandler->error;
    	if (app()->request->isAjaxRequest) {
    		echo $error['message'];
    	} else {
    		if ($error) {
    			$code = $error['code'] != 404 ? $error['code'] : false;
    			$login = $code == 403 && user()->isGuest;
    			$url = ($login ? 'login' : 'error'.($code ? "/$code" : ''));
    			$this->redirect($this->createUrl($url));
    		}
			$this->render('error', array('code'=>$code ? $code : 404));
		}
	}
	
	/**
	 * Display tasks in list view
	 */
	public function actionList($agent, $order=false, $model=null, $set=null)
	{
		if (app()->request->isAjaxRequest) {
			if ($set === null) {
				if ($order) updateRefine($agent, $order, $model);
				if ($order && $model == 'Category') {
					state('ajax', array('category', $order));
				} else if ($agent != Agent::Browse) {
					if (!user()->isGuest) {
						$action = $agent == Agent::Accounts ? 'account' : ($agent 
						== Agent::Profile ? 'profile' : 'space');
						state('ajax', array($action, null));
					} else {
						$agent = Agent::Browse;
					}
				} else {
					state('ajax', array('index', null));
				}
				state('agent', $agent);
			} else {
				$refine = state('refine');
				$refine[$set] = $order;
				state('refine', $refine);
			}
			$this->widget('application.components.ListView', array(
				'agent'=>$agent,
			));
		}
	}

	/**
	 * Authorize email login
	 */
	public function actionLogin()
	{
		$model = personModel();
		if (isset($_POST['Person'])) {
			$model->attributes = $_POST['Person'];
			if ($model->login()) {
				$this->redirect(app()->homeUrl.'#!/');
			} else {
				$model->password = '';
			}
		}
		$this->render('index', array('class'=>'auth', 'model'=>$model));
	}
	
	/**
	 * Complete logout and redirect
	 */
	public function actionLogout()
	{
		user()->logout();
		if ($logout = facebook_api('logout')) {
			$this->redirect($logout);
		} else {
			$this->redirect(app()->homeUrl.'#!/');
		}
	}
	
	/**
	 * Process email registration
	 */
	public function actionRegister($member='customer')
	{
		$model = isset($_POST['Hub']) ? registerProfile($_POST['Hub'], 
		$member) : hubModel();
		$this->render('index', array('class'=>'auth', 'model'=>$model));
	}
	
	/**
	 * Updates the session variable data
	 */
	public function actionSession()
	{
		ignore_user_abort(1);
		foreach ($_GET as $key=>$val) {
			state($key, $val);
		}
	}
	
	/**
	 * Validate unique person
	 */
	public function actionValidate()
	{
		if (app()->request->isAjaxRequest && isset($_GET['Person'])) {
			foreach ($_GET['Person'] as $name=>$value) {
				if (!Person::model()->count($name.'=:val AND locked = 0', 
				array(':val'=>$value))) {
					echo CJavaScript::jsonEncode(false);
					return;
				}
			}
			echo CJavaScript::jsonEncode(true);
		}
	}
	
	/**
	 * Confirm email received
	 */
	public function actionConfirm($state, $code)
	{
		list($out, $person) = confirmCode($state, $code);
		if ($out) {
			if ($out == 'validChange' || $out == 'validEmail') {
				if ($out == 'validChange') {
					$person->email = $person->new_email;
					$person->new_email = null;
				}
				$person->valid_email = 1;
				$person->save();
			} else if ($out == 'validReset') {
				$refine = state('refine');
				foreach (multiRefine() as $i=>$multi) {
					if ($multi['panel'] == 'Accounts') {
						$refine[$i] = 'security';
					}
				}
				state('refine', $refine);
				$this->redirect(app()->createUrl('my/account'));
			}
			state('name', $person ? $person->first_name : '');
			$this->redirect(app()->createUrl("mail/$out"));
		}
		$this->redirect(app()->homeUrl);
	}
	
	/**
	 * Report from email
	 */
	public function actionReport($entry)
	{
		list($out, $email, $person) = reportCode($entry, true);
		if ($out == 'oldEmail') {
			$person->email = $email;
			$person->confirm_key = $person->new_email = null;
			$person->save();
		} else if ($out == 'newPassword') {
			$person->password = null;
			$person->save();
			// send password reset link
		}
		state('email', $email);
		state('name', $person ? $person->first_name : '');
		$this->redirect(app()->createUrl("mail/$out"));
	}
	
	/**
	 * Resend verification link
	 */
	public function actionResend()
	{
		if (app()->request->isAjaxRequest && verify(1)) {
			$subject = array('Activate this email', 'Verify your account');
			$type = array('newEmail', 'verifyEmail');
			$i = user()->person->new_email ? 0 : 1;
			queueMail(array('confirm'=>confirmCode($type[$i])), $subject[$i], $type[$i]);
		}
	}
	
	/**
	 * Redirect for users
	 */
	private function renderRoute($data, $route, $flush=true)
	{
		$action = $this->action->id;
		$focus = array('data'=>$data, 'route'=>$route);
		state('ajax', array($action, $data));
		if ($route) {
			$url = $this->createUrl('site/index')."#!/$action".($data ? "/$data" : '');
			if ($flush) {
				echo pageHeader().'<script type="text/javascript">document.title = "'.
				title('Please wait').'"; window.location = "'.$url.'"</script>';
				echo strstr($this->render('index', $focus, true), '<meta name');
			} else {
				$this->redirect($url);
			}
		} else {
			$this->render('index', $focus);
		}
	}
	
	/**
	 * View store page
	 */
	public function actionStore($name, $login=false, $route=true, 
	$paid=false, $token=null, $PayerID=null)
	{
		if ($paid && $token) {
			ignore_user_abort(1);
			// display a payment.php page "Payment processing..."
			$result = paypal_nvp('get', $token);
			if ($result && $result['PAYERID'] == $PayerID && paypal_nvp('make', $result)) {
				$task = Task::model()->findByPk($result['task_id']);
				$payment = new Payment;
				$payment->task_id = $task->row_id;
				$payment->person_id = $task->person_id;
				$payment->amount = $result['amount'];
				$payment->commit = 1;
				$payment->paid_on = new CDbExpression('NOW()'); 
				foreach ($result['designs'] as $mirror) {
					$design = Design::model()->find('task_id=:tid AND mirror=:mirror', 
					array(':tid'=>$task->row_id, ':mirror'=>$mirror));
					if (!$design->sold) {
						$design->sold = 1;
						$design->save();
						$payment = new Payment;
						$payment->task_id = $task->row_id;
						$payment->person_id = $design->person_id;
						$payment->amount = $task->price;
						$payment->debit = 0;
						$payment->fee = paypal_fee($task->price);
						$payment->save();
						$subject = "Purchase Invoice #$mirror";
						queueMail(array('design_id'=>$design->row_id, 
						'task_id'=>$task->row_id), $subject, 'invoice');
					}
				}
				$this->renderRoute($name, $route, false);
			} else {
				$this->redirect($this->createUrl('site/error', array('code'=>503)));
			}
		} else if (match_like('Task', $name)) {
			if ($login && user()->isGuest) {
				state('ajax', array('store', $name));
				$this->redirect($this->createUrl('site/login'));
			}
			$this->renderRoute($name, $route);
		} else {
			$this->missingAction('');
		}
	}
	
	/**
	 * View register confirmation
	 */
	public function actionApprove()
	{
		$this->render('index', array('class'=>'register'));
	}
	
	/**
	 * Browse category
	 */
	public function actionCategory($name, $route=true)
	{
		$this->renderRoute($name, $route);
	}
	
	/**
	 * View job page
	 */
	public function actionJob($name, $route=true)
	{
		if (match_like('Task', $name)) {
			$this->renderRoute($name, $route);
		} else {
			$this->missingAction('');
		}
	}
	
	/**
	 * View design message
	 */
	public function actionMessage($name, $order)
	{
		$task = match_like('Task', $name);
		$design = $task ? Design::model()->find('task_id=:tid AND mirror'.
		'=:mirror', array(':tid'=>$task->row_id, ':mirror'=>$order)) : false;
		state('ajax', array('message', array($name, $order)));
		if ($design && ($task->person_id == user()->id || $design->person_id == 
		user()->id)) {
			header('Location: '.$this->createAbsoluteUrl("#!/message/$name/$order"));
		} else {
			$this->missingAction('');
		}
	}
	
	/**
	 * View user profile
	 */
	public function actionPerson($alias, $route=true)
	{
		if (match_like('person', $alias)) {
			$this->renderRoute($alias, $route);
		} else {
			$this->missingAction('');
		}
	}
	
	/**
	 * Post job brief
	 */
	public function actionPost($route=true)
	{
		$this->renderRoute(null, $route);
	}
	
	/**
	 * Stats for protopage feed
	 */
	public function actionStats7()
	{
		echo '<div style="font-family: arial; font-size: 10pt; text-align: center">';
		$fields = array('Technetium'=>array(Payment::model(), 'debit=1'), 'Customers'=>
			array(Person::model(), 'member="customer"'), 'Designs'=>Design::model());
		foreach ($fields as $desc=>$model) {
			$list = is_array($model) ? $model[0]->findAll($model[1]) : $model->findAll();
			echo "<p>$desc: ".count($list).'</p>';
		}
		echo '</div>';
	}
	
	/**
	 * Default action
	 */
	public function actionIndex()
	{
		if (isset($_GET['_escaped_fragment_'])) {
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: http://'.$_SERVER['HTTP_HOST'].$_GET['_escaped_fragment_'].
			'?route=0');
			die();
		}
		$this->render('index');
	}
}