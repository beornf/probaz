<?php

class PersonController extends Controller
{
	/**
	 * Defines the access control rules
	 */
	public function accessRules() 
	{
		return array(
			array('allow',
				'actions'=>array('account', 'update', 'edit'),
				'users'=>array('@'), 
			),
			array('allow', 
				'actions'=>array('reset', 'view'),
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Update account setting
	 */
	public function actionAccount()
	{
		if (app()->request->isAjaxRequest) {
			$gain = false;
			$result = array('invalid'=>array());
			if (isset($_POST['Account'])) {
				$post = $_POST['Account'];
				foreach ($post as $key=>$val) {
					if ($val == '') {
						$result['invalid'][] = $key;
					}
				}
				if (!count($result['invalid'])) {
					$model = user()->person;
					$type = $post['setting'];
					$update = false;
					if ($type == 'credit') {
						if (date('Y-m') < date($post['expirationYear'].'-'.$post['expirationMonth'])) {
							unset($post['setting']);
							$result = braintree_card($post, $model);
							if ($result->success) {
								$update = 'member_safe';
								$model->valid_card = true;
								$card = $result->creditCard;
								$credit = $card->last4.$card->expirationMonth.substr(
								$card->expirationYear, 2); $safe = $model->member_safe;
								$key = $safe ? strstr(decrypt($safe), '|', true) : '';
								$post['member_safe'] = rtrim(encrypt($key.'|'.$credit), '=');
							} else {
								$result['message'] = braintree_response($result, $post['number']);
							}
						} else {
							$result['message'] = 'Credit card has expired';
						}
					} else if ($type == 'email') {
						if (!Person::model()->count('email=:val', array(':val'=>$post['new_email'])) && 
						filter_var($post['new_email'], FILTER_VALIDATE_EMAIL)) {
							$update = 'new_email';
						} else {
							$result['invalid'][] = 'new_email';
						}
					} else if ($type == 'password') {
						if (!isset($post['current_pass']) || authorize($post['current_pass'], $model)) {
							if ($post['password'] == $post['repeat_pass']) {
								$post['password'] = passwd($post['password']);
								$model->reset_pass = null;
								$update = 'password';
							} else {
								$result['invalid'][] = 'repeat_pass';
							}
						} else {
							$result['invalid'][] = 'current_pass';
						}
					} else if ($type == 'paypal') {
						$update = 'paypal_email';
					}
					if ($update) {
						$model->$update = $post[$update];
						if ($gain = $model->save()) {
							if ($type == 'email') {
								$subject = array('Email change notice', 'Activate this email');
								$type = array('oldEmail', 'newEmail');
								queueMail(array('report'=>reportCode($type[0])), $subject[0], $type[0]);
								queueMail(array('confirm'=>confirmCode($type[1])), $subject[1], $type[1]);
							} else if ($type == 'password') {
								$subject = 'Password was changed'; $type = 'newPassword';
								queueMail(array('report'=>reportCode($type)), $subject, $type);
							}
						}
					}
				}
			}
			foreach ($result['invalid'] as $i=>$val) {
				$result['invalid'][$i] = 'Account_'.$val;
			}
			echo response($gain, $result);
		}
	}
	
	/**
	 * Reset account password
	 */
	public function actionReset($email)
	{
		if (app()->request->isAjaxRequest) {
			$gain = false;
			if ($person = Person::model()->find('email=:email', array(':email'=>$email))) {
				$gain = true; $reset = randomSalt(10);
				$person->reset_pass = substr(time(), 0, 6).$reset;
				$person->save();
				queueMail(array('reset'=>$reset), 'Temporary account login', 
				'resetPassword', $person->row_id);
			}
			echo $gain;
		}
	}
	
	/**
	 * Update person profile
	 */
	public function actionUpdate()
	{
		if (app()->request->isAjaxRequest) {
			$gain = false;
			$result = array();
			if (isset($_POST['Person'])) {
				$post = $_POST['Person'];
				$struct = array('first_name', 'last_name');
				$name = array();
				foreach ($struct as $field) {
					$post[$field] = $name[] = ucwords(strtolower($post[$field]));
				}
				$post['name'] = $post['first_name'].' '.$post['last_name'][0];
				$model = mirror(user()->person, $post);
				if (isset($post['profile'])) {
					$model->profile_id = $post['profile'];
				}
				if ($gain = $model->save()) {
					$result['notice'] = 'Your profile has been updated';
				}
			}
			echo response($gain, $result);
		}
	}
	
	/**
	 * Profile edit page
	 */
	public function actionEdit()
	{
		if (app()->request->isAjaxRequest) {
			state('ajax', array('edit', null));
			$html = Plan::Model('Profile', Plot::Edit, user()->person)->html();
			smartTitle('Edit Profile'); unsetScript('savePerson');
			echo openDiv('.profile').$html.closeDiv().renderJs();
		}
	}
	
	/**
	 * Display profile page
	 */
	public function actionView($alias=null)
	{
		if (app()->request->isAjaxRequest) {
			$person = $alias ? match_like('person', $alias) : user()->person;
			state('ajax', array('profile', $alias));
			smartTitle($person->name);
			$html = Plan::Model('Profile', $alias ? Plot::View : Plot::Edit, $person)->html();
			echo openDiv('.profile').$html.closeDiv().renderJs();
			/*$person->member == Chain::Customer ? 'Profile' : 'Portfolio';*/
		}
	}
}