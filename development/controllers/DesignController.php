<?php

class DesignController extends Controller
{
	/**
	 * Defines the access control rules
	 */
	public function accessRules() 
	{
		return array(
			array('allow', 
				'actions'=>array('crop', 'prepare'),
				'expression'=>'$user->admin',
			),
			array('allow', 
				'actions'=>array('add', 'buy', 'cart', 'view'),
				'users'=>array('@'),
			),
		);
	}
	
	/**
	 * Add new design
	 */
	public function actionAdd()
	{
		if (app()->request->isAjaxRequest) {
			$gain = false;
			$result = array();
			$file = isset($_POST['filename']) ? $_POST['filename'] : '';
			if (isset($_POST['Design']) && $file != '') {
				$name = str_replace('_' , '[ _]', substr($file, 0 , -4));
				$work = File::model()->find('hub_id=:hid AND name RLIKE :name AND salt=:salt AND type='.
				'"design"', array(':hid'=>user()->hid, ':name'=>$name, ':salt'=>substr($file, -3)));
				$found = Design::model()->count('work_id=:rid', array(':rid'=>$work->row_id));
				$task = Task::model()->find('row_id=:rid', array(':rid'=>$_POST['Design']['task_id']));
				if ($work && !$found && $task->live == 'open') {
					$count = Design::model()->count('task_id=:rid', array(':rid'=>$task->row_id));
					$model = $this->loadModel();
					$model->attributes = $_POST['Design'];
					$model->mirror = $count + 1;
					$model->person_id = user()->id;
					$model->work_id = $work->row_id;
					if ($gain = $model->save()) {
						optimizeThumb($work, $model, user()->hid);
					}
				} else if ($task->live != 'open') {
					$result['error'] = 'Submissions for "'.$task->name.'" have closed.';
				}
			} else {
				$result['error'] = 'Please upload a file before submitting.';
			}
			echo response($gain, $result);
		}
	}
	
	/**
	 * Initiate buy process
	 */
	public function actionBuy()
	{
		if (app()->request->isAjaxRequest) {
			if (isset($_POST['Task'])) {
				$task = Task::model()->findByPk($_POST['Task']['row_id']);
				$total = subTotal($task);
				if ($total && $task->person_id == user()->id && $task->status == 'closed') {
					$bought = $fix = array();
					foreach ($task->designs as $design) {
						if ($design->cart && !$design->sold) {
							$bought[] = $design;
							if (!$design->ready) $fix[] = $design->mirror;
						}
					}
					if ($url = paypal_nvp('cart', $bought, $task)) {
						$response = response(true, array('url'=>$url));
						ignore_user_abort(1);
						session_write_close();
						header('Connection: close');
						header('Content-Length: '.strlen($response));
						echo $response;
						ob_end_flush(); flush();
						if (count($fix)) {
							$route = 'store/'.hash_route(null, $task->name, 'task');
							$target = $this->createAbsoluteUrl($route, array('login'=>1));
							$body = 'Submission(s) #'.implode(', #', $fix).' need to be '.
							"manually cropped. Go to $target to settle submissions.";
							queueMail($body, 'Incomplete purchase');
						}
						foreach ($bought as $design) {
							if ($design->sold_on === null) {
								$local = param('root').'/private/'.$task->row_id;
								$dir = $local.'/pack'.$design->mirror;
								if (!is_dir($dir)) {
									mkdir($dir, 0755);
								}
								$smarty = smartyObject(array('client'=>$task->person->full, 
								'designer'=>$design->person->full, 'sold_on'=>date('j F Y')));
								$spec = array(array('pipe', 'r'), array('file', "$dir/DTA.txt", 'w'));
								$process = proc_open('fold -sw 89', $spec, $pipes);
								fwrite($pipes[0], $smarty->fetch('dta.txt'));
								fclose($pipes[0]);
								proc_close($process);
								$work = $design->work; $ext = '.'.$work->ext;
								serverProc('download', $local, $work->filename.$ext);
								$source = "$local/".$work->filename.$ext;
								if (file_exists($source)) {
									copy($source, "$dir/".$work->name.$ext);
									if (!DEV) unlink($source);
								}
								if ($design->ready == 1) makePackage($design);
								$design->sold_on = new CDbExpression('NOW()');
								$design->save();
							}
						}
					}
				}
			}
			echo response(false);
		}
	}
	
	/**
	 * Save design to cart
	 */
	public function actionCart($rid)
	{
		if (app()->request->isAjaxRequest) {
			$design = $this->loadModel($rid);
			$task = $design->task;
			$result = array('cart'=>$design->cart ? 0 : 1);
			$design->cart = $result['cart'];
			$result['success'] = $design->save();
			$result['total'] = subTotal($task) * $task->price;
			echo CJSON::encode($result);
		}
	}
	
	/**
	 * Finalize design position
	 */
	public function actionCrop()
	{
		if (app()->request->isAjaxRequest) {
			$gain = false;
			if (isset($_POST['Design'])) {
				$post = $_POST['Design'];
				$design = $this->loadModel($post['row_id']);
				$design->exactX = $post['pX'];
				$design->exactY = $post['pY'];
				$design->ready = 1;
				if ($gain = $design->save()) {
					$dir = param('root').'/private/'.$design->task->row_id;
					$edge = '+'.$design->exactX.'+'.$design->exactY;
					$file = $design->work->filename;
					serverProc('crop', $dir, array($edge, $file));
					if ($design->sold_on) makePackage($design);
				}
			}
			echo response($gain);
		}
	}
	
	/**
	 * Prepare design bounds
	 */
	public function actionPrepare($job, $mirror)
	{
		if (app()->request->isAjaxRequest) {
			$task = match_like('Task', $job);
			smartTitle('Crop for Design #'.(int)$mirror);
			$design = Design::model()->find('task_id=:tid AND mirror=:mirror', 
			array(':tid'=>$task->row_id, ':mirror'=>$mirror));
			$html = Plan::Model('Prepare', Plot::Edit, $design)->html();
			unsetScript('cropDesign');
			echo $html.renderJs();
		}
	}
	
	/**
	 * View design message
	 */
	public function actionView($job, $mirror)
	{
		if (app()->request->isAjaxRequest) {
			$task = match_like('Task', $job);
			state('ajax', array('message', array($job, $mirror)));
			smartTitle('Discussion on Submission #'.(int)$mirror);
			$design = Design::model()->find('task_id=:tid AND mirror=:mirror', 
			array(':tid'=>$task->row_id, ':mirror'=>$mirror));
			$html = Plan::Model('Message', Plot::View, $design)->html();
			echo $html.renderJs();
		}
	}
	
	/**
	 * Load model based on design id
	 */
	public function loadModel($rid=null)
	{
		return $rid !== null ? Design::model()->findByPk($rid) : new Design;
	}
}