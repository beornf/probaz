<?php

class EMultiSelect extends JuiWidget
{	
	public $model;
	public $html;
	public $column;
	public $listModel;
	public $multiple = true;
	public $active = true;
	public $call = false;
	public $compact = false;
	public $dataColumn = null;
	public $selectAll = null;
	public $selectText = null;
	public $selectNone = null;
	public $header = null;
	public $click = null;
	public $class = null;
	public $type = null;
	public $config = array();
	public $primary = 'row_id';
	public $height = 205;

	// publish and register required assets
	public function init()
	{
		parent::init();
		$assets = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets');
		Yii::app()->clientScript->registerCssFile($assets.'/jquery.multiselect.css');
		Yii::app()->clientScript->registerScriptFile($assets.'/jquery.multiselect.js');
	}
	
	// retrieves the list or an item
	protected function listData($params, $item='')
	{
		$model = isset($params['model']) ? $params['model'] : $this->model;
		$column = isset($params['column']) ? $params['column'] : 'name';
		$options = isset($params['options']) ? $params['options'] : array();
		if ($item == '') {
			if (!isset($params['values'])) {
				$keys = array_keys($options);
				$values = array_values($options);
				$criteria = new CDbCriteria;
				if (isset($options['type'])) $criteria->limit = 25;
				$criteria->order = isset($params['order']) ? $params['order'] : $column;
				for ($x = 0; $x < count($options); $x++) {
					if (is_array($values[$x])) {
						$condition = '';
						for ($y = 0; $y < count($values[$x]); $y++) {
							$val = strstr($values[$x][$y], '=');
							if ($val !== false) {
								$key = strstr($values[$x][$y], '=', true);
								$val = substr($val, 1);
							} else {
								$key = $keys[$x];
								$val = $values[$x][$y];
							}
							$param = ':val'.$x.'_'.$y;
							$condition .= ($y ? ' OR ' : '').$key.'='.$param;
							$criteria->params[$param] = $val;
						}
						$criteria->addCondition($condition);
					} else {
						$param = ':val'.$x;
						$val = $values[$x];
						if (strstr($val, ' ')) {						
							$eq = strstr($val, ' ', true);
							$val = strstr($val, ' ');
						} else {
							$eq = '=';
						}
						$criteria->addCondition($keys[$x].$eq.$param);
						$criteria->params[$param] = $val;
					}
				}
				return CHtml::listData($model->findAll($criteria), $this->primary, $column);
			} else {
				return $params['values'];
			}
		} else {
			$row = $model->find($column.'=:item', array(':item'=>$item));
			$key = $row != null ? $row[$this->primary] : false;
			return $key;
		}
	}
	
	// display multi select box with options
	public function run()
	{
		if ($this->active && !isset($this->model)) {
			return;
		}
		$id = $this->getId();
		if (!isset($this->html['id'])) {
			$this->html['id'] = $id;
		}
		$config = $this->config;
		$config['height'] = $this->height;
		$listData = isset($this->listModel) ? $this->listData($this->listModel) : array();
		$keys = array();
		if ($this->dataColumn) {
			$file = $this->dataColumn == 'file';
			foreach ($listData as $key=>$val) {
				$model = isset($this->listModel['model']) ? $this->listModel['model'] : $this->model;
				$row = $model::model()->findByPk($key);
				if ($file) {
					$type = array(Thumbs::Large(), Thumbs::Medium());
					foreach ($type as $size) {
						echo CHtml::image(ImageHelper::thumb($size, $row), $row->name);
					}
				} else if ($this->dataColumn == 'photo') {
					$keys[$key] = array('data-path'=>param('baseDir').$row['path'], 'data-ext'=>$row['ext']);
				} else {
					$keys[$key] = array('data-'.$this->dataColumn=>$row[$this->dataColumn]);
				}
			}
			if ($file) {
				return;
			}
		}
		if ($this->multiple) {
			$this->html['multiple'] = 'multiple';
			if ($this->selectText) {
				$noneSelected = $this->selectNone ? $this->selectNone : 'No '.$this->selectText.'s';
				$classes = "multi$this->class multiple".($this->compact ? ' compact' : '');
				$config = array_merge($config, array('header'=>!$this->compact, 'noneSelectedText'=>$noneSelected, 'classes'=>$classes,
					'selectedText'=>"js:function(checked, total, items) {text = checked + ' '; if (checked == total) 
					{text = 'All '} text += '$this->selectText'; if (checked != 1) {text += 's'} return text;}"));
				if ($this->selectText == 'none') {
					$config['selectedText'] = $noneSelected;
				}
			}
			if ($this->selectAll) {
				foreach ($listData as $key=>$val) {
					if (!array_key_exists($key, $keys)) {
						$keys[$key] = array();
					}
					$keys[$key]['selected'] = 'selected';
				}
			}
			if (count($keys) > 0) {
				$this->html['options'] = $keys;
			}
		} else {
			$config = array_merge($config, array('header'=>false, 'multiple'=>false, 'selectedList'=>1));
			if (!isset($config['classes'])) {
				$config['classes'] = "multi$this->class";
			}
			if ($this->header) {
				$listData[''] = $this->header;
			}
			if (!isset($this->html['options'])) {
				$this->html['options'] = $keys;
			} else if (count($keys) > 0) {
				foreach ($this->html['options'] as $key=>$array) {
					foreach ($array as $id=>$val) {
						$keys[$key][$id] = $val;
					}
				}
				$this->html['options'] = $keys;
			}
		}
		if ($this->click) {
			$config['click'] = "js: function(event, ui) { $this->click }";
		}
		$config = CJavaScript::encode($config);
		if ($this->class) {
			$this->html['class'] = $this->class;
			global $header;
			if (!is_array($header)) {
				$header = array();
			}
			if (!in_array($this->class, $header)) {
				$js = "$('.$this->class').multiselect($config)";
				if ($this->header) {
					$js .= "; $('.$this->class').multiselect('widget').find('.ui-multiselect-checkboxes li:last-child').hide()";
				}
				if ($this->call) {
					$js = "function multi$this->class() { $js}";
				}
				Yii::app()->clientScript->registerScript($this->getId(), $js, CClientScript::POS_END);
			}
			$header[] = $this->class;
		} else {
			Yii::app()->clientScript->registerScript($this->getId(), "$('#$id').multiselect($config)", CClientScript::POS_END);
		}
		if ($this->active) {
			echo EHtml::activeDropDownList($this->model, $this->column, $listData, $this->html);
		} else {
			echo EHtml::dropDownList($this->class, '', $listData, $this->html);
		}
	}
}