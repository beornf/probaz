/*
 * jQuery validate.password plug-in 1.0
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validate.password/
 *
 * Copyright (c) 2009 J�rn Zaefferer
 *
 * $Id$
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
(function($) {	
	var LETTER = /[a-zA-Z]/,
		LOWER = /[a-z]/,
		UPPER = /[A-Z]/,
		SYMBOL = /[^a-zA-Z]/,
		SAME = /^(.)\1+$/;
		
	function rating(rate, message) {
		return {
			rate: rate,
			messageKey: message
		};
	};
	
	function uncapitalize(str) {
		return str.substring(0, 1).toLowerCase() + str.substring(1);
	};
	
	$.validator.passwordRating = function(password, email) {
		var pos = email.indexOf("@");
		if ( pos > -1 )
			email = email.substr(0, pos);
		if (!password || password.length < 7)
			return rating(0, "too-short");
		if (email && password.toLowerCase().match(email.toLowerCase()))
			return rating(0, "similar-to-email");
		if (SAME.test(password))
			return rating(1, "weak");
		
		var letter = LETTER.test(password),
			lower = LOWER.test(password),
			upper = UPPER.test(uncapitalize(password)),
			symbol = SYMBOL.test(password);
				
		var good = (upper && lower) || (letter && symbol);
		if (password.length > 9 || (password.length > 8 && letter) || (password.length > 7 && good))
			return rating(4, "strong");
		if (good)
			return rating(3, "good");
		return rating(2, "easy");
	};
	
	$.validator.passwordRating.messages = {
		"similar-to-email": "Too similar to email",
		"too-short": "Too short",
		"weak": "Weak",
		"easy": "Simple",
		"good": "Almost",
		"strong": "Strong"
	};
	
	$.validator.addMethod("password", function(value, element, emailField) {
		// use untrimmed value
		var password = element.value,
		// get email for comparison, if specified
			email = $(typeof emailField != "boolean" ? emailField : []);
			
		var rating = $.validator.passwordRating(password, email.val());
		// update message for this field
		
		var meter = $(".password-meter", element.form);
		
		meter.find(".password-meter-bar").removeClass().addClass("password-meter-bar").addClass("password-meter-" + rating.messageKey);
		meter.find(".password-meter-message").removeClass().addClass("password-meter-message")
		.addClass("password-meter-message-" + rating.messageKey)
		.text($.validator.passwordRating.messages[rating.messageKey]);
		meter.show();
		// display process bar instead of error message
		
		return rating.rate > 3;
	}, ""); //"&nbsp;"
	// manually add class rule, to make email param optional
	$.validator.classRuleSettings.password = { password: true };
	
})(jQuery);

(function($) {
$.fn.EJFValidate = {
	version : 1.0,
	spice : 'EJSv_',
	normalizedNames : false,
	uniqueName : function(){
		
		if($.fn.EJFValidate.normalizedNames == false)
		{
			//alert('uniqueName : '+$.fn.EJFValidate.normalizedNames);
			$('input[type="hidden"]').each(function(){		
				// this causes a problem with hidden fields insert on purpose (and not automatically 
				// by yii. All hidden input fields names are changed.
				var e = $(this);
				e.attr('name',$.fn.EJFValidate.spice+e.attr('name'));
			});	
			$.fn.EJFValidate.normalizedNames = true;	
		}
	},
	restoreName:function(){		
		if($.fn.EJFValidate.normalizedNames == true)
		{
			//alert('restoreName'+$.fn.EJFValidate.normalizedNames);
			$('input[type="hidden"]').each(function(){		
				var e = $(this);
				e.attr('name',e.attr('name').substring($.fn.EJFValidate.spice.length));
			});	
			$.fn.EJFValidate.normalizedNames=false;
		}		
	},
	submitHandler:function(form){		
		//alert("submitHandler ...");		
		$.fn.EJFValidate.restoreName();		
		form.submit();		
	}
};	
})(jQuery);
jQuery.validator.addMethod("equalToConst", function(value, element, params) { 
	try {
		switch( element.nodeName.toLowerCase() ) {
		case 'input':
			// specific for checkbox : in Yii a checkbox value is set to 0 (checked) or 1 (unchecked)
			// As the yii required validator would always be true, user must use the 'compare' validator
			// together with a constant value : true or 1 for checked, false or 0 for unchecked. 
			if ( this.checkable(element) ){
				//alert( 'equalToConst - params : '+params+' value : '+ value+ ' isChecked : '+$(element).is(':checked'));
				var strParam = String(params);
				var paramIsTrue = (strParam.toLowerCase() === 'true' || strParam == '1');

				return    (   $(element).is(':checked') && paramIsTrue) 
						||( ! $(element).is(':checked') && !paramIsTrue);
			}
		default:
			return this.optional(element) || value == params;
		}		
	}catch(Err){
		return true;
	}

}, "Please enter value {0}"); 

/**
 * params options : 
 * 		integerOnly : (boolean)
 * 		max         : (number)
 *      min         : (number)
 *      tooBig      : (string)
 *      tooSmall    : (string)
 *      notInt      : (string)
 *      msg         : (string)      
 */
jQuery.validator.addMethod("numerical", function(value, element, params) { 

	if(value === undefined || value == '')
		return true;
	if( params.integerOnly == true && /^[-+]?[0-9]*[0-9]+$/.test(value) == false)
	{
    	//alert('no match : not integer');
    	$(element).attr('msgId',1);	// not integer
    	return false;
    }
    else if( /^[-+]?[0-9]*\.?[0-9]+$/.test(value) == false) 
    {
    	//alert('no match : not number');
    	$(element).attr('msgId',2);	// not numerical
    	return false;
    } 
	if( params.max && ( 
    		(params.integerOnly && parseInt(value) > params.max) ||
    		(parseFloat(value) > params.max)
		)){
    	//alert('out of range (over max)');
    	$(element).attr('msgId',3);	// tooBig
    	return false;
    }
	if( params.min != null && ( 
			(params.integerOnly && parseInt(value) < params.min) ||
			(parseFloat(value) < params.min)
		)){
		//alert('out of range (below min)');
		$(element).attr('msgId',4);	// tooSmall
		return false;
	}
    return true;

}, function(params, element){
		var msgId = parseInt($(element).attr('msgId'));
		$(element).removeAttr('msgId');
			
		if( msgId == 3 && params.tooBig !== undefined )
			return params.tooBig;
		if( msgId == 4 && params.tooSmall !== undefined )
			return params.tooSmall;
		if( msgId == 1 && params.notInt !== undefined)
			return params.notInt;
		if(params.msg !== undefined )	
			return params.msg;
		else
			return "please enter a numerical value";
	
	}
);
/**
 * If the regexp can't be evaluated, this rule returns true. This is because PHP and JS
 * RegExp are not 100% compatible so if the Regexp can't be used on client side, it will
 * be validate by server. 
 * params : the regexp pattern to apply to 'value'
 */
jQuery.validator.addMethod("match", function(value, element, params) {
	
	if(value === undefined || value == '')
		return true;
	var match=false;
	try{
		match = eval(params).test(value);
	}catch(err){
		return true;
	}
	return match;
}, "Please enter value {0}");

jQuery.validator.addMethod("phone", function(phone_number, element, params) {
	phone_number = phone_number.replace(/\s+/g, "");
	var format = $(params.input + "_" + params.type + "_format").val();
	return this.optional(element) || phone_number.match(new RegExp(format));
}, function(params){return "Enter valid " + params.type + " num. e.g. " + $(params.input + "_" + params.type + "_example").val()});
