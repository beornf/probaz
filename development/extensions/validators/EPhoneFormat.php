<?php
/**
 * 
 * EPhoneFormat class
 * 
 * Validate if phone number is correct format
 *
 *
 * @see      http://www.yiiframework.com
 * @version  1.0
 * @access   public
 * @author   Beorn Facchini (engineer@vulcheers.com)
 */
class EPhoneFormat extends CValidator {
	public $type;
    /**
	 * (non-PHPdoc)
	 * @see CValidator::validateAttribute()
	 */
    protected function validateAttribute($object, $attribute){
		if(!$this->checkPhoneFormat($object->$attribute)){
			$message = "Enter valid ".$this->type." number e.g. ".Yii::app()->session['geoData'][$this->type.'_example'];
			$this->addError($object, $attribute, $message);
		}
    }
    
    /**
     * Check if password is strong enough
     * @param string $password
     * @return boolean 
     */
    protected function checkPhoneFormat($phone_number){
		if (preg_match("/".Yii::app()->session['geoData'][$this->type.'_format']."/", $phone_number)) {
            return true;
        } else {
            return false;
        }
    }   
}
