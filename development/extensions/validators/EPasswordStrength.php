<?php
/**
 * 
 * EPasswordStrength class
 * 
 * Validate if password is strong enough
 *
 * The validator check if password has at least min characters,
 * and if password contain at least one lower case letter, at least one upper case letter,
 * and at least one number
 *
 *
 * @see      http://www.yiiframework.com
 * @version  1.0
 * @access   public
 * @author   ivica Nedeljkovic (ivica.nedeljkovic@gmail.com)
 */
class EPasswordStrength extends CValidator{
    
    //Minimum password length
    public $min = 8;
    
    /**
	 * (non-PHPdoc)
	 * @see CValidator::validateAttribute()
	 */
    protected function validateAttribute($object, $attribute){
       if(!$this->checkPasswordStrength($object->$attribute)){
			$message = $this->message!==null ? $this->message : Yii::t("EPasswordStrength", "{attribute} is weak. Choose a strong password having at least {$this->min} characters.");
			$this->addError($object, $attribute, $message);
       }
    }
    
    /**
     * Check if password is strong enough
     * @param string $password
     * @return boolean 
     */
    protected function checkPasswordStrength($password){
        if (preg_match("/^.*(?=.{" . $this->min . ",})(?=.*[^a-zA-Z])(?=.*[a-zA-Z]).*$/", $password) || 
			preg_match("/^.*(?=.{" . $this->min . ",})(?=.*[a-z])(?=.*[A-Z]).*$/", $password) ||
			preg_match("/^.*(?=.{" . ($this->min+2) . ",}).*$/", $password)) {
            return true;
        } else {
            return false;
        }
    }        
}

