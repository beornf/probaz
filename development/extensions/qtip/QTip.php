<?php

/**
 * Tooltip behavior. Use static method :
 * <code>
 * // all html items corresponding to the selector (input elements inside
 * // elements of class .row and containing title attribute)
 * // will have their tite attribute used to create tooltip content.
 * QTip::qtip('.row input[title]');
 * </code>
 * 
 * Or instance method. For example, create a qtip and add it to a widget :
 * $qtip = new QTip();
 * $qtip->addQTip($widget);
 * 
 * @author parcouss
 */

class QTip extends CComponent {
	/**
	 * @brief retrieve the script file name
	 * @param minify bool true to get the minified version
	 */
	protected static function scriptName($minify) {
		$ext = $minify ? '.min' : '';
		return 'jquery.qtip'.$ext;
	}
	
	/**
	 * @brief register core and qtip js needed files
	 * @param scriptName string the qtip file name
	 */
	protected static function registerScript($scriptName) {
		$assets = dirname(__FILE__).'/assets';
		$aUrl = Yii::app()->getAssetManager()->publish($assets);
		Yii::app()->clientScript->registerScriptFile($aUrl.'/'.$scriptName.'.js');
		return $aUrl;
	}
	
	/**
	 * @brief register the qtip js code needed to apply tooltip
	 * @param jsSelector string the selector jquery to select html element(s) to apply tooltips
	 * @param options array) the qtip js options
	 * @param minify bool true to select the minified js script
	 */
	public static function tip($jsSelector='', $options=array(), $minify=false) {
		$url = self::registerScript(self::scriptName($minify));
		Yii::app()->clientScript->registerCssFile($url.'/'.self::scriptName(false).'.css');

		$id = __CLASS__.$jsSelector;
		$options = CJavaScript::encode($options);
		if (strpos($jsSelector, '.') == 0) {
			$class = substr($jsSelector, 1);
			global $header;
			if (!is_array($header)) {
				$header = array();
			}
			$part = strstr($class, ' ', true);
			$class = $part !== false ? $part : $class;
			if (!in_array($class, $header)) {
				$js = ($class == 'dialogForm' ? preloadJs($class).'; ' : '')."jQuery('$jsSelector').qtip($options)";
				if (!$minify) {
					$js = "function qtip$class() { $js}";
				}
				Yii::app()->clientScript->registerScript($id, $js, CClientScript::POS_END);
			}
			$header[] = $class;
		} else {
			Yii::app()->clientScript->registerScript($id, "jQuery('$jsSelector').qtip($options);", CClientScript::POS_END);
		}
	}
	
	public $minify = true; // true to select the minified js script
	
	public $options = array(); // array general qtip js options
	
	/*public function __construct($params = array()) {
		foreach ($params as $p => $val) $this->$p = $val;
	}*/
	
	/**
	 * @brief instance method to apply qtip on a widget or on any html item. can override general options.
	 * @param widgetOrSelector mixed  a widget instance or a jquery selector
	 * @param specific_opts specific options to pass to qtip javascript code
	 */
	public function addQTip($widgetOrSelector, $specific_opts = array()) {
		$jsSelector = is_string($widgetOrSelector) ?  $widgetOrSelector : '#'.$widgetOrSelector->id;
		self::qtip($jsSelector, array_merge($this->options, $specific_opts), $this->minify);
	}
}
