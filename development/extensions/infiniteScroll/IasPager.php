<?php

class IasPager extends CLinkPager {

    private $baseUrl;
	public $listViewId;
    public $rowSelector = '.row';
    public $itemsSelector = '.items';
    public $nextSelector = '.next:not(.hidden) a';
    public $pagerSelector = '.pager';
    public $options = array();

    public function init() {

        parent::init();

        $assets = dirname(__FILE__) . '/assets';
        $this->baseUrl = Yii::app()->assetManager->publish($assets);

        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($this->baseUrl . '/jquery.ias.js', CClientScript::POS_END);

        return;
    }

    public function run() {

        $js = "jQuery.ias(" .
			CJavaScript::encode(
				CMap::mergeArray($this->options, array(
					'container' => '#' . $this->listViewId . ' ' . $this->itemsSelector,
					'item' => $this->rowSelector,
					'pagination' => '#' . $this->listViewId . ' ' . $this->pagerSelector,
					'next' => '#' . $this->listViewId . ' ' . $this->nextSelector,
					'loader' => " Loading...",
				))) . ");";


        $cs = Yii::app()->clientScript;
        $cs->registerScript('infscrl', $js, CClientScript::POS_READY);

        echo $this->header;
		
		$options = array_merge($this->htmlOptions, array('style'=>'display: none'));
        echo CHtml::tag('ul', $options, implode("\n", $this->createPageButtons()));

        echo $this->footer;
    }

}