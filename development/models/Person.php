<?php

/**
 * This is the model class for table "person".
 *
 * The followings are the available columns in table 'person':
 * @property integer $row_id
 * @property string $member
 * @property string $hub_id
 * @property string $name
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property integer $profile_id
 * @property string $birthday
 * @property string $city
 * @property string $confirm_key
 * @property string $facebook_id
 * @property string $member_safe
 * @property string $new_email
 * @property string $paypal_email
 * @property string $reset_pass
 * @property integer $valid_card
 * @property integer $valid_email
 * @property integer $locked
 * @property integer $mirror
 * @property string $create_on
 * @property string $active_on
 * @property string $login_on
 *
 * The followings are the available model relations:
 * @property Design[] $designs
 * @property Mail[] $mails
 * @property Payment[] $payments
 * @property File $profile
 * @property Hub $hub
 * @property Task[] $tasks
 * @property Wall[] $walls
 */
class Person extends CActiveRecord
{
	private $_identity;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Person the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'person';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('member, hub_id, name, first_name, last_name, email', 'required', 'on'=>'register'),
			array('email, password', 'required', 'on'=>'login'),
			array('profile_id, valid_card, valid_email, locked, mirror', 'numerical', 'integerOnly'=>true),
			array('member, hub_id', 'length', 'max'=>8),
			array('name, city', 'length', 'max'=>64),
			array('first_name, last_name, reset_pass', 'length', 'max'=>32),
			array('email, new_email, paypal_email', 'length', 'max'=>255),
			array('email, new_email, paypal_email', 'email'),
			array('email, new_email, paypal_email', 'unique'),
			// validate correct email
			array('email', 'application.extensions.jformvalidate.ECustomJsValidator', 'rules'=>array(
			'remote'=>app()->createUrl('site/validate')), 'messages'=>array('remote'=>'{attribute} was not found')),
			array('password', 'authenticate', 'on'=>'login'),
			array('password', 'length', 'max'=>60),
			// validate phone number format
			array('phone', 'application.extensions.jformvalidate.ECustomJsValidator', 'rules'=>array(
			'phone'=>array('input'=>'#Hub', 'type'=>'phone')), 'messages'=>array('none')),
			array('phone', 'filter', 'filter'=>'numeric'),
			array('phone', 'length', 'max'=>16),
			array('confirm_key', 'length', 'max'=>38),
			array('facebook_id', 'length', 'max'=>20),
			array('member_safe', 'length', 'max'=>22),
			array('birthday, create_on, active_on, login_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, member, hub_id, name, first_name, last_name, email, password, phone, profile_id, birthday, city, confirm_key, facebook_id, member_safe, new_email, paypal_email, reset_pass, valid_card, valid_email, locked, mirror, create_on, active_on, login_on', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array behaviors for timestamp attributes.
	 */
	public function behaviors()
	{
		return array(
			'timestamps'=>array(
				'class'=>'zii.behaviors.CTimestampBehavior',
				'createAttribute'=>'create_on',
				'updateAttribute'=>null,
			),
		);
	}

	/**
	 * Encrypt the password with blowfish hash.
	 */
	public function beforeSave()
	{
		if ($this->scenario == 'register' && $this->password) {
			$this->password = passwd($this->password);
		}
		return parent::beforeSave();
	}

	public function getFull()
	{
		return $this->first_name.' '.$this->last_name;
	}
	
	/**
	 * Authenticates the password.
	 */
	public function authenticate()
	{
		if (!$this->hasErrors()) {
			$this->_identity = new PrivateIdentity($this->email, $this->password);
			$this->_identity->authenticate();
			if ($this->_identity->errorCode == PrivateIdentity::ERROR_USERNAME_INVALID) {
				$this->addError('email', 'Incorrect email');
			} else if ($this->_identity->errorCode == PrivateIdentity::ERROR_PASSWORD_INVALID) {
				$this->addError('password', 'Incorrect password');
			}
		}
	}

	/**
	 * Logs in the merchant using the entered email and password.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if ($this->_identity === null) {
			$this->authenticate();
		}
		if ($this->_identity->errorCode == PrivateIdentity::ERROR_NONE) {
			// store login 1 week i.e. $this->remember_me ? 60*60*24*7 : 0;
			user()->login($this->_identity, 0);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'designs' => array(self::HAS_MANY, 'Design', 'person_id'),
			'mails' => array(self::HAS_MANY, 'Mail', 'person_id'),
			'payments' => array(self::HAS_MANY, 'Payment', 'person_id'),
			'profile' => array(self::BELONGS_TO, 'File', 'profile_id'),
			'hub' => array(self::BELONGS_TO, 'Hub', 'hub_id'),
			'tasks' => array(self::HAS_MANY, 'Task', 'person_id'),
			'walls' => array(self::HAS_MANY, 'Wall', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'member' => 'Member',
			'hub_id' => 'Hub',
			'name' => 'Name',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'password' => 'Password',
			'phone' => 'Phone',
			'profile_id' => 'Profile',
			'birthday' => 'Birthday',
			'city' => 'City',
			'confirm_key' => 'Confirm Key',
			'facebook_id' => 'Facebook',
			'member_safe' => 'Member Safe',
			'new_email' => 'New Email',
			'paypal_email' => 'Paypal Email',
			'reset_pass' => 'Reset Pass',
			'valid_card' => 'Valid Card',
			'valid_email' => 'Valid Email',
			'locked' => 'Locked',
			'mirror' => 'Mirror',
			'create_on' => 'Create On',
			'active_on' => 'Active On',
			'login_on' => 'Login On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id);
		$criteria->compare('member',$this->member,true);
		$criteria->compare('hub_id',$this->hub_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('confirm_key',$this->confirm_key,true);
		$criteria->compare('facebook_id',$this->facebook_id,true);
		$criteria->compare('member_safe',$this->member_safe,true);
		$criteria->compare('new_email',$this->new_email,true);
		$criteria->compare('paypal_email',$this->paypal_email,true);
		$criteria->compare('reset_pass',$this->reset_pass,true);
		$criteria->compare('valid_card',$this->valid_card);
		$criteria->compare('valid_email',$this->valid_email);
		$criteria->compare('locked',$this->locked);
		$criteria->compare('mirror',$this->mirror);
		$criteria->compare('create_on',$this->create_on,true);
		$criteria->compare('active_on',$this->active_on,true);
		$criteria->compare('login_on',$this->login_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}