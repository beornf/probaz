<?php

/**
 * This is the model class for table "design".
 *
 * The followings are the available columns in table 'design':
 * @property integer $row_id
 * @property integer $task_id
 * @property integer $person_id
 * @property integer $work_id
 * @property integer $pX
 * @property integer $pY
 * @property integer $pW
 * @property integer $pH
 * @property integer $exactX
 * @property integer $exactY
 * @property integer $cart
 * @property integer $message
 * @property integer $ready
 * @property integer $sold
 * @property integer $mirror
 * @property string $create_on
 * @property string $pull_on
 * @property string $sold_on
 *
 * The followings are the available model relations:
 * @property Task $task
 * @property Person $person
 * @property File $work
 * @property Wall[] $walls
 */
class Design extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Design the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'design';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('task_id, person_id, work_id', 'required'),
			array('task_id, person_id, work_id, pX, pY, pW, pH, exactX, exactY, cart, message, ready, sold, mirror', 'numerical', 'integerOnly'=>true),
			array('create_on, pull_on, sold_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, task_id, person_id, work_id, pX, pY, pW, pH, exactX, exactY, cart, message, ready, sold, mirror, create_on, pull_on, sold_on', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array behaviors for timestamp attributes.
	 */
	public function behaviors()
	{
		return array(
			'timestamps'=>array(
				'class'=>'zii.behaviors.CTimestampBehavior',
				'createAttribute'=>'create_on',
				'updateAttribute'=>null,
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
			'work' => array(self::BELONGS_TO, 'File', 'work_id'),
			'walls' => array(self::HAS_MANY, 'Wall', 'design_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'task_id' => 'Task',
			'person_id' => 'Person',
			'work_id' => 'Work',
			'pX' => 'P X',
			'pY' => 'P Y',
			'pW' => 'P W',
			'pH' => 'P H',
			'exactX' => 'Exact X',
			'exactY' => 'Exact Y',
			'cart' => 'Cart',
			'message' => 'Message',
			'ready' => 'Ready',
			'sold' => 'Sold',
			'mirror' => 'Mirror',
			'create_on' => 'Create On',
			'pull_on' => 'Pull On',
			'sold_on' => 'Sold On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('work_id',$this->work_id);
		$criteria->compare('pX',$this->pX);
		$criteria->compare('pY',$this->pY);
		$criteria->compare('pW',$this->pW);
		$criteria->compare('pH',$this->pH);
		$criteria->compare('exactX',$this->exactX);
		$criteria->compare('exactY',$this->exactY);
		$criteria->compare('cart',$this->cart);
		$criteria->compare('message',$this->message);
		$criteria->compare('ready',$this->ready);
		$criteria->compare('sold',$this->sold);
		$criteria->compare('mirror',$this->mirror);
		$criteria->compare('create_on',$this->create_on,true);
		$criteria->compare('pull_on',$this->pull_on,true);
		$criteria->compare('sold_on',$this->sold_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}