<?php

/**
 * This is the model class for table "wall".
 *
 * The followings are the available columns in table 'wall':
 * @property integer $row_id
 * @property integer $design_id
 * @property integer $task_id
 * @property integer $person_id
 * @property string $message
 * @property string $type
 * @property integer $block
 * @property string $record_on
 *
 * The followings are the available model relations:
 * @property Design $design
 * @property Task $task
 * @property Person $person
 */
class Wall extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wall the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'wall';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type', 'required'),
			array('design_id, task_id, person_id, block', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>7),
			array('message, record_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, design_id, task_id, person_id, message, type, block, record_on', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array behaviors for timestamp attributes.
	 */
	public function behaviors()
	{
		return array(
			'timestamps'=>array(
				'class'=>'zii.behaviors.CTimestampBehavior',
				'createAttribute'=>'record_on',
				'updateAttribute'=>null,
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'design' => array(self::BELONGS_TO, 'Design', 'design_id'),
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'design_id' => 'Design',
			'task_id' => 'Task',
			'person_id' => 'Person',
			'message' => 'Message',
			'type' => 'Type',
			'block' => 'Block',
			'record_on' => 'Record On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id);
		$criteria->compare('design_id',$this->design_id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('block',$this->block);
		$criteria->compare('record_on',$this->record_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}