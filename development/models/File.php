<?php

/**
 * This is the model class for table "file".
 *
 * The followings are the available columns in table 'file':
 * @property integer $row_id
 * @property string $hub_id
 * @property integer $person_id
 * @property string $type
 * @property string $name
 * @property string $ext
 * @property string $salt
 * @property string $fb
 * @property integer $copy
 * @property integer $remove
 * @property string $upload_on
 *
 * The followings are the available model relations:
 * @property Design[] $designs
 * @property Hub $hub
 * @property Hub[] $hubs
 * @property Person[] $people
 * @property Task[] $tasks
 */
class File extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return File the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hub_id, type, name, ext', 'required'),
			array('person_id, copy, remove', 'numerical', 'integerOnly'=>true),
			array('hub_id, ext', 'length', 'max'=>8),
			array('type', 'length', 'max'=>6),
			array('name, fb', 'length', 'max'=>255),
			array('salt', 'length', 'max'=>3),
			array('upload_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, hub_id, person_id, type, name, ext, salt, fb, copy, remove, upload_on', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array behaviors for timestamp attributes.
	 */
	public function behaviors()
	{
		return array(
			'timestamps'=>array(
				'class'=>'zii.behaviors.CTimestampBehavior',
				'createAttribute'=>'upload_on',
				'updateAttribute'=>null,
			),
		);
	}
	
	public function getDisplay()
	{
		return $this->name.($this->copy > 1 ? ' ('.$this->copy.')' : '').'.'.$this->ext;
	}
	
	public function getFilename()
	{
		return ($this->fb ? $this->fb : strtolower(str_replace(' ', '_', $this->name)).
		'_'.$this->salt);
	}
	
	public function getPath()
	{
		return '/uploads/'.$this->hub_id.'/'.$this->filename;
	}

	public function getUrl()
	{
		return $this->hub->url.$this->fb.'.'.$this->ext;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'designs' => array(self::HAS_MANY, 'Design', 'work_id'),
			'hub' => array(self::BELONGS_TO, 'Hub', 'hub_id'),
			'hubs' => array(self::HAS_MANY, 'Hub', 'logo_id'),
			'people' => array(self::HAS_MANY, 'Person', 'profile_id'),
			'tasks' => array(self::MANY_MANY, 'Task', 'task_photo(photo_id, task_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'hub_id' => 'Hub',
			'person_id' => 'Person',
			'type' => 'Type',
			'name' => 'Name',
			'ext' => 'Ext',
			'salt' => 'Salt',
			'fb' => 'Fb',
			'copy' => 'Copy',
			'remove' => 'Remove',
			'upload_on' => 'Upload On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id);
		$criteria->compare('hub_id',$this->hub_id,true);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('fb',$this->fb,true);
		$criteria->compare('copy',$this->copy);
		$criteria->compare('remove',$this->remove);
		$criteria->compare('upload_on',$this->upload_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}