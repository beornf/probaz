<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property string $row_id
 * @property string $name
 * @property string $phone
 * @property string $mobile
 * @property integer $flag
 * @property integer $postsize
 * @property integer $sectsize
 *
 * The followings are the available model relations:
 * @property City[] $cities
 * @property Hub[] $hubs
 * @property Location[] $locations
 * @property Postal[] $postals
 * @property Sector[] $sectors
 */
class Country extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Country the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('row_id, name', 'required'),
			array('flag, postsize, sectsize', 'numerical', 'integerOnly'=>true),
			array('row_id', 'length', 'max'=>2),
			array('name', 'length', 'max'=>64),
			array('phone, mobile', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, name, phone, mobile, flag, postsize, sectsize', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cities' => array(self::HAS_MANY, 'City', 'country_code'),
			'hubs' => array(self::HAS_MANY, 'Hub', 'country_code'),
			'locations' => array(self::HAS_MANY, 'Location', 'country_code'),
			'postals' => array(self::HAS_MANY, 'Postal', 'country_code'),
			'sectors' => array(self::HAS_MANY, 'Sector', 'country_code'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'name' => 'Name',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'flag' => 'Flag',
			'postsize' => 'Postsize',
			'sectsize' => 'Sectsize',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('flag',$this->flag);
		$criteria->compare('postsize',$this->postsize);
		$criteria->compare('sectsize',$this->sectsize);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}