<?php

/**
 * This is the model class for table "payment".
 *
 * The followings are the available columns in table 'payment':
 * @property integer $row_id
 * @property integer $task_id
 * @property integer $person_id
 * @property string $amount
 * @property integer $debit
 * @property string $fee
 * @property string $mode
 * @property integer $commit
 * @property string $paid_on
 *
 * The followings are the available model relations:
 * @property Task $task
 * @property Person $person
 */
class Payment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Payment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('task_id, person_id', 'required'),
			array('task_id, person_id, debit, commit', 'numerical', 'integerOnly'=>true),
			array('amount, fee', 'length', 'max'=>9),
			array('mode', 'length', 'max'=>6),
			array('paid_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, task_id, person_id, amount, debit, fee, mode, commit, paid_on', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array behaviors for timestamp attributes.
	 */
	public function behaviors()
	{
		return array(
			'timestamps'=>array(
				'class'=>'zii.behaviors.CTimestampBehavior',
				'createAttribute'=>'paid_on',
				'updateAttribute'=>'paid_on',
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'task_id' => 'Task',
			'person_id' => 'Person',
			'amount' => 'Amount',
			'debit' => 'Debit',
			'fee' => 'Fee',
			'mode' => 'Mode',
			'commit' => 'Commit',
			'paid_on' => 'Paid On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('debit',$this->debit);
		$criteria->compare('fee',$this->fee,true);
		$criteria->compare('mode',$this->mode,true);
		$criteria->compare('commit',$this->commit);
		$criteria->compare('paid_on',$this->paid_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}