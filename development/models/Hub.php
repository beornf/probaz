<?php

/**
 * This is the model class for table "hub".
 *
 * The followings are the available columns in table 'hub':
 * @property string $row_id
 * @property string $name
 * @property string $dscpt
 * @property string $url
 * @property integer $logo_id
 * @property string $country_code
 * @property string $create_on
 *
 * The followings are the available model relations:
 * @property File[] $files
 * @property File $logo
 * @property Country $countryCode
 * @property Person[] $people
 * @property Task[] $tasks
 */
class Hub extends CActiveRecord
{
	public $first_name;
	public $last_name;
	public $business;
	public $email;
	public $password;
	public $city;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Hub the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hub';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('row_id, first_name, last_name, email, city, country_code', 'required'),
			array('password', 'required', 'on'=>'register'),
			array('logo_id', 'numerical', 'integerOnly'=>true),
			array('row_id', 'length', 'max'=>8),
			array('name', 'length', 'max'=>48),
			array('dscpt, url, email', 'length', 'max'=>255),
			array('country_code', 'length', 'max'=>2),
			array('email', 'email'),
			array('password', 'length', 'max'=>60),
			array('password', 'application.extensions.jformvalidate.ECustomJsValidator', 'rules'=>array(
			'password'=>'#Hub_email'), 'messages'=>array('password'), 'on'=>'business, person, site'),
			array('password', 'ext.validators.EPasswordStrength', 'on'=>'business, person, site'),
			array('url', 'url', 'defaultScheme'=>'http', 'message'=>'Enter a valid URL address'),
			// validate unique fields
			array('name', 'unique'),
			/*array('email', 'required', 'message'=>'Contact email is required', 'on'=>'register'),
			array('password_confirm', 'compare', 'compareAttribute'=>'password'),
			array('name, email', 'application.extensions.jformvalidate.ECustomJsValidator', 'rules'=>array(
			'remote'=>app()->createUrl('hub/validate')), 'messages'=>array('remote'=>'{attribute} is already taken')),*/
			array('create_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, name, dscpt, url, logo_id, country_code, create_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array behaviors for timestamp attributes.
	 */
	public function behaviors()
	{
		return array(
			'timestamps'=>array(
				'class'=>'zii.behaviors.CTimestampBehavior',
				'createAttribute'=>'create_on',
				'updateAttribute'=>false,
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'files' => array(self::HAS_MANY, 'File', 'hub_id'),
			'logo' => array(self::BELONGS_TO, 'File', 'logo_id'),
			'countryCode' => array(self::BELONGS_TO, 'Country', 'country_code'),
			'people' => array(self::HAS_MANY, 'Person', 'hub_id'),
			'tasks' => array(self::HAS_MANY, 'Task', 'hub_id'),
			//filter example: 'hub_union' => array(self::HAS_MANY, 'HubUnion', 'hub_id', 'condition'=>
			//'user_id = :uid AND request=1', 'params'=>array(':uid'=>app()->user->getId())),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'name' => 'Business Name',
			'dscpt' => 'Dscpt',
			'url' => 'Web URL',
			'logo_id' => 'Logo',
			'country_code' => 'Country Code',
			'create_on' => 'Create On',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'password' => 'Password',
			'password_confirm' => 'Confirm Password',
			'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('dscpt',$this->dscpt,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('logo_id',$this->logo_id);
		$criteria->compare('country_code',$this->country_code,true);
		$criteria->compare('create_on',$this->create_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}