<?php

/**
 * This is the model class for table "mail".
 *
 * The followings are the available columns in table 'mail':
 * @property integer $row_id
 * @property string $layout
 * @property string $query
 * @property string $subject
 * @property integer $sent
 * @property integer $person_id
 * @property integer $thread
 * @property string $queue_on
 * @property string $sent_on
 *
 * The followings are the available model relations:
 * @property Person $person
 */
class Mail extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Mail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('query, subject, person_id', 'required'),
			array('sent, person_id, thread', 'numerical', 'integerOnly'=>true),
			array('layout', 'length', 'max'=>32),
			array('subject', 'length', 'max'=>48),
			array('queue_on, sent_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, layout, query, subject, sent, person_id, thread, queue_on, sent_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'layout' => 'Layout',
			'query' => 'Query',
			'subject' => 'Subject',
			'sent' => 'Sent',
			'person_id' => 'Person',
			'thread' => 'Thread',
			'queue_on' => 'Queue On',
			'sent_on' => 'Sent On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id);
		$criteria->compare('layout',$this->layout,true);
		$criteria->compare('query',$this->query,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('sent',$this->sent);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('thread',$this->thread);
		$criteria->compare('queue_on',$this->queue_on,true);
		$criteria->compare('sent_on',$this->sent_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}