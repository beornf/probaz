<?php

/**
 * This is the model class for table "task".
 *
 * The followings are the available columns in table 'task':
 * @property integer $row_id
 * @property string $hub_id
 * @property integer $person_id
 * @property string $name
 * @property string $price
 * @property integer $category_id
 * @property string $needs
 * @property string $status
 * @property integer $mirror
 * @property string $create_on
 * @property string $update_on
 * @property string $deadline_on
 * @property string $phase_on
 *
 * The followings are the available model relations:
 * @property Design[] $designs
 * @property Payment[] $payments
 * @property Category $category
 * @property Hub $hub
 * @property Person $person
 * @property File[] $files
 * @property Wall[] $walls
 */
class Task extends CActiveRecord
{
	public $log = true;
	public $deadline;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hub_id, name, price, category_id', 'required'),
			array('person_id, category_id, mirror', 'numerical', 'integerOnly'=>true),
			array('hub_id, status', 'length', 'max'=>8),
			array('name', 'length', 'max'=>48),
			array('price', 'length', 'max'=>9),
			array('price', 'numerical', 'min'=>300, 'max'=>99999),
			array('needs, create_on, update_on, deadline_on, phase_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('row_id, hub_id, person_id, name, price, category_id, needs, status, mirror, create_on, update_on, deadline_on, phase_on', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array behaviors for timestamp attributes.
	 */
	public function behaviors()
	{
		return array(
			'timestamps'=>array(
				'class'=>'application.components.TaskLogBehavior',
				'createAttribute'=>'create_on',
				'updateAttribute'=>'update_on',
			),
		);
	}
	
	public function getLive()
	{
		if ($this->status != 'archived') {
			$phase = app()->db->createCommand()->select('phase_on < NOW()')->from('task')
			->where('row_id=:rid', array('rid'=>$this->row_id))->queryScalar();
			if ($phase) {
				$this->log = false;
				$set = 'archived';
				if ($this->status == 'open') {
					$this->phase_on = date('Y-m-d H:i:s', strtotime('+7 days', 
					strtotime($this->phase_on)));
					$set = 'closed';
				}
				$this->status = $set;
				$this->save();
			}
		}
		return $this->status;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'designs' => array(self::HAS_MANY, 'Design', 'task_id'),
			'payments' => array(self::HAS_MANY, 'Payment', 'task_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'hub' => array(self::BELONGS_TO, 'Hub', 'hub_id'),
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
			'files' => array(self::MANY_MANY, 'File', 'task_photo(task_id, photo_id)', 'order' => 'rank'),
			'walls' => array(self::HAS_MANY, 'Wall', 'task_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'row_id' => 'Row',
			'hub_id' => 'Hub',
			'person_id' => 'Person',
			'name' => 'Name',
			'price' => 'Price',
			'category_id' => 'Category',
			'needs' => 'Needs',
			'status' => 'Status',
			'mirror' => 'Mirror',
			'create_on' => 'Create On',
			'update_on' => 'Update On',
			'deadline_on' => 'Deadline On',
			'phase_on' => 'Phase On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('row_id',$this->row_id);
		$criteria->compare('hub_id',$this->hub_id,true);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('needs',$this->needs,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('mirror',$this->mirror);
		$criteria->compare('create_on',$this->create_on,true);
		$criteria->compare('update_on',$this->update_on,true);
		$criteria->compare('deadline_on',$this->deadline_on,true);
		$criteria->compare('phase_on',$this->phase_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}