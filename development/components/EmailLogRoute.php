<?php
/**
 * EmailLogRoute is the email route class.
 */
class EmailLogRoute extends CEmailLogRoute
{
	protected function processLogs($logs)
	{
		if (empty($logs)) {
			return;
		}
		parent::processLogs($logs);
	}
	
	protected function sendEmail($email,$subject,$message)
	{
		if ($from = $this->getSentFrom()) {
			$headers = $this->getHeaders(); $headers[] = "From: {$from}";
			mail($email, $subject, $message, implode("\r\n", $headers), '-f '.UBER);
		} else {
			parent::sendEmail($email, $subject, $message);
		}
	}
}