<?php
/**
 * LogFilter is the customized base log filter class.
 */
class LogFilter extends CLogFilter
{
	public $globals = array(
		'_SERVER'=>array(true, 'HTTP_USER_AGENT', 'REMOTE_ADDR', 'REQUEST_URI'),
		'_SESSION'=>array(true, 'customer'=>array('city', 'country')),
		//'_COOKIE'=>array(false, COOKIE),
	);
	
	public function filter(&$logs)
	{
		foreach (app()->params['deny'] as $deny) {
			if (eval('return $block='.$deny.';')) {
				$logs = null;
			}
		}
		return parent::filter($logs);
	}
	
	protected function format(&$logs)
	{
		parent::format($logs);
		$stack = count($logs) > 1 ? 1 : 0;
		$logs[$stack][0] = str_replace("\n", "\n\n", $logs[$stack][0]);
	}
	
	protected function getContext()
	{
		$context = array();
		foreach ($this->globals as $array=>$items) {
			if (!empty($GLOBALS[$array])) {
				$global = $GLOBALS[$array];
				$exclude = $items[0];
				$keys = array_slice($items, 1);
				foreach ($global as $id=>$val) {
					if (key_exists($id, $keys)) {						
						foreach ($global[$id] as $key=>$opt) {
							if (!in_array($key, $keys[$id])) {
								unset($global[$id][$key]);
							}
						}
					}
					else if ($exclude != in_array($id, $keys)) {
						unset($global[$id]);
					}
				}
				if (count($global)) $context[] = "\${$array}=".var_export($global, true);
			}
		}
		$out = implode("\n\n", $context);
		return $out != '' ? "$out\n" : '';
	}
}