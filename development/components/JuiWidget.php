<?php

Yii::import('zii.widgets.jui.CJuiWidget');

class JuiWidget extends CJuiWidget
{
	public function init() {
		$this->theme = 'probaz';
		$this->themeUrl = app()->getAssetManager()->publish(web_root().'/css/jui');
		parent::init();
	}
}