<?php
/**
 * Displays an image with jCrop sizing controls
 */
class JCrop extends CWidget {
	
	/**
	 * Image id rendered into the <img> tag
	 * @var string $imageId
	 */
	public $imageId = 'jcrop';

	/**
	 * Specifies the image URL that is displayed in the <img> tag
	 * @var string $imageUrl
	 */
	public $imageUrl;
	
	/**
	 * Preview thumbnail dimensions
	 * @var int $thumbSize
	 */
	public $thumbSize;
	
	/**
	 * key => value options that are rendered into the <img> tag
	 * @var array
	 */
	public $htmlOptions = array();
	
	public function run() {
		$hidden = array('x', 'y', 'w', 'h');
		$id = $this->imageId;
		if ($this->thumbSize) {
			$hidden[] = 'filename';
			$thumbId = "$id-thumb";
			$moveCoords = <<<EOF
function moveCoords(c)
{
	$('#{$thumbId}').css({
		marginLeft: '-' + Math.round({$this->thumbSize} * c.x / c.w) + 'px',
		marginTop: '-' + Math.round({$this->thumbSize} * c.y / c.h) + 'px'
	});
};
EOF;
			Yii::app()->clientScript->registerScript('moveCoords', $moveCoords);
		}
		
		$this->htmlOptions['id'] = $id;
		$saveCoords = "function saveCoords(c)\n{\n";
		foreach ($hidden as $key) {
			if (strlen($key) == 1) {
				$field = 'p'.ucwords($key);
				$saveCoords .= "\tjQuery('#Design_$field').val(Math.round(c.$key));\n";
				echo CHtml::activeHiddenField(new Design, $field);
			} else {
				echo CHtml::hiddenField($key, '');
			}
		}
		$saveCoords .= "\tmoveCoords(c);\n}"; 
		Yii::app()->clientScript->registerScript('saveCoords', $saveCoords);
		echo CHtml::image($this->imageUrl, '', $this->htmlOptions);
	}
	
	public static function Script($path, $src) {
		$size = array_slice(getimagesize($path), 0, 2);
		$mid = bisect($size[0], $size[1]);
		$box = array(Thumbs::Jcrop); $vert = $size[1] > $size[0];
		$box[] = round($size[($vert + 1) % 2] / $size[$vert] * $box[0]);
		list($diff, $offset) = bisect(Thumbs::Stage, $box[0] - $box[1] - 1);
		list($width, $height) = $vert ? array_reverse($box) : $box;
		$jcrop = array('allowSelect'=>false, 'allowResize'=>false, 'trueSize'=>$size, 
		'setSelect'=>array($mid[0] - $diff, $mid[1] - $diff, $mid[0] + $diff, $mid[1] + 
		$diff), 'onChange'=>'js:moveCoords', 'onSelect'=>'js:saveCoords');
		
		$options = CJavaScript::encode($jcrop); $tag = '#jcrop';
		$js = "jQuery('$tag').attr('src', '$src').attr('width', $width).attr('height', ".
		"$height); crop = $.Jcrop('$tag', $options); jQuery('.jcrop-holder')".
		".css('margin-".($vert ? 'left' : 'top')."', $offset)";
		$mult = Thumbs::Preview / Thumbs::Stage;
		list($width, $height) = array(round($size[0] * $mult), round($size[1] * $mult));
		return $js."; jQuery('$tag-thumb').attr('src', '$src').attr('width', $width)".
		".attr('height', $height); ";
	}
}