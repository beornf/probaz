<?php
/**
 * ControlFilter is the customized access control filter class.
 */
class ControlFilter extends CAccessControlFilter
{
	protected function accessDenied($user, $message)
	{
		throw new CHttpException(403, $message);
	}
}