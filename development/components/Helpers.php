<?php
/**
 * Helpers file holds all functions that are autoloaded for the application.
 */

function app()
{
	return Yii::app();
}

function cs()
{
	return Yii::app()->clientScript;
}

function user()
{
	return Yii::app()->getUser();
}

function state($key, $val=null)
{
	if ($val !== null) {
		Yii::app()->session[$key] = $val;
	} else {
		return Yii::app()->session[$key];
	}
}

function state_new($key, $val=null)
{
	$new = !isset(Yii::app()->session[$key]);
	if ($val !== null && $new) Yii::app()->session[$key] = $val;
	return $new;
}

function abs_path()
{
	return '.'.(!DEV ? base_path() : '');
}

function base_path($full=0)
{
	return local() ? '.' : (($full ? web_root() : '').'/'.(DEV ? 'uber' : 'vault'));
}

function client($key=false, $quest=false)
{
	$val = state('customer');
	if ($key) {
		$val = $val[$key];
		if ($quest) {
			$country = Country::model()->find("row_id='$val'");
			$val = $country->name;
		}
	}
	return $val;
}

function cookie($val=false)
{
	if ($val) Yii::app()->request->cookies[COOKIE] = $val;
	if (Yii::app()->request->cookies[COOKIE]) {
		return Yii::app()->request->cookies[COOKIE];
	}
	return false;
}

function filter($value, $type=false)
{
	$key = $type == 'agent' ? 'HTTP_USER_AGENT' : ($type == 'uri' ? 
	'REQUEST_URI' : 'REMOTE_ADDR');
	return isset($_SERVER[$key]) ? stripos($_SERVER[$key], $value) !== false : false;
}

function host()
{
	return str_replace('www.', '', isset($_SERVER['HTTP_HOST']) ? 
	$_SERVER['HTTP_HOST'] : 'mail');
}

function ie_version()
{
	if (isset($_SERVER['HTTP_USER_AGENT'])) {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		if ($pos = strpos($agent, 'MSIE')) {
			return substr($agent, $pos + 5, 1);
		}
	}
	return false;
}

function local()
{
	return host() == '127.0.0.1';
}

function merchant()
{
	$name = host();
	$host = strstr($name, '.', true);
	return ucwords($host ? $host : $name);
}

function param($key, $array=false)
{
	if (isset(Yii::app()->params[$key])) {
		return Yii::app()->params[$key];
	} else {
		$default = $array ? array() : false; /*true*/
		Yii::app()->params[$key] = $default;
		return $default;
	}
}

function paypal_fee($amount)
{
	$profit = param('profit'); $net = 1.0 - $profit;
	$fee = param('ccFee') + param('ppFee') / $amount;
	return number_format($amount * ($profit + $net * $fee), 2);
}

function paypal_nvp($cmd, $data, $task=null)
{
	$entry = param('ppDomain');
	if (!defined('API_ENDPOINT')) define('API_ENDPOINT', "https://api-3t.$entry/nvp");
	require_once('CallerService.php');
	if ($cmd == 'cart') {
		$method = 'SetExpressCheckout';
		$grid = 'store/'.hash_route(null, $task->name, 'task');
		$total = number_format($task->price * count($data), 2);
		$request = array(
			'returnUrl'=>app()->createAbsoluteUrl($grid, array('paid'=>1)),
			'cancelUrl'=>app()->createAbsoluteUrl($grid),
			'landingPage'=>'Billing',
			'reqConfirmShipping'=>0,
			'noShipping'=>1,
			'paymentRequest_0_amt'=>$total,
			'paymentRequest_0_itemamt'=>$total,
			'paymentRequest_0_currencyCode'=>param('currency'),
			'paymentRequest_0_paymentAction'=>'Sale',
			'paymentRequest_0_invNum'=>$task->row_id,
		);
		if (!local()) {
			$request['hdrImg'] = app()->request->hostInfo.'/images/header.png';
		}
		$name = $task->category->name.' by ';
		foreach ($data as $i=>$design) {
			$request = array_merge($request, array(
				"L_paymentRequest_0_name$i"=>$name.$design->person->name,
				"L_paymentRequest_0_amt$i"=>number_format($task->price, 2),
				"L_paymentRequest_0_qty$i"=>1,
				"L_paymentRequest_0_itemCategory$i"=>'Digital',
				"L_paymentRequest_0_number$i"=>$design->mirror,
			));
		}
	} else if ($cmd == 'get') {
		$method = 'GetExpressCheckoutDetails';
		$request = array('token'=>$data);
	} else if ($cmd == 'make') {
		$method = 'DoExpressCheckoutPayment';
		$find = array_upper(array('paymentRequest_0_', 'payerId', 'token'));
		$request = array_slice(find_keys($data, $find[0]), 0, 2);
		$request['paymentRequest_0_paymentAction'] = 'Sale';
		foreach (array_slice($find, 1) as $key) $request[$key] = $data[$key];
	} else if ($cmd == 'pay') {
		$method = 'MassPay';
		$request = array(
			'token'=>$data,
			//'something'=>$abc,
		);
	}
	$request = array_change_key_case($request, CASE_UPPER);
	$nvp = '&'.http_build_query($request, '', '&');
	if ($cmd != 'cart') echo $nvp.';';
	$result = hash_call($method, $nvp);
	if (isset($result['ACK']) && $result['ACK'] == 'Success') {
		if ($cmd == 'get') {
			list($amt, $num, $inv) = array_upper(array('paymentRequest_0_amt', 
			'L_paymentRequest_0_number', 'invNum'));
			return array_merge($result, array(
				'amount'=>$result[$amt],
				'designs'=>array_values(find_keys($result, $num)),
				'task_id'=>$result[$inv],
			));
		} else if ($cmd == 'cart') {
			return "https://www.$entry/webscr&cmd=_express-checkout".
			'&useraction=commit&token='.$result['TOKEN'];
		} else if ($cmd == 'make') {
			return true;
		}
	} else {
		print_r($result);
	}
	return false;
}

function match_like($model, $route)
{
	$repeat = is_string($model) ? 1 : 0;
	$class = $repeat ? $model : get_class($model);
	$person = $class == 'person';
	if ($repeat) {
		preg_match('/'.(!$person ? '_' : '').'(\d+)$/', $route, $match, PREG_OFFSET_CAPTURE);
		$class = ucwords($class);
		if (count($match)) {
			list($repeat, $length) = $match[0];
			if (!$person) $repeat = substr($repeat, 1);
			$route = substr($route, 0, $length);
		}
	}
	if ($person) {
		preg_match_all('/[A-Z]/', $route, $match, PREG_OFFSET_CAPTURE);
		$route = strtolower($route);
		$count = 0;
		foreach ($match[0] as $pos) {
			if ($pos[1]) {
				$split = $pos[1] + $count;
				$route = substr($route, 0, $split).'-'.substr($route, $split);
				$count++;
			}
		}
	}
	$route = str_replace('.', '[[:alnum:]]+', $route);
	$case = '^'.str_replace('-', '[^[:alnum:]]+', $route).'$';
	if ($repeat) {
		return $class::model()->find('name RLIKE :case AND mirror=:repeat',
		array(':case'=>$case, ':repeat'=>$repeat));
	} else {
		return array($case, $class);
	}
}

function mirror($model, $post, $key='name')
{
	$value = $post[$key];
	if ($model->$key !== $value) {
		list($case, $type) = match_like($model, $value);
		$model->mirror = $type::model()->count("$key RLIKE :case", 
		array(':case'=>$case)) + 1;
	}
	$model->attributes = $post;
	return $model;
}

function nice_date($stamp, $desc=true)
{
	$period = array('second', 'minute', 'hour', 'day', 'week', 'month', 'year');
	$length = array(60, 60, 24, 7, 4.348, 12, 100);
	$now = time(); $date = strtotime($stamp);
	$future = $now < $date; 
	$diff = abs($now - $date);
	if (!$stamp || $diff < 3) return $stamp ? 'Right now' : '';
	for ($i = 0; $diff >= $length[$i]; $i++) {
		$diff /= $length[$i];
	}
	$diff = round($diff);
	$add = $desc ? ($diff > 1 ? 's' : '').' '.($future ? ($i > 3 ? 
	'remain'.($diff == 1 ? 's' : '') : 'left') : 'ago') : '';
	return "$diff $period[$i]$add";
}

function price($value)
{
	return number_format($value, 2, '.', '');
}

function ref_link($url)
{
	return CHtml::link($url, $url, array('target'=>'_blank'));
}

function sql_date($stamp, $format=false)
{
	if ($format) {
		return $stamp ? date($format, strtotime($stamp)) : '';
	} else {
		return date('Y-m-d', strtotime(strstr($stamp, ' ')));
	}
}

function web_root()
{
	return dirname(__FILE__).'/../../../httpdocs';
}

function verify($resend=0)
{
	$person = user()->person;
	if ($resend) {
		$time = substr(time(), 0, 6);
		return $person->confirm_key ? substr($person->confirm_key, 32) <= $time : true;
	} else {
		return $person->new_email || !$person->valid_email;
	}
}

function facebook_api($key='profile')
{
	$facebook = new Facebook(array(
		'appId'=>app()->params['fbId'],
		'secret'=>'d37ea942bd65d830c14940acae3bf94a',
	));
	if ($facebook->getUser()) {
		try {
			if ($key == 'profile') {
				return $facebook->api('/me');
			} else if ($key == 'logout') {
				return $facebook->getLogoutUrl(array('next'=>app()->createAbsoluteUrl('')));
			}
		} catch (FacebookApiException $e) {}
	}
	return false;
}

function fetchLocation()
{
	if ($cookie = cookie()) {
		$customer = decrypt($cookie->value, false);
		$cookie = indexSet($customer, array('hid', 'city', 'country'));
	}
	if (!$cookie) {
		$hub_id = randomKey(8, Hub::model());
		$ipLite = new ip2location_lite;
		$ipLite->setKey(app()->params['dbKey']);
		$location = $ipLite->getCity($_SERVER['REMOTE_ADDR']);
		if (isset($location['statusCode']) && $location['statusCode'] == 'OK') {
			$customer = array('hid'=>$hub_id);
			$customer['city'] = $location['cityName'] != '-' ? titleCase($location['cityName']) : 'Singapore';
			$customer['country'] = $location['countryCode'] != '-' ? $location['countryCode'] : 'SG';
			$model = Country::model()->find('row_id=:rid OR name=:name', array(':rid'=>
				$customer['country'], ':name'=>$location['countryName']));
			if ($model === null) {
				app()->db->createCommand()->insert('country', array('row_id'=>
				$customer['country'], 'name'=>titleCase($location['countryName'])));
			} else {
				$customer['country'] = $model->row_id;
			}
			app()->db->createCommand()->insert('hub', array('row_id'=>$hub_id, 'country_code'=>
			$customer['country'], 'create_on'=>new CDbExpression('NOW()')));
			//'latitude'=>$location['latitude'], 'longitude'=>$location['longitude']
			
			// store data in cookie
			$cookie = new CHttpCookie(COOKIE, encrypt($customer, false));
			$cookie->expire = time()+60*60*24*7;
			cookie($cookie);
		} else {
			//echo $ipLite->getError();
			return;
		}
	}
	state('customer', $customer);
}

function hash_route($base, $item='', $type='category')
{
	$route = $desc = '';
	if ($item != '') {
		if ($type == 'category') {
			foreach (explode('/', $item, 2) as $str) {
				list($desc) = explode(' ', trim($str));
				$route .= ($route != '' ? '-' : '').preg_replace('/[^a-z]+/', '', strtolower($desc));
			}
		} else {
			$sep = $type != 'profile' ? '_' : ''; $tail = '';
			if (is_array($item)) {
				$tail = '/'.sprintf("%02d", $item[1]->mirror);
				$item = $item[0];
			}
			$desc .= !is_string($item) ? ($item->name.($item->mirror > 1 ? $sep.
			$item->mirror : '')).$tail : $item;
			$desc = preg_replace('/[^[:print:]]+/', '.', strtolower($desc));
			$route = preg_replace('/[^a-z0-9\s\/_.]+/', '-', $desc);
			if ($type == 'task') {
				$route = preg_replace('/[\s-]+/', '-', $route);
			} else if ($type == 'profile') {
				$split = explode(' ', $route);
				foreach ($split as $key=>$val) {
					$split[$key] = ucwords($val);
				}
				$route = preg_replace('/\s+/', '', implode(' ', $split));
			}
		}
	}
	$close = $base != '' && $item != '' ? '/' : '';
	return ($base !== null ? app()->baseUrl.'/#!/'.$base.$close : '').$route;
}

function loginProfile($person)
{
	if ($fid = $person->facebook_id) {
		$ch = curl_init("http://graph.facebook.com/$fid/picture?type=large");
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_exec($ch);
		$url = substr(strrchr(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), '/'), 1);
		curl_close($ch);
		$ext = strtolower(substr(strstr($url, '.'), 1));
		$fb = strstr($url, '.', true);
		$profile = File::model()->find('hub_id=:hub AND person_id=:rid', 
		array(':hub'=>'akamaihd', ':rid'=>$person->row_id));
		if ($profile === null) {
			$profile = new File;
			$profile->hub_id = 'akamaihd';
			$profile->person_id = $person->row_id;
			$profile->name = 'Facebook Pic';
			$profile->type = 'image';
			$profile->ext = $ext;
			$profile->fb = $fb;
			$profile->save();
			$profile->refresh();
			$person->profile_id = $profile->row_id;
			$person->save();
		} else if ($profile->fb != $fb) {
			$profile->ext = $ext;
			$profile->fb = $fb;
			$profile->save();
		}
	}
	$identity = PrivateIdentity::createIdentity($person->row_id, $person->name);
	user()->login($identity, 0);
}

function registerProfile($post, $member, $fb=false)
{
	$transaction = app()->db->beginTransaction();
	$model = hubModel($fb);
	$model->attributes = $post;
	$model->row_id = randomKey(8, Hub::model());
	$model->country_code = client('country');
	$person = Person::model()->find('email=:addr', array(':addr'=>$post['email']));
	$live = param('live'); $exist = $person !== null;
	if ($exist || $model->save()) {
		if ($exist) {
			if ($fb && $person->facebook_id === null) {
				$person->facebook_id = $post['facebook_id'];
			} else if ($person->password === null) {
				$person->password = $post['password'];
				$person->scenario = 'register';
			} else {
				$person = null;
			}
		}
		if ($person === null) {
			$save = array('first_name', 'last_name');
			$name = array();
			foreach ($save as $field) {
				$post[$field] = $name[] = ucwords(strtolower($post[$field]));
			}
			$post['name'] = $name[0].' '.$name[1][0];
			$person = mirror(personModel(0), $post);
			$person->attributes = $post;
			$person->member = $member;
			$person->hub_id = $model->row_id;
			$city = isset($post['city']) ? $post['city'] : client('city');
			$code = City::model()->find('name=:city', array(':city'=>$city));
			if ($code === null) {
				app()->db->createCommand()->insert('city', array('name'=>$city, 
				'country_code'=>client('country')));
			}
			$person->city = $city;
			if (!$live) $person->locked = 1;
		}
		if ($person->save()) {
			$transaction->commit();
			if ($live) {
				loginProfile($person);
			} else {
				state('hide', true);
			}
			$base = new CController('base');
			$base->redirect(app()->createUrl('site/approve'));
		} else {
			$transaction->rollback();
			foreach ($person->getErrors() as $attr=>$message) {
				$model->addError($attr, $message);
			}
		}
	}
	return $model;
}

function openDiv($name=false, $options=array())
{
	$attr = array();
	if ($name) {
		if ($name[0] == '#') {
			$attr['id'] = substr($name, 1);
		} elseif ($name[0] == '.') {
			$attr['class'] = substr($name, 1);
		}
		$token = array('id', 'class', 'data-model', 'style');
		foreach ($options as $key=>$val) {
			if ($key == 'hidden' && $val) {
				$attr['style'] = 'display: none;';
			} else if (in_array($key, $token) && $val != '') {
				$attr[$key] = isset($attr[$key]) ? $attr[$key].' '.$val : $val;
			}
		}
	}
	return CHtml::openTag('div', $attr);
}

function blockDiv($name, $hide)
{
	$options = $hide ? array('hidden'=>true) : array();
	return openDiv($name, $options);
}

function closeDiv($repeat=0)
{
	if ($repeat) {
		$divs = '';
		for ($i = 0; $i < $repeat; $i++) {
			$divs .= CHtml::closeTag('div');
		}
		return $divs;
	} else {
		return CHtml::closeTag('div');
	}
}

function auth($action=false, $model=null)
{
	$person = personModel(); $hub = hubModel();
	if ($action == 'login') {
		$person = $model;
	} else if ($action == 'register') {
		$hub = $model;
	}
	return openDiv('#auth').openDiv('.login').Plan::Model('Login', Plot::Edit, 
	$person)->html().closeDiv().openDiv('.register').Plan::Model('Register', 
	Plot::Edit, $hub)->html().closeDiv().closeDiv();
}

function array_upper($array)
{
	$out = array();
	foreach ($array as $key=>$val) {
		$out[] = strtoupper($val);
	}
	return $out;
}

function bisect($num1, $num2)
{
	return array(intval($num1 / 2), intval($num2 / 2));
}

function copyright()
{
	$year = date('y'); if (date('m') > 10) $year++; 
	return BRAND." All Rights Reserved 2012-$year";
}

function currency($value)
{
	if (floatval($value) == intval($value)) {
		$value = intval($value);
	}
	return $value;
}

function find_keys($array, $search)
{
	$out = array(); $len = strlen($search);
	foreach ($array as $key=>$val) {
		if (!strncmp($key, $search, $len)) $out[$key] = $val;
	}
	return $out;
}

function intersect_model($model, $keys)
{
	return array_values(array_intersect_key($model->attributes, array_flip($keys)));
}

function indexSet($array, $values)
{
	if (!is_array($array)) {
		return false;
	}
	foreach ($values as $val) {
		if (!isset($array[$val])) {
			return false;
		}
	}
	return true;
}

function imageBase($filename)
{
	return root()."/images/$filename";
}

function numeric($value)
{
	return $value ? preg_replace('/[^\d]/', '', $value) : null;
}

function root()
{
	return implode('/', array_slice(explode('/', DOMAIN), 0, 3));
}

function smartTitle($name)
{
	cs()->registerScript('smartTitle', 'updateTitle("'.$name.'"); '.
	'$("#page").unbind("click");', CClientScript::POS_END);
}

function titleCase($value)
{
	return ucwords(strtolower($value));
}

function hubId()
{
	if (state_new('customer')) fetchLocation();
	return user()->isGuest ? client('hid') : user()->hid;
}

function hubModel($fb=0)
{
	$model = new Hub;
	if (!$fb) $model->setScenario('register');
	return $model;
}

function glossThumb($size, $hide=false)
{
	$file = File::model()->find('name="igloss" AND hub_id=:hid', array(':hid'=>
	param('hub'))); $options = array('class'=>'gloss'.($hide ? ' hidden' : ''));
	return CHtml::image(ImageHelper::thumb($size, $file), '', $options);
}

function gridStatus($model)
{
	$text = '';
	if ($model->live == 'open') {
		$text = 'Submit now - '.nice_date($model->phase_on);
	} else if ($model->live == 'closed') {
		$text = 'Submissions closed';
	} else {
		$text = 'Ended - '.nice_date($model->phase_on);
	}
	return " ($text)";
}

function personModel($login=1)
{
	$model = new Person;
	$model->setScenario($login ? 'login' : 'register');
	return $model;
}

function photoDefault()
{
	return File::model()->find('hub_id=:hid AND name=:name', 
	array(':hid'=>param('hub'), ':name'=>'profile'));
}

function renderJs()
{
	$js = '';
	cs()->renderBodyEnd($js);
	return $js;
}

function refineSort($order, $model)
{
	$sort = false;
	foreach ($model::model()->findAll() as $item) {
		if ($order == hash_route(null, $item->name)) {
			$sort = $item;
			break;
		}
	}
	return $sort ? array($sort->row_id, $sort->name) : array(false, '');
}

function subTotal($model, $cart=true)
{
	$find = 'task_id=:tid'.($cart ? ' AND cart=1 AND sold=0' : '');
	return Design::model()->count($find, array(':tid'=>$model->row_id));
}

function subWork($tid)
{
	return Wall::model()->find('task_id=:tid AND person_id=:pid AND work', 
	array(':tid'=>$tid, ':pid'=>user()->id));
}

function thumbnail($data, $size=false, $work=false)
{
	list($file, $icon) = $work ? array($data->work, $data) : array($data, null);
	$thumb = ImageHelper::thumb($size ? $size : Thumbs::Small(), $file, $icon);
	$image = $thumb ? CHtml::image($thumb, $file->name, $work ? 
	array('class'=>'sample') : array()) : 'Image error!';
	return $image;
}

function updateRefine($agent, $order, $model=null)
{
	$sort = $model ? ($order == 'all' ? -1 : 0) : $order;
	$name = ''; $index = null;
	if (!$sort) list($sort, $name) = refineSort($order, $model);
	$panel = Agent::Model($agent);
	foreach (multiRefine() as $i=>$multi) {
		if ($multi['panel'] == $panel) {
			$index = $i;
			break;
		}
	}
	if ($sort && $index !== null) {
		$refine = state('refine');
		$refine[$index] = $sort;
		state('refine', $refine);
	}
	return $name;
}

function wallLog($row, $type, $person=false, $member='task')
{
	$model = new Wall;
	$model["{$member}_id"] = $row;
	if ($person) {
		$model->person_id = $person;
	} else if (!user()->isGuest) {
		$model->person_id = user()->id;
	}
	$model->type = $type;
	$model->save();
}

function designPreview($brief, $ext, $dir, $file)
{
	$action = $ext == 'ai' ? 'graph' : 'render';
	$path = "$file.$ext";
	if (serverProc($action, $dir, array($file, $ext))) {
		serverProc('archive', $dir, $path);
		$src = app()->createUrl("file/private/$brief/$file");
		return '<script type="text/javascript">'.JCrop::Script($dir.$file.'.png', 
		$src)."jQuery('#filename').val('$file')</script>";
	}
	return false;
}

function makePackage($design)
{
	$home = param('root').'/private/'.$design->task->row_id;
	$dir = $home.'/pack'.$design->mirror.'/iOS Icons';
	if (!is_dir($dir)) {
		mkdir($dir, 0755);
	}
	$file = $design->work->filename.'_icon.png';
	copy("$home/$file", "$dir/$file");
	serverProc('resize', $dir, array($file, 
		array(57, 'Icon'),
		array(114, 'Icon@2x'),
		array(72, 'Icon-72'),
		array(144, 'Icon-72@2x'),
		array(29, 'Icon-Small'),
		array(58, 'Icon-Small@2x'),
		array(50, 'Icon-Small-50'),
		array(100, 'Icon-Small-50@2x'),
		array(512, 'iTunesArtwork'),
		array(1024, 'iTunesArtwork@2x'),
	));
	$name = str_replace(' ', '_', $design->task->name);
	$pack = substr($dir, 0, strrpos($dir, '/'));
	serverProc('zip', $pack, array("../$name-".$design->mirror.'.zip', '.'));
	serverProc('del', $home, array('pack'.$design->mirror));
	$design->ready = 2;
	$design->save();
}

function optimizeThumb($file, $model, $hid, $design=true)
{
	$sizes = array(Thumbs::Grid, Thumbs::Sample);
	$files = array('../../private/'.$model->task_id.'/'.$file->filename.'.png');
	foreach ($sizes as $edge) {
		$path = ImageHelper::thumb($edge, $file, $model);
		if ($path) $files[] = $path;
	}
	foreach ($files as $i=>$path) {
		if ($i) $files[$i] = substr(strrchr($path, '/'), 1);
	}
	$dir = param('root')."/uploads/$hid/";
	serverProc('shrink', $dir, $files);
	return $file;
}

function serverProc($action, $dir, $input)
{
	$win = PHP_OS == 'WINNT'; $spec = null;
	$proc = array();
	if ($action == 'archive' || $action == 'download') {
		$s3 = 'export GNUPGHOME=/home/apache/.gnupg && s3cmd -c /home/apache/.s3cfg';
		$path = 's3://probaz-vault/'.strstr($dir, 'private').'{xzip}';
		if ($action == 'archive') {
			$proc = array('xz {file}', "$s3 put -e {xzip} $path 2>&1", 'rm {xzip} &');
		} else {
			$proc = array("$s3 get $path {xzip}", 'chmod 644 {xzip}', 'unxz {xzip}');
		}
	} else if ($action == 'compose') {
		$proc = array('convert "{file}" -matte {mask} -compose DstIn -composite {file}');
	} else if ($action == 'crop') {
		$proc = array('convert "{infile}" -crop 1024x1024{dim} {outfile}');
	} else if ($action == 'del') {
		$proc = array('rm -r {files}');
	} else if ($action == 'ghost' || $action == 'graph') {
		$gs = ($win ? 'gswin64c' : 'gs'); $out = ' -sOutputFile={outfile} "{infile}"';
		$gs .= ' -dSAFER -dBATCH -dNOPAUSE -dLastPage=1 -sDEVICE=png16m ';
		if ($action == 'ghost') {
			$proc = array($gs.'-r271'.str_replace('outfile', 'temp', $out), 
			'convert {temp} -resize 26.59%% -quality 80 {outfile}', 'rm {temp}');
		} else {
			$proc = array($gs.'-r72.055'.$out);
		}
	} else if ($action == 'resize') {
		for ($i = 1; $i < count($input); $i++) $proc[] = 'convert "{infile}" '.
		"-resize {dim$i} {outfile$i}"; $proc[] = 'rm {infile}';
	} else if ($action == 'render') {
		$proc = array('convert "{infile}[0]" -channel rgb -quality 60 {outfile}');
	} else if ($action == 'sizes') {
		$proc = array('identify -format %P "{infile}[0]"');
	} else if ($action == 'shrink') {
		$proc = array('optipng -o2 {files} &');
	} else if ($action == 'webview') {
		$proc = array('w3m -cols 72 -dump {infile} > {outfile}');
	} else if ($action == 'zip') {
		$proc = array('zip -r {files}');
	}
	$type = array(null=>array('compose', 'crop', 'resize'), 'batch'=>array('del', 
	'shrink', 'zip'), 'vault'=>array('archive', 'download'));
	foreach ($type as $set=>$params) {
		if (in_array($action, $params)) {
			$spec = $set ? $set : $action;
		}
	}
	if ($spec == 'vault' && ($win || DEV)) return 1;
	$args = $find = array();
	if ($spec != 'batch') {
		if ($spec == 'compose') {
			$swap = array('file'=>$input, 'mask'=>'../probaz17/mask.png');
		} else if ($spec == 'crop') {
			list($dim, $name) = $input;
			$swap = array('dim'=>$dim, 'infile'=>"$name.png", 'outfile'=>$name.'_icon.png');
		} else if ($spec == 'resize') {
			$swap = array('infile'=>$input[0]);
			foreach ($input as $i=>$var) if ($i) $swap = array_merge($swap, 
			array("dim$i"=>$var[0], "outfile$i"=>$var[1].'.png'));
		} else if ($spec == 'vault') {
			$swap = array('file'=>$input, 'xzip'=>$input.'.xz');
		} else {
			list($file, $ext) = $input;
			$swap = array('infile'=>$file, 'outfile'=>$file, 'temp'=>$file.'_temp');
			$encode = array('ai'=>'png', 'psd'=>'png', 'html'=>'txt');
			foreach ($swap as $token=>$arg) {
				$swap[$token] = $arg.'.'.$ext;
				if (isset($encode[$ext])) $ext = $encode[$ext];
			}
		}
		$args = array_values($swap);
		foreach (array_keys($swap) as $token) {
			$find[] = '{'.$token.'}';
		}
	} else {
		list($args, $find) = array(implode(' ', $input), '{files}');
	}
	$cmd = str_replace($find, $args, implode(' && ', $proc));
	$cur = getcwd(); chdir($dir);
	exec($cmd, $output, $status); chdir($cur);
	return $action == 'sizes' ? $output[0] : !$status;
}

function multiRefine()
{
	return array(
		array(
			'panel'=>'Browse',
			'type'=>'Filter',
			'model'=>'Category',
			'data'=>'category_id',
		),
		array(
			'panel'=>'Browse',
			'type'=>'Order',
			'desc'=>'Filter',
			'values'=>array('-create_on'=>'Most Recent', 'price'=>'Lowest Price', 
			'-price'=>'Highest Price', 'status'=>'Open Jobs'),
		),
		array(
			'panel'=>'Accounts',
			'type'=>'Tab',
			'values'=>array('security'=>'Security Management', 'payment'=>'Payment Details'),
		),
		array(
			'panel'=>'Space',
			'type'=>'Channel',
			'values'=>array('brief'=>'My Briefs', 'sub'=>'My Submissions'),
		),
		array(
			'panel'=>'Profile',
			'type'=>'Tab',
			'values'=>array('main'=>'My Profile'),
		),
	);
}

function confirmCode($state, $code=false)
{
	$views = array('newEmail', 'resetPassword', 'verifyEmail');
	$valid = array('validChange', 'validReset', 'validEmail');
	$time = substr(time(), 0, 6);
	if ($code) {
		$person = Person::model()->findByPk($state);
		$type = $code[0]; $out = 'wrongUrl';
		if ($person) {
			if ($key = $person->confirm_key) {
				list($check, $expire) = array(substr($key, 0, 32), substr($key, 32));
				if ($check == $code) {
					$person->confirm_key = null;
					$person->save();
					$out = $expire > $time ? $valid[$type] : 'expiredCode';
				}
			}
		}
		if ($out == 'expiredCode') {
			$type = $views[$type];
			if (($type == 'validChange' && $person->new_email === null) || 
			($type == 'verifyEmail' && $person->valid_email)) {
				$out = false;
			}
		}
		return array($out, $person);
	} else {
		$views = array_flip($views); $time += 2;
		$code = $views[$state].substr(sha1(randomSalt()), 0, 31).$time;
		Person::model()->updateByPk(user()->id, array('confirm_key'=>$code));
		return app()->createAbsoluteUrl('site/confirm', array('state'=>user()->id, 
		'code'=>substr($code, 0, 32)));
	}
}

function reportCode($entry, $report=false)
{
	$views = array('oldEmail', 'newPassword');
	$desc = array('email change', 'password reset');
	if ($report) {
		$out = 'wrongUrl'; $email = $person = null;
		if ($code = decrypt($entry, false, true)) {
			list($pid, $j) = $code;
			list($type, $out) = array($desc[$j], $views[$j]);
			$person = Person::model()->findByPk($pid);
			$email = $out == 'oldEmail' ? $code[2] : $person->email; 
			$body = $person->first_name." [{$person->row_id}] reported this action via $email.";
			queueMail($body, "Invalid $type", null, $pid);
		}
		return array($out, $email, $person);
	} else {
		$views = array_flip($views);
		$code = array(user()->id, $views[$entry]);
		if ($entry == 'oldEmail') $code[] = user()->person->email;
		return app()->createAbsoluteUrl('site/report', array(
		'entry'=>encrypt($code, false, true)));
	}
}

function productControl($cart, $download, $status)
{
	$action = false; $rule = array();
	if ($download) {
		$rule = array('.download'=>'if ($(this).attr("href") == "#") {e.preventDefault(); juiAlert('.
		'"Download is being prepared. Please try again soon.")}'); $new = $download == 1;
		$action = array(($new ? 'New ' : '').'Download', 'download'.($new ? ' add' : ''));
	} else if ($status != 'archived') {
		$rule = array('.cart, .want'=>'saveCart($(this), match)');
		$action = $cart ? array('Added to cart', 'cart') : array('I want this', 'want');
	}
	foreach ($rule as $select=>$js) {
		if (!$download) $js = 'e.preventDefault(); var match = cellMatch($(this).closest('.
		'".cell"), 0); if (!$(this).hasClass("focus")) {$(this).addClass("focus"); '.$js.'}';
		cs()->registerScript(strrchr($select, '.'), '$'."('$select').click(function(e) { $js});");
	}
	return $action;
}

function smartyObject($data)
{
	require_once('Smarty.class.php');
	$smarty = new Smarty; $smarty->assign($data);
	$smarty->compile_dir = Yii::getPathOfAlias('vendors.smarty.compile');
	$smarty->template_dir = Yii::getPathOfAlias('application.views.builds');
	return $smarty;
}

function sendMail($data, $subject, $layout, $person_id)
{
	$boundary = '==Yii-Mail_'.md5(time()); $utf = '=?UTF-8?B?';
	$charset = 'Content-Type: text/plain; charset=UTF-8';
	$person = Person::model()->findByPk($person_id);
	$mail = $layout ? $person->email : SUPPORT;
	if ($layout) {
		$output = array(); $data['person'] = $person;
		$smarty = smartyObject($data);
		foreach(array('txt', 'html') as $ext) {
			$output[] = $smarty->fetch("$layout.$ext");
		}
		// TODO add subject as title to html
		$nl = "\n"; $np = $nl.$nl;
		$html = $layout == 'invoice' ? 1 : 0;
		$type = array($charset, str_replace('plain', 'html', $charset));
		$bounds = "--$boundary"; $encode = $nl.'Content-Transfer-Encoding: 7bit';
		$message = $bounds.$nl.$type[$html].$encode.$np.trim($output[$html]).$np.$bounds;
		if (!$html) $message .= $nl.$type[1].$encode.$np.trim($output[1]).$nl.$bounds;
		$message .= '--'.$nl;
		if ($layout == 'newEmail' && $person->new_email) $mail = $person->new_email;
	} else {
		$signature = array($person->full, $person->hub->countryCode->name.' | '.
		nice_date($person->create_on, false).' member', $person->email);
		$message = "[ $subject ]\n\n$data\n\n--\n".implode("\n", $signature);
	}
	$headers = array('From: '.BRAND.' Team <'.UBER.'>', 'Reply-To: '.($layout ? SUPPORT : 
	UBER), 'MIME-Version: 1.0', $layout ? "Content-Type: multipart/alternative;\n".
	"\tboundary=\"$boundary\"" : $charset, 'X-Mailer: PHP v'.phpversion());
	mail($layout ? $utf.base64_encode($person->full)."?= <$mail>" : $mail, $utf.
	base64_encode($subject).'?=', $message, implode("\r\n", $headers), '-f '.UBER);
}

function packUrl($task, $mirror)
{
	return app()->createUrl('file/pack/'.$task->row_id.'/'.str_replace(' ', '_', 
	$task->name).'-'.$mirror.'.zip');
}

function queueMail($query, $subject, $layout=null, $person_id=false)
{
	$thread = $layout == 'broadcast' ? 1 : 0;
	app()->db->createCommand()->insert('mail', array('query'=>serialize($query), 
	'layout'=>$layout, 'person_id'=>$person_id ? $person_id : user()->id, 
	'subject'=>$subject, 'thread'=>$thread, 'queue_on'=>new CDbExpression('NOW()')));
}

function response($gain, $result=array())
{
	$result['gain'] = $gain;
	return CJSON::encode($result);
}

function serverHost($server, $local)
{
	if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == '127.0.0.1') {
		return array($local);
	} else {
		return array($server, $local);
	}
}

function staticUrl($url, $params=array())
{
	srand(param('static'));
	$cache = rand(pow(10, 3), pow(10, 4) - 1);
	return app()->createUrl($url, array('cb'=>$cache));
	//return app()->createUrl($url, array_merge(array('_cb'=>app()->params['static']), $params));
}

function title($desc='')
{
	return ($desc != '' ? ucfirst($desc).' - ' : BRAND.' | ').app()->params['title'];
}

function unsetScript($method)
{
	$method = !is_array($method) ? array($method) : $method;
	foreach ($method as $desc) {
		unset(cs()->scripts[CClientScript::POS_END][$desc]);
	}
}

function ajaxSuccess($js/*, $follow=false*/)
{
	// TODO inspect data.notice
	return "function(data) {if (data.gain) {if (data.notice) juiAlert(data.notice); $js} else if ".
	'(data.invalid) {$.each(data.invalid, function(x, id) {$("#"+id).addClass("error")}); scrollUp()} '.
	'else {juiAlert(data.error ? data.error : "Process failed! Please contact support and try later.")}}';
	/*return 'function(data) {if (data.status == "done") {var error = false; if ("token" in data) '.
	'{alert(data.token.message); error = data.token.type == "error"} if (!error) {'.$js.'}} '.($follow 
	!= '' ? 'else if (data.status == "follow") {'.$follow.'} ' : '')."else {alert('Webpage error!')}}";
	$denied = CJavaScript::encode(array('type'=>'error', 'message'=>'Request failed! '.
	'Please contact the site administrator.')); qtipGrowl(data.token); qtipGrowl($denied)*/
}

function authorize($str, $model)
{
	if ($model->reset_pass && substr(time(), 0, 6) > substr($model->reset_pass, 0, 6) + 1) {
		$model->reset_pass = null; $model->save();
	}
	return crypt($str, $model->password) == $model->password || ($model->reset_pass && 
	$str == substr($model->reset_pass, 6));
}

function passwd($str)
{
	$salt = randomSalt(22);
	$rounds = 10; // check execution and confirm salt is 21 or 22 length
	return crypt($str, '$2a$'.$rounds.'$'.$salt.'$');
}

function support($desc=false)
{
	$text = is_string($desc) ? $desc : 'Email us here if you have queries.';
	return CHtml::mailto($desc ? $text : SUPPORT, SUPPORT.'?subject='.BRAND.' Enquiry');
}

function randomKey($len, $model, $filter=array())
{
	$row = get_class($model) == 'File' ? 'salt' : 'row_id';
	$criteria = "$row=:row";
	$params = array();
	$i = 0;
	foreach ($filter as $key=>$val) {
		$criteria .= " AND $key=:val$i";
		$params[":val$i"] = $val;
		$i++;
	}
	$salt = randomSalt($len, 0);
	$params[':row'] = $salt;
	while ($model->count($criteria, $params)) {
		$salt = randomSalt($len, 0);
		$params[':row'] = $salt;
	}
	return $salt;
}

function randomSalt($len=32, $upper=1)
{	
	// letter range
	$chars = array_merge(range('a', 'z'), range('0', '9'));
	if ($upper) {
		$chars = array_merge($chars, range('A', 'Z'));
	}
	$mod = count($chars);
	
	// random chars
	$timestamp = strval(time());
	if ($len <= 16) {
		$timestamp = substr($timestamp, strlen($timestamp)-5, 5);
	}
	if (@is_readable('/dev/urandom')) {
		$f = fopen('/dev/urandom', 'r');
		$rand = fread($f, 32);
	} else {
		$rand = secret();
	}
	
	// build salt
	$salt = array();
	while ($timestamp > '') {
		$pos = (int)substr($timestamp, 0, 2);
		$timestamp = substr($timestamp, 2);
		array_push($salt, $chars[$pos % $mod]);
	}	
	while (count($salt) < $len) {
		$pos = ord(substr($rand, 0, 1));
		$rand = substr($rand, 1);
		array_push($salt, $chars[$pos % $mod]);
	}
	shuffle($salt);
	return implode('', $salt);
}

function secret($fast=0, $account=0)
{
	$key = app()->params['secret'];
	$key = $fast ? md5($account ? $key : strrev($key)) : substr(sha1($key), 0, 32);
	if ($account) {
		$stamp = app()->db->createCommand('SELECT UNIX_TIMESTAMP(create_on) FROM '.
		'person WHERE row_id='.user()->id)->queryScalar();
		$salt = substr($stamp, 2).user()->id;
		$key = $salt.substr($key, strlen($salt));
	}
	return $key;
}

function decrypt($str, $person=true, $url=false)
{
	if (!$person && strlen($str) < 30) return false;
	$short = $person || $url;
	$cipher = $short ? MCRYPT_RIJNDAEL_128 : MCRYPT_RIJNDAEL_256;
	$key = secret($short, $person); $iv = secret(true);
	if (!$person) list($hash, $str) = array(substr($str, 0, 9), substr($str, 9));
	if ($url) $str = strtr($str, '-_', '+/');
	$data = mcrypt_decrypt($cipher, $key, base64_decode($str), MCRYPT_MODE_CBC, $short ? substr($iv, 16) : $iv);
	if (!$person && $hash != substr(md5(rtrim($data, "\0")), 23)) return false;
	$var = @gzinflate($data);
	return $var ? json_decode($var, true) : $data;
}

function encrypt($var, $person=true, $url=false)
{
	$short = $person || $url;
	$cipher = $short ? MCRYPT_RIJNDAEL_128 : MCRYPT_RIJNDAEL_256;
	$key = secret($short, $person); $iv = secret(true);
	$data = is_array($var) ? gzdeflate(json_encode($var)) : $var;
	$hash = !$person ? substr(md5(rtrim($data, "\0")), 23) : '';
	$str = base64_encode(mcrypt_encrypt($cipher, $key, $data, MCRYPT_MODE_CBC, $short ? substr($iv, 16) : $iv));
	return $hash.($url ? rtrim(strtr($str, '+/', '-_'), '=') : $str);
}

function constantName($id, $class)
{
	$reflection = new ReflectionClass($class);
	$enum = $reflection->getConstants();
	foreach ($enum as $key=>$val) {
		if ($id == $val) {
			return $key;
		}
	}
	return false;
}

function httpError($code)
{
	$status = array(200=>'OK', 300=>'Multiple Choices', 301=>'Moved Permanently', 302=>'Found', 304=>'Not Modified', 
	307=>'Temporary Redirect', 400=>'Bad Request', 401=>'Unauthorized', 403=>'Forbidden', 404=>'Not Found', 410=>'Gone', 
	500=>'Internal Server Error', 501=>'Not Implemented', 503=>'Service Unavailable', 550=>'Permission denied');
	if (isset($status[$code])) {
		return $status[$code];
	} else {
		return 'Unknown Error';
	}
}

function pageHeader()
{
	return <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

END;
}

class Agent
{
	const MessageComments = 'a0';
	const ProfileComments = 'a1';
	const TaskComments = 'a2';
	const Browse = 'b0';
	const Grid = 'b1';
	const Accounts = 'c0';
	const Space = 'c1';
	const Profile = 'c2';
	
	public static function Model($id)
	{
		return constantName($id, get_class());
	}
}

class Chain
{
	const Admin = 'admin';
	const Business = 'business';
	const Customer = 'customer';
}

class Plot
{
	const Create = 0;
	const Edit = 1;
	const View = 2;
}

class Thumbs
{
	const Phone = 52;
	const Button = 60;
	const Frame = 84;
	const Preview = 143;
	const Grid = 155;
	const Invoice = 195;
	const Sample = 308;
	const Jcrop = 582;
	const Stage = 1024;
	
	public static function Large()
	{
		return array(174, 166);
	}
	
	public static function Full()
	{
		return array(160, 152);
	}
	
	public static function Medium()
	{
		return array(81, 77);
	}
	
	public static function Small()
	{
		return array(49, 47);
	}
}

Yii::setPathOfAlias('vendors', dirname(__FILE__).'/../../vendors/');
