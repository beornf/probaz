<?php

Yii::import('zii.behaviors.CTimestampBehavior');

class TaskLogBehavior extends CTimestampBehavior
{
	public function afterSave($event) {
		if ($this->owner->log) {
			$row = $this->owner->row_id;
			if ($this->owner->getIsNewRecord()) {
				$type = 'post';
			} else {
				$walls = Wall::model()->findAll(array('condition'=>'task_id=:rid', 
				'params'=>array(':rid'=>$row), 'order'=>'record_on DESC'));
				foreach ($walls as $wall) {
					if ($wall->type == 'edit') {
						$wall->delete();
					} else {
						break;
					}
				}
				$type = 'edit';
			}
			wallLog($row, $type);
		} else {
			$this->owner->log = true;
		}
	}
}