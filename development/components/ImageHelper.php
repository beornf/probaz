<?php
/**
 * Image helper functions
 * 
 * @author Chris
 * @link http://con.cept.me
 */
class ImageHelper
{
    /**
     * Suffix to store thumbnails
     * @var string
     */
    const THUMB = 'thumb';

    /**
     * Create a thumbnail of an image and returns relative path in webspace
     *
     * @param array $size
     * @param file $image
     * @param design $design
     * @return string $path
     */
    public static function thumb($size, $image, $design=null)
    {
    	// thumbnail dimensions
		$width = is_array($size) ? $size[0] : $size;
		$height = is_array($size) ? $size[1] : $size;
		
		// jpeg quality / resize method
		$quality = 90;
		$method = 'adaptiveResize';
		
		$path = param('root').$image->path;
		$ext = $design ? 'png' : $image->ext; $file = "$path.$ext";
		if (!file_exists($file)) {
			$dir = param('root').'/uploads/'.$image->hub_id;
			if (!is_dir($dir)) {
				mkdir($dir, 0755);
			}
			if ($design) {
				$file = param('root').'/private/'.$design->task_id.'/'.$image->filename.'.png';
			}
			if ($image->fb && is_array(@get_headers($image->url))) {
				file_put_contents($file, file_get_contents($image->url));
			}
		}
		if (file_exists($file)) {
			$thumb_path = $path.'_'.$width.'_'.$height.'.'.$ext;
			if (!file_exists($thumb_path)) {
				$options = array('jpegQuality'=>$quality);
				$thumb = PhpThumbFactory::create($file, $options);
				if ($design) {
					$diff = 4; $method = 'resize';
					$thumb->crop($design->pX + $diff, $design->pY + $diff, $design->pW - 
					($diff * 2), $design->pH - ($diff * 2));
				}
				if ($method) $thumb->{$method}($width, $height);
				$thumb->save($thumb_path);
			}
			return (DEV ? param('baseDir') : '').substr($thumb_path, 1);
		}
		return false;
    }
}
