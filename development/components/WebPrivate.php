<?php

class WebPrivate extends CWebUser
{
	private $_hid;
	private $_person;
	private $_customer;
	
	public function getAdmin()
	{
		return !user()->isGuest ? $this->getPerson()->member == 'admin' : false;
	}
	
	public function getHid()
	{
		if ($this->_hid === null) {
			$this->_hid = $this->getPerson()->hub_id;
		}
		return $this->_hid;
	}
	
	protected function getPerson()
	{
		if ($this->_person === null) {
			$this->_person = Person::model()->find('row_id=:id', array(':id'=>$this->getId()));
		}
		return $this->_person;
	}
	
	public function newActive()
	{
		Person::model()->updateByPk($this->getId(), array('active_on'=>new CDbExpression('NOW()')));
	}
	
	public function login($identity, $duration=0)
	{
		state('ajax', array('index', null));
		parent::login($identity, $duration);
		$this->moveHub(client('hid'), $this->getHid(), $this->getId());
		Person::model()->updateByPk($this->getId(), array(
			'login_on'=>new CDbExpression('NOW()'),
		));
		if (verify() && verify(1)) {
			$subject = 'Verify your account'; $type = 'verifyEmail'; queueMail(array(
			'confirm'=>confirmCode($type)), $subject, $type, $this->_person->row_id);
		}
	}
	
	private function moveHub($guest, $target, $person)
	{
		$command = app()->db->createCommand('CALL moveHub(:guest, :target, :person)');
		$command->bindParam(':guest', $guest, PDO::PARAM_STR);
		$command->bindParam(':target', $target, PDO::PARAM_STR);
		$command->bindParam(':person', $person, PDO::PARAM_INT);
		$command->execute();
		
		$source = param('baseDir').'/uploads/'.$guest;
		if (is_dir($source)) {
			$files = scandir($source);
			$dest = param('baseDir').'/uploads/'.$target;
			if (!is_dir($dest)) {
				mkdir($dest, 0755);
			}
			foreach ($files as $file) {
				if (!in_array($file, array('.', '..'))) {
					rename($source.'/'.$file, $dest.'/'.$file);
				}
			}
		}
	}
}