<?php

abstract class Plan
{
	private $plot;
	private $edit;
	private $model;
	private $post;
	private $back;
	private $title;
	private $layout;
	private $fields;
	private $forms;
	private $html;
	
	public function __construct($plot)
	{
		$this->plot = $plot[0];
		$this->edit = $plot[0] != Plot::View;
		$this->model = $plot[1];
		$this->back = $this->title = false;
		$this->layout = array();
	}
	
	public function __set($property, $value)
	{
		$func = 'set'.ucwords($property);
		if (method_exists($this, $func)) {
			$this->$func($value);
		} else if (property_exists($this, $property)) {
			$this->$property = $value;
		}
	}
	
	private function attr($item, $id)
	{
		$item->id = $id;
		$item->edit = $this->edit;
		$parent = strstr($item->data, '-', true);
		if ($parent) {
			$item->data = substr($item->data, strpos($item->data, '>') + 1);
		}
		$item->model = $parent ? $this->model[$parent] : $this->model;
		if ($item->data) {
			$item->value = $item->model[$item->data];
		}
		if (is_string($item->attr)) {
			if ($item->attr == 'active') {
				$item->active = false;
			} else if ($item->attr == 'private') {
				if ($this->model->row_id != user()->id) {
					$item->attr = 'hidden';
				}
			}
		}
	}
	
	private function setLayout($layout=array())
	{
		$this->fields = array();
		foreach ($layout as $id=>$item) {
			if ($item->class == 'submit' && $this->plot == Plot::View) {
				unset($layout[$id]);
			} else {
				$this->fields[$id] = &$item;
				foreach ($item->subItems as $subId=>$subItem) {
					$this->fields[$subId] = &$subItem;
				}
				foreach ($item->children as $subId=>$subItem) {
					$this->attr($subItem, $subId);
				}
				$this->attr($item, $id);
			}
		}
		$this->layout = $layout;
	}
	
	private function setTips($tips=array())
	{
		foreach ($tips as $id=>$tip) {
			$this->fields[$id]->tip = $tip;
		}
	}
	
	public function html()
	{
		$out = '';
		if ($this->plot == Plot::Create) {
			$state = 'Create';
		} else if ($this->edit) {
			$state = 'Edit';
		} else {
			$state = 'View';
		}
		$plan = str_replace('plan', '', strtolower(get_class($this)));
		$content = $plan == 'message' ? openDiv('.raise').closeDiv() : '';
		if ($this->back) {
			$hash = strpos($this->back[1], '()') ? '#' : '#!/'.$this->back[1];
			$content .= openDiv('.top').CHtml::link('Back to '.$this->back[0], $hash, 
			array('class'=>'path')).closeDiv();
			$id = $plan == 'upload' ? 'uploadBar' : 'page';
			if ($hash == '#') cs()->registerScript('back', '$("#'.$id.' .path").click('.
			'function(e) {e.preventDefault(); '.$this->back[1].'});');
		}
		if ($this->title) {
			$heading = '';
			if (is_array($this->title)) {
				list($field, $attr) = $this->title;
				$data = $this->model[$field];
				$heading = $data ? $data->$attr : '';
			} else {
				$heading = strpos($this->title, '<a') ? $this->title : str_replace('#', 
				$state, $this->title); 
			}
			$compare = array('login', 'register');
			if (!in_array(app()->controller->action->id, $compare) && 
			in_array($plan, $compare)) $heading .= openDiv('#close').
			CHtml::link('Close', '#').closeDiv(); 
			$content .= openDiv($plan == 'grid' ? '.grid' : '.header').$heading.
			closeDiv(); /*openDiv('.middle').closeDiv()*/
		}
		if ($this->edit) {
			$url = explode('/', DOMAIN, 4); $action = '/'.$url[3].$this->post;
			$content .= EHtml::beginForm($action, 'post', array('class'=>$plan.'Form'));
			if ($plan == 'prepare') $content .= openDiv('.raise').closeDiv();
			EHtml::setOptions(array(
				'errorLabelContainer'=>'.validate',
				'wrapper'=>'li',
			));
		}
		foreach ($this->layout as $item) {
			if (($this->edit || $item->attr != 'hidden') && (!$this->edit || $item->active)) {
				$content .= $item->content();
			}
		}
		$append = '';
		if ($plan == 'grid') {
			$height = 'height: '.strval(ceil(count($this->model->designs) / 4) * 
			214 + 475).'px';
			$append = openDiv('.raise', array('style'=>$height)).closeDiv();
		} else if ($plan == 'profile' || $plan == 'upload' || ($plan == 'task' 
		&& !$this->edit)) {
			$append = openDiv('.raise').closeDiv();
		}
		return $content.($this->edit ? EHtml::endForm() : '').$append;
	}

	public function plotMember($task)
	{
		$member = !user()->isGuest;
		$client = $member && $task->person_id == user()->id;
		$work = $client || ($member && Design::model()->count('task_id=:tid AND '.
		'person_id=:pid', array(':tid'=>$task->row_id, ':pid'=>user()->id)));
		return array($work, $client, $member);
	}

	public function submit($post, $update, $create='')
	{
		$edit = $this->plot == Plot::Edit; 
		$desc = $edit ? $update : $create;
		/*$tree = in_array($create, array('inner', 'hidden')) || 
		get_class($this->model) == 'Person';*/
		$type = is_bool($post) ? 'Button' : 'Submit';
		$this->post = is_bool($post) ? 'inner' : $post;
		$submit = new Token($type, null, 'submit', $create == 'hidden');
		$class = (get_class($this) == 'UploadPlan' || (get_class($this) == 'TaskPlan' 
		&& $this->plot == Plot::Create) ? 'add' : 'save').(in_array(get_class($this), 
		array('GridPlan', 'PreparePlan')) ? ' theme' : '');
		$submit->addItem(new Token(strtoupper($desc), $class)); /*.($tree ? ' tree' : '')*/
		return $submit;
	}
	
	public static function Model($name, $plot=Plot::View, $model)
	{
		$name .= 'Plan';
		return new $name(array($plot, $model));
	}
}

class Token extends CController
{
	private $id;
	private $desc;
	private $model;
	private $data;
	private $value;
	private $class;
	private $attr;
	private $children;
	private $subItems;
	private $active;
	private $combo;
	private $edit;
	private $tip;
	
	public function __construct()
	{
		$argv = func_get_args();
		switch (func_num_args()) {
			case 4:
				$this->__constructX($argv[0], $argv[1], $argv[2], $argv[3]);
				break;
			case 3:
				$this->__constructX($argv[0], $argv[1], $argv[2]);
				break;
			case 2:
				$this->__constructY($argv[0], $argv[1]);
		}
	}
	
	private function __constructX($desc, $data, $class, $attr=null)
	{
		$this->desc = $desc;
		$this->data = $data;
		$this->class = $class;
		$this->attr = $attr;
		$this->children = array();
		$this->subItems = array();
		$this->active = true;
		$this->combo = $this->edit = $this->tip = false;
	}
	
	private function __constructY($desc, $class)
	{
		$this->desc = $desc;
		$this->class = $class;
		$this->children = array();
		$this->subItems = array();
		$this->active = true;
		$this->combo = $this->edit = $this->tip = false;
	}
	
	public function __set($property, $value)
	{
		$func = 'set'.ucwords($property);
		if (method_exists($this, $func)) {
			$this->$func($value);
		} else if (property_exists($this, $property)) {
			$this->$property = $value;
		}
	}
	
	public function __get($property)
	{
		$func = 'get'.ucwords($property);
		if (method_exists($this, $func)) {
			return $this->edit && $this->combo ? $this->$func() : $this->cell($this->$func(), $this->class);
		} else if (property_exists($this, $property)) {
			return $this->$property;
		}
		return null;
	}
	
	public function content($view=false)
	{
		$func = $this->class.(!$this->edit || $view ? 'View' : '');
		return $this->$func;
	}
	
	public function cell($content, $class)
	{
		$options = array();
		$attr = strstr($this->attr, '|'); $type = false;
		$attr = $attr ? substr($attr, 1) : $this->attr;
		if (!$this->edit && ($class == 'upload' && !count($this->value) && $this->data != 'profile')) {
			return '';
		} else if (!$this->edit) {
			if (in_array($this->class, array('frame', 'textPrice', 'sub'))) $type = 'flex';
			if ($this->class == 'legend') $type = $this->class;
			$options['class'] = 'edit'.($type ? " $type" : '');
		} else if (!in_array($class, array('fbook', 'upload'))) {
			if (in_array($attr, array('inner', 'hidden')) && 
			in_array($class, array('text', 'pass', 'dropdown'))) {
				$type = 'inner';
			} else if ($attr == 'center') {
				$type = 'center';
			} else if ($class == 'jcrop') {
				$type = 'crop';
			} else if ($class == 'thumb') {
				$type = 'icon';
			}
			if ($type) $options['class'] = $type;
		}
		if (get_class($this->model) != 'Person' && $attr == 'hidden') $options[] = 'hidden';
		return $this->desc ? openDiv('.token', $options).$content.closeDiv() : $content;
	}
	
	public function unit($content, $func=false)
	{
		$affix = '';
		foreach ($this->children as $child) {
			if (!$child->active && $this->edit) {
				$child->model = $this->model;
				$affix .= $child->content();
			}
		}
		return $this->desc != '' ? $this->openDesc().$content.$affix.$this->close($func) : $content;
	}
	
	public function addItem($item, $id=null)
	{
		if ($id) {
			$this->subItems[$id] = $item;
		} else {
			$this->subItems[] = $item;
		}
	}
	
	public function addChild($item, $active=true)
	{
		$item->active = $active;
		$this->children[] = $item;
	}
	
	private function openDesc()
	{
		$style = array('class'=>$this->edit ? 'middle' : 'upper');
		$bind = $this->edit || get_class($this->model) != 'Person';
		$desc = !$this->edit ? ($this->class == 'upload' ? 'Images/Sketches as guide' : 
		ucfirst(str_replace('_', ' ', $this->id))) : $this->desc;
		return ($bind ? openDiv('.desc', $style).$desc.closeDiv() : '').openDiv('.input', $style);
	}
	
	private function close($func)
	{
		$func = str_replace('get', '', $func).'View';
		return ($this->combo ? $this->$func : '').closeDiv();
	}
	
	private function agent()
	{
		$list = Panel::Agent($this->desc, $this->value);
		return $list->display();
	}
	
	private function clear($fix=0)
	{
		if (isset($this->attr)) {
			$fix = $fix ? $fix : get_class($this->model).'_'.$this->data;
			cs()->registerScript($fix, '$("#'.$fix.'").watermark("'.$this->attr.'");');
		}
	}
	
	private function holder($options=array())
	{
		if (isset($this->attr) && !in_array($this->attr, array('hidden', 'inner'))) {
			$options['placeholder'] = $this->attr;
		}
		return $options;
	}
	
	private function textSet($price=0, $small=0)
	{
		$options = $this->holder(array('style'=>$this->combo ? 'display: none' : ''));
		$options['class'] = $price ? 'cost' : ($small ? 'small' : '');
		$field = EHtml::activeTextField($this->model, $this->data, $options);
		return $price ? '<span class="dollar">$</span>'.$field : $field;
		/*.'<span>with</span>'*/
	}
	
	private function getClient()
	{
		return $this->unit(client('country', true));
	}
	
	private function getText()
	{
		return $this->unit($this->textSet(), __FUNCTION__);
	}
	
	/*private function getTextBox()
	{
		return $this->unit($this->textSet(0, 1));
	}*/
	
	private function getTextPrice()
	{
		return $this->unit($this->textSet(1));
	}
	
	private function getTextArea()
	{
		$options = array('style'=>$this->combo ? 'display: none' : '');
		cs()->registerScript($this->id, '$("#'.get_class($this->model).'_'.
		$this->data.'").autosize();');
		return $this->unit(EHtml::activeTextArea($this->model, $this->data, 
		$this->holder($options)), __FUNCTION__);
	}
	
	private function affixView($deal=false)
	{
		$before = $body = '';
		$start = true;
		foreach ($this->children as $child) {
			if ($child->active) {
				if (!($deal && $start)) {
					$body .= $child->content(true);
				} else if ($start) {
					$before = $child->content();
					$start = false;
				}
			}
		}
		$affix = count($this->children) > 1 && !$deal ? openDiv('.affix').
			$body.closeDiv() : $body;
		return array($affix, $before);
	}
	
	private function newSub()
	{
		cs()->registerScript('newSub', '$("#page .sub").click(function(e) {'.
		'e.preventDefault(); showSub()});');
		return CHtml::link($this->desc, '#', array('class'=>'sub'));
	}
	
	private function textSetView($class='') /*, $prefix=''*/
	{
		$affix = $before = ''; $deal = $class == 'deal';
		if (count($this->children)) {
			list($affix, $before) = $this->affixView($deal);
			//$model = subWork($this->model->row_id);
		}
		$person = get_class($this->model) == 'Person';
		$prefix = $this->attr == 'prefix' ? $this->desc.' ' : '';
		$value = $this->value.($person ? '.' : '');
		$content = CHtml::openTag('span', $class != '' ? array('class'=>$class) : array()).
			$prefix.nl2br($deal ? currency($value) : $value).CHtml::closeTag('span');
		if ($class == 'badge') {
			$row = !$person ? 'person' : 'row';
			if (!user()->isGuest && $this->model[$row.'_id'] == user()->id) {
				$content .= CHtml::link('Edit', $person ? app()->createAbsoluteUrl(
				'/#!/my/edit') : hash_route('edit', $this->model->name, 'task'));
			}
		}
		else if ($deal) $content = $before.$content.CHtml::closeTag('a'); $content .= $affix;
		return $class == 'badge' || $this->edit || $deal ? $content : ($class == 'tag' ? 
		openDiv('.start').$content.closeDiv() : $this->unit($content));
	}
	
	private function getTextView()
	{
		$badge = get_class($this->model) == 'Person' || count($this->children);
		return $this->textSetView($badge ? 'badge' : '');
	}
	
	private function getTextAreaView()
	{
		if ($this->value == '') $this->value = 'Not provided.';
		return $this->textSetView();
	}
	
	private function getTextPriceView()
	{
		return $this->edit ? $this->textSetView(count($this->children) ? 'deal' : '', '$') : 
		openDiv('.prize').'Prize<span>$'.currency($this->value).closeDiv().'</span>';
	}
	
	private function getBranchView()
	{
		return $this->textSetView('branch');
	}
	
	private function getControlView()
	{
		$content = ''; $client = false;
		if ($this->model->task->person_id == user()->id && $control = productControl(
		$this->value, $this->model->sold, $this->model->task->status)) {
			$href = '#'; $options = array('class'=>$control[1]);
			if ($this->model->sold && $this->model->ready == 2) {
				$href = packUrl($this->model->task, $this->model->mirror);
				$options['target'] = '_blank';
			}
			$content = CHtml::link($control[0], $href, $options);
			$client = true;
		}
		if ((!$client && $this->model->message == 1) || ($client && $this->model->message = 2)) {
			$this->model->message = 0;
			$this->model->save();
		}
		$options = array('class'=>'cell', 'data-model'=>'design'.$this->model->row_id);
		return openDiv('.hook', $options).$content.closeDiv();
	}
	
	private function getMessageView()
	{
		$person = $this->value != user()->id ? $this->model->person : $this->model->task->person;
		$this->value = 'Private Discussion with '.CHtml::openTag('span').$person->name;
		$this->value .= CHtml::closeTag('span').'<br>for "'.$this->model->task->name;
		$this->value .= '" - #'.(int)$this->model->mirror;
		return $this->textSetView('tag');
	}
	
	private function getThumbView()
	{
		$size = $this->attr ? ($this->attr == 'button' ? Thumbs::Button : 
		Thumbs::Sample) : false; $work = false;
		if ($this->attr == 'button') {
			$path = ImageHelper::thumb($size, $this->value, $this->model);
			$this->value = glossThumb(Thumbs::Button).CHtml::image($path, 'Button', 
			array('class'=>'icon'));
			return $this->textSetView('tag');
		} else if ($this->attr == 'sample') {
			$this->value = $this->model; $work = true;
		}
		return thumbnail($this->value ? $this->value : photoDefault(), $size, $work);
	}
	
	private function getHidden()
	{
		return CHtml::activeHiddenField($this->model, $this->data, array('value'=>$this->attr));
	}
	
	private function getPostedView()
	{
		return CHtml::openTag('span').nice_date($this->value).CHtml::closeTag('span').closeDiv();
	}
	
	private function getUserView()
	{
		return openDiv('.list', array('class'=>'middle')).'Posted by '.CHtml::link($this->value, 
		hash_route('', $this->model, 'profile'), array('class'=>'systemUser', 'data-model'=>
		'person'.$this->model->row_id)).'<br><br>';
	}
	
	private function getZlistView()
	{
		return $this->agent();
	}
	
	private function getAuto()
	{
		$source = array();
		if ($this->data == 'city') {
			$model = City::model()->findAll('country_code=:country AND confirm=1', 
				array(':country'=>client('country')));
			foreach ($model as $city) {
				$source[] = $city->name;
			}
		}
		sort($source);
		$content = $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			'model'=>$this->model,
			'attribute'=>$this->data,
			'source'=>$source,
			'htmlOptions'=>$this->holder(),
			'themeUrl'=>'',
		), true);
		return $this->unit($content);
	}
	
	private function getDate()
	{
		$this->model[$this->data] = sql_date($this->value, 'D, j M, Y');
		$content = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=>$this->model,
			'attribute'=>$this->data,
			'options'=>array(
				'dateFormat'=>'D, d M yy',
				'minDate'=>'0',
				'maxDate'=>'+1M',
				'showAnim'=>'slide',
			),
			'htmlOptions'=>array(
				'readOnly'=>true,
			),
			'themeUrl'=>'',
		), true);
		return $this->unit($content.CHtml::image('/images/date.png'));
	}
	
	private function getDesign()
	{
		$content = $this->widget('ext.ajaxUpload.EAjaxUpload', array(
			'class'=>'theme token',
			'method'=>true,
			'postParams'=>array('type'=>'design'),
			'config'=>array(
				'action'=>app()->createUrl('file/upload', array('tid'=>$this->value)),
				'allowedExtensions'=>app()->params['designExt'],
				'sizeLimit'=>app()->params['sizeLimit'],
				'listElement'=>'js: document.getElementById("state")',
				'onComplete'=>'js: function(id, name, data) {if (data.gain) {'.
				'$("#holder").html(data.script)}}',
			),
		), true);
		$span = CHtml::openTag('span').'Upload your design!'.CHtml::closeTag('span');
		return $content.openDiv('#state', array('class'=>'token')).$span.closeDiv();
	}
	
	private function getJcrop()
	{
		if ($crop = $this->desc == 'crop') {
			$param = array(
				'imageUrl'=>'/images/clear.png',
				'thumbSize'=>Thumbs::Preview,
				'htmlOptions'=>array(
					'width'=>Thumbs::Jcrop,
					'height'=>Thumbs::Jcrop
				),
			);
		} else {
			$file = param('root').$this->value->path.'.png';
			$param = array('imageUrl'=>file_exists($file) ? $file : app()->createUrl(
			'file/private/'.$this->model->task_id.'/'.$this->value->filename));
			$options = CJavaScript::encode(array('allowChange'=>false, 
			'allowSelect'=>false, 'onSelect'=>'js:saveCoords', 
			'setSelect'=>array($this->model->pX, $this->model->pY, 
			$this->model->pX + $this->model->pW, $this->model->pY + 
			$this->model->pH)));
			cs()->registerScript('load', "$('#jcrop').load(function() {".
			'$.Jcrop("#jcrop", '.$options.')});');
		}
		$content = $this->widget('application.components.JCrop', $param, true);
		return CHtml::activeHiddenField(new Design, !$crop ? 'row_id' : 'task_id', 
			array('value'=>$this->model->row_id)).$content;
	}
	
	private function getLarge()
	{
		$path = $this->createUrl('file/large', array('rid'=>$this->model->row_id));
		return CHtml::image($path, $this->value->name);
	}
	
	private function getPass()
	{
		return $this->unit(EHtml::activePasswordField($this->model, $this->data));
	}
	
	private function getReset()
	{
		return openDiv('#reset').CHtml::link($this->desc, '#').closeDiv();
	}
	
	private function getRule()
	{
		return '"'.$this->value.'" '.$this->desc.' '.$this->model->person->first_name;
	}
	
	private function getSpan()
	{
		$content = '';
		$line = !is_array($this->desc) ? array($this->desc) : $this->desc;
		foreach ($line as $class=>$text) {
			$exist = is_string($class);
			$options = $exist ? array('class'=>$class) : array();
			if ($exist && $class == 'cost') {
				$text .= ' '.subTotal($this->model) * $this->value;
			}
			if ($exist && $class == 'expand') {
				$content .= CHtml::link($text, '#', $options);
			} else {
				$content .= CHtml::openTag('span', $options).$text.CHtml::closeTag('span');
			}
		}
		return $this->attr ? $content : openDiv('.desc').$content.closeDiv();
	}
	
	private function getAuthor()
	{
		$affix = $this->affixView();
		return $affix[0];
	}
	
	private function getDivider()
	{
		return openDiv('.divider').$this->desc.closeDiv();
	}
	
	private function getFbook()
	{
		$link = CHtml::link($this->desc, '#', array('class'=>'dialog'));
		return openDiv('#fbook').$link.closeDiv();
	}
	
	private function getNotice()
	{
		return openDiv('.notice').$this->desc.closeDiv();
	}
	
	private function getSub()
	{
		return $this->newSub();
	}
	
	private function getThumb()
	{
		$gloss = glossThumb(Thumbs::Preview, true);
		return $gloss.CHtml::image('/images/clear.png', 'Thumb', array('id'=>'jcrop-thumb'));
	}
	
	private function getZList()
	{
		return $this->agent();
	}
	
	private function getDateView()
	{
		return $this->unit(sql_date($this->value, 'l, jS F'));
	}
	
	private function getDateNiceView()
	{
		$time = nice_date($this->value); $join = $this->data == 'create_on' ? 'Member since ' : '';
		return $this->unit($join ? CHtml::openTag('span')."$join $time".CHtml::closeTag('span') : $time);
	}
	
	private function getEditView()
	{
		return 'abc';
	}
	
	private function getFrameView()
	{
		$content = '';
		$route = hash_route('store', $this->model, 'task');
		$models = Design::model()->findAll(array('condition'=>'task_id=:tid', 'params'=>
		array(':tid'=>$this->value), 'order'=>'sold_on DESC, create_on DESC'));
		for ($i = 0; $i < min(count($models), 4); $i++) {
			$design = $models[$i];
			$path = ImageHelper::thumb(Thumbs::Frame, $design->work, $design);
			$content .= CHtml::openTag('a', array('class'=>'set', 'href'=>$route)).
			glossThumb(Thumbs::Frame).CHtml::image($path, 'Icon').CHtml::closeTag('a');
		}
		$content .= !count($models) ? CHtml::openTag('span').'Be the first to '.
		'submit!'.CHtml::closeTag('span') : CHtml::link($this->desc, $route);
		return openDiv('.frame').$content.closeDiv();
	}
	
	private function getHireView()
	{
		$item = 'hire'; $options = array('class'=>'leaf', 'hidden'=>!$this->value);
		return openDiv("#$item", $options).CHtml::link('Hire', hash_route($item)).closeDiv();
	}
	
	private function getOptionView()
	{
		$field = $this->data == 'location' ? 'locality' : 'name'; $item = $this->value;
		$text = $item ? (strstr($this->attr, 'Val') !== false ? $this->subItems[$item] : 
		$item->$field) : ($field == 'locality' ? 'Online/Phone' : 'NA');
		return $this->unit($this->data == 'live' ? openDiv('.status', array('class'=>'tree')).
		$text.closeDiv() : $text);
	}
	
	/*private function getValidView()
	{
		$content = '';
		foreach (array('Facebook'=>'facebook_id', 'Email'=>'valid_email') as $desc=>$valid) {
			if ($this->model[$valid]) {
				$content .= openDiv()."$desc<br><span>Verified</span>".closeDiv();
			}
		}
		return openDiv('#valid').openDiv().closeDiv().openDiv('.block', 
		array('hidden'=>$content == '')).$content.closeDiv().closeDiv();
	}*/
	
	private function getButtonView()
	{
		return $this->getOptionView();
	}
	
	private function getDropdownView()
	{
		//$desc = strstr($this->desc, '-', true); if ($desc) $this->desc = $desc;
		return $this->getOptionView();
	}

	private function getLegendView()
	{
		return $this->desc;
	}
	
	private function getSubView()
	{
		return $this->newSub();
	}
	
	private function getUploadView()
	{
		list($affix) = $this->affixView();
		return $this->unit($this->photoPreview().$affix);
	}
	
	private function getWorkView()
	{
		cs()->registerScript('viewUpload', '$("a.subUpload").on("click", function(e) '.
		'{e.preventDefault(); showSub()})');
		return CHtml::openTag('a', array('class'=>'subUpload', 'href'=>'#')).'Do This For';
	}
	
	private function multiSelect($listModel, $config, $options=array(), $class=false)
	{
		$options = array_merge(array('model'=>$this->model, 'column'=>$this->data, 'listModel'=>$listModel, 
		'class'=>$class ? $class : $this->id, 'height'=>194, 'config'=>$config), $options);
		return $this->widget('ext.multiselect.EMultiSelect', $options, true);
	}
	
	private function photoPreview()
	{
		$content = openDiv('.display');
		$border = $this->attr === null;
		for ($i = 0; $i < ($border ? 1 : $this->attr); $i++) {
			$photo = false;
			if ($i < count($this->value)) {
				$photo = $this->attr > 1 ? $this->value[$i] : $this->value;
			} else if ($this->data == 'profile') {
				$photo = photoDefault();
			}
			$size = $border ? Thumbs::Full() : ($i ? Thumbs::Medium() : Thumbs::Large());
			$thumb = $photo ? ImageHelper::thumb($size, $photo) : false;
			$view = openDiv('.thumb'.(!$thumb ? ' hidden' : '')).($thumb ? 
			CHtml::image($thumb, $photo->name.'.'.$photo->ext) : '').closeDiv();
			$content .= $border ? openDiv('.border').$view.closeDiv() : $view;
		}
		$content .= closeDiv();
		return $content;
	}
	
	private function getButton()
	{
		$this->desc = false;
		$id = $this->data.'Set';
		$type = substr($this->attr, 5);
		$options = array();
		if ($type = 'Val') {
			foreach (array_keys($this->subItems) as $val) {
				$options[$val] = ucwords($val);
			}
		}
		$content = openDiv("#$id").CHtml::activeRadioButtonList($this->model, $this->data, 
		$options, array('separator'=>'')).closeDiv();
		$content .= $this->widget('application.components.ButtonSet', array(
			'buttonType'=>'buttonset',
			'id'=>$id,
		), true);
		return $this->unit($content);
	}
	
	private function getDropdown()
	{
		$multiple = strstr($this->attr, 'multi') !== false;
		$html = $values = array();
		if (strstr($this->attr, 'Val') !== false) {
			foreach ($this->subItems as $key=>$val) {
				$values[$key] = $val;
			}
			$relation = $this->data;
		} else {
			$condition = array('order'=>'name');
			$model = $this->model->relations();
			$relation = $model[$this->data][1];
			$options = $relation::model()->findAll($condition);
			foreach ($options as $data) {
				$values[$data->row_id] = $data->name;
			}
			foreach ($this->subItems as $item) {
				if ($item->class == 'none') {
					$values[''] = $item->desc;
				}
			}
		}
		$html['id'] = 'multi'.ucwords($relation);
		$expire = strstr($this->data, 'expiration') !== false;
		$label = $expire ? str_replace('expiration', '', $this->data) : false;
		$header = $label ? $label : 'Select '.($this->data != 'scope' ? $this->data : 'one');
		if ($this->data == 'deadline') $header = false; /*== category ? $this->desc = false;*/
		$click = 'js: function(event, ui) {$(this).multiselect("option", "classes", "")}';
		$close = 'js: function() {if ($(this).multiselect("getChecked").map(function() {'.
		'return this.value}).get() != "") {$(".taskForm").validate().element(this)}}';
		if ($this->data == 'scope') {
			$click = substr($click, 0, -1).'; juiAlert("Deadline has been set to " + '.
			'(ui.value == "expensive" ? "14" : "7") + " days.")}';
		}
		EHtml::activeDropdown($this->model, $this->data);
		$content = $this->multiSelect(array('values'=>$values), array('minWidth'=>183, 
		'click'=>$click, 'close'=>$close, 'selectedList'=>3), array('multiple'=>$multiple, 
		'header'=>$header, 'html'=>$html));
		return $this->unit($content);
	}
	
	private function getSubmit()
	{
		$content = ''; $design = false;
		foreach ($this->subItems as $item) {
			$class = strstr($item->class, ' ', true);
			if (!$class) $class = $item->class;
			if (!strcasecmp($item->desc, 'Buy Now!')) $class = 'buy';
			if (!strcasecmp($item->desc, 'Crop Settled!')) $class = 'crop';
			$model = $class == 'buy' || !strcasecmp($item->desc, 'Submit Now') ? 'Design' : 
			(!strcasecmp($item->desc, 'Sign In') ? 'Hub' : get_class($this->model));
			$id = (!$this->data ? $class : strtolower($item->desc)).$model;
			$html = array('id'=>$id, 'class'=>$item->class.($this->data ? ' inner' : ''), 
			'style'=>$this->data && !$item->combo ? 'display: none' : '');
			if (in_array($class, array('add', 'buy', 'crop', 'save')) && $model != 'Mail') {
				$edit = $this->attr; $type = $edit ? 'update' : 'add';
				$url = strtolower($model).'/'.(in_array($class, array('buy', 'crop')) ? 
				$class : $type); $js = 'routePage()'; $design = false;
				if ($buy = $class == 'buy') {
					$js = 'window.location = data.url';
					if (!subTotal($this->model)) $html['disabled'] = true;
				} else if ($design = $model == 'Design') {
					$js = 'function(data) {if (data.gain) {hideDesign()} else '.
					'if (data.error) {juiAlert(data.error)}}';
				} else if ($model == 'Person') {
					$js = 'routePage($("a.path").attr("href"))';
					$url = str_replace('add', 'update', $url);
				}
				$link = app()->createUrl($url, $buy ? array('gzip'=>0) : array());
				$content .= ($this->edit ? openDiv('.validate').closeDiv() : '').
				($this->desc == 'Submit' ? EHtml::submitButton($item->desc, $html) : 
				EHtml::ajaxSubmitButton($item->desc, $this->data ? '"'.$link.'"' : $link, 
				array('success'=>$design ? $js : ajaxSuccess($js)), $html));
				/*$js = $design ? 'function(data) {if (data.gain) {hideDesign()} else '.
				'if (data.error) {juiAlert(data.error)}}' : (!$this->data ? ($class == 
				'buy' ? 'window.location = data.url' : ($type == 'add' ? 'routePage()' : 
				'juiAlert("Success!")')) : "unionChange('$type', '$model')");*/
			} else {
				$js = $model == 'Mail' ? 'window.location = "'.app()->createUrl(
				'site/index').'"' : '';
				$content .= CHtml::button($item->desc, $html);
				cs()->registerScript($id, "jQuery('#$id').click(function(e) {
				e.preventDefault(); $js})");
				//$js = $this->data ? "unionChange('$item->class', '$model')" : '';
			}
		}
		$open = ($this->data ? 'form' : '').'submit';
		return !$design ? openDiv(".$open").$content.closeDiv() : $content;
	}
	
	private function getUpload()
	{
		$content = ''; $multiple = $this->attr > 1;
		$pick = 'Pick '.($multiple ? '1-'.$this->attr.' photos' : 'a photo');
		$selectNone = '<span class="active">'.str_replace('_', ' ', ucwords($this->id)).
		'</span><span class="inactive">'.$pick.'</span>';
		$join = !user()->isGuest ? array('person_id='.user()->id) : array();
		$divs = array('pick', 'hidden'); $join[] = 'hub_id='.hubId();
		foreach ($divs as $element) {
			$content .= openDiv(".$element").$this->multiSelect(array('model'=>File::model(), 'column'=>'display',
			'order'=>'upload_on DESC', 'options'=>array('type'=>'image', $join)), array('minWidth'=>177, 
			'position'=>array('my'=>'left bottom', 'at'=>'left top')), array('html'=>array('id'=>'pick'), 'compact'=>true, 
			'dataColumn'=>$element == 'hidden' ? 'file' : 'photo', 'multiple'=>$multiple, 'selectText'=>'none', 
			'click'=>"return maxPhotos(this, $this->attr)", 'selectNone'=>$selectNone), 'photo'); /*'header'=>$multiple ? $pick : null,*/
			if ($element == 'pick') {
				$content .= $this->widget('ext.ajaxUpload.EAjaxUpload', array(
					'class'=>'file',
					'postParams'=>array('type'=>'image'),
					'config'=>array(
						'action'=>app()->createUrl('file/upload'),
						'allowedExtensions'=>app()->params['imageExt'],
						'sizeLimit'=>app()->params['sizeLimit'],
						'onComplete'=>'js: function(id, name, data) {if (data.gain) {prependList('.
						'data, name, 3)} else {juiAlert("Image could not be uploaded!")}}',
					),
				), true);
			}
			$content .= closeDiv();
		}
		$shell = 'js: $(document.body)';
		QTip::tip('.multiphoto label', array('content'=>array('text'=>'js: function() {return previewImage($(this).find("input").val()'.
		', false)}'), 'position'=>array('my'=>'center left', 'at'=>'center right', 'viewport'=>'js: $(window)', 'container'=>$shell, 
		'adjust'=>array('x'=>16)), 'show'=>array('event'=>'mouseenter', 'solo'=>$shell), 'hide'=>array('event'=>'unfocus mouseleave', 
		'target'=>'js: $(this)'), 'style'=>array('classes'=>'ui-tooltip-tipsy ui-tooltip-preview', 'widget'=>true)), true);
		return $this->unit($content.$this->photoPreview());
	}
}

class ApprovePlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->title = param('live') ? 'You are cool!' : 'Hi There.';
		$tip = new Token(param('live') ? 'We have sent you an email '.
		'confirming your registration. Welcome to '.BRAND.'.' : 'Thanks for '.
		'registering! We will notify you when we are in beta.', null, 'span');
		$button = $this->submit('continue', 'Continue');
		
		unset($plot);
		$this->layout = get_defined_vars();
	}
}

class LoginPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->title = 'Sign In';
		$facebook = new Token('Connect with facebook', 'fbook');
		$email = new Token('Enter Email', 'email', 'text');
		$password = new Token('Enter Password', 'password', 'pass');
		$button = $this->submit('login', 'Sign In');
		$reset = new Token('Forgot Password?', 'reset');
		/*$tip = new Token(array('Hello there! Please login to continue.', 
		'Login with Facebook!'), 'span');
		$div = new Token('Or Login with email!', 'divider');*/
		
		unset($plot);
		$this->layout = get_defined_vars();
	}
}

class RegisterPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->title = 'Sign Up';
		$tip = new Token('We do not spam ever.', 'divider');
		$facebook = new Token('Connect with facebook', 'fbook');
		$div = new Token('Or signup with email.', 'divider');
		$first_name = new Token('First name', 'first_name', 'text');
		$last_name = new Token('Last name', 'last_name', 'text');
		$name = new Token('Organisation Name (Optional)', 'name', 'text');
		$email = new Token('Enter Email', 'email', 'text');
		$password = new Token('Choose Password', 'password', 'pass');
		$url = new Token('Show Us Your Portfolio Url', 'url', 'text');
		$city = new Token('City', 'city', app()->controller->action->id == 'index' ? 
		'auto' : 'text');
		$button = $this->submit('register', 'Sign Up');
		/*$tip = new Token(array('Got A Facebook Account?', 'We will never post '.
		'anything without your permission.'), 'span');*/
		
		unset($plot);
		$this->layout = get_defined_vars();
	}
}

class MessagePlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->back = array('Store', 'store/'.hash_route(null, $plot[1]->task->name, 'task'));
		$message = new Token('Button', 'work', 'thumb', 'button');
		$message->addChild(new Token(false, 'person_id', 'message', 'prefix'));
		$sample = new Token('Sample', null, 'thumb', 'sample');
		$control = new Token('Control', 'cart', 'control');
		$comments = new Token('MessageComments', 'row_id', 'zlist', 'active');
		
		unset($plot);
		$this->layout = get_defined_vars();
	}
}

class GridPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot); $bought = 0;
		list($work, $client, $member) = $this->plotMember($plot[1]);
		if ($work) {
			$this->back = array('Workspace', 'my/design');
		} else {
			$this->back = array('Job', 'job/'.hash_route(null, $plot[1]->name, 'task'));
		}
		$this->title = subTotal($plot[1], 0)." Submissions for ".CHtml::openTag('a', array(
		'href'=>'#!/job/'.hash_route(null, $plot[1]->name, 'task'))).'"'.$plot[1]->name.
		'"'.CHtml::closeTag('a').gridStatus($plot[1]);
		$post = new Token('Author', null, 'author');
		$post->addChild(new Token(false, 'person->profile', 'thumb'));
		$post->addChild(new Token(false, 'person->name', 'user'));
		$post->addChild(new Token(false, 'create_on', 'posted'));
		if((!$member || !$client) && $plot[1]->live == 'open') $post->addChild(new Token('Submit Your work', 'sub'));
		foreach ($plot[1]->designs as $design) {
			if ($design->sold == 1 && $client) {
				$bought++;
			}
		}
		if ($member && count($plot[1]->designs)) $note = $bought ? new Token('Thanks for your '.
		'purchase, '.$plot[1]->person->name.". You have $bought <span>NEW</span> Downloads ".
		'below', null, 'notice') : new Token('Click on work to view larger and have a private '.
		'conversation with '.(!$client ? 'client' : 'designer').'.', null, 'span', 'inline');
		$grid = new Token('Grid', 'row_id', 'zlist');
		if ($client && $plot[1]->live != 'archived') {
			$divider = new Token('', 'divider');
			$row = new Token($plot[1]->row_id, 'row_id', 'hidden');
			$total = new Token(array('sign'=>'Total', 'cost'=>'SGD'), 'price', 'span', 
			'inline');
			if ($plot[1]->live == 'closed') $button = $this->submit(false, 'Buy Now!');
		}
		$comments = new Token('TaskComments', 'row_id', 'zlist');
		
		unset($plot, $bought, $client, $design, $member, $work);
		$this->layout = get_defined_vars();
	}
}

class TaskPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot); $edit = $plot[0] == Plot::Edit;
		if ($edit || $plot[0] == Plot::Create) $this->title = $edit ? 'Edit '.$plot[1]->name : 
		'Tell Us About Your Project.';
		list($work, $client, $member) = $this->plotMember($plot[1]);
		if ($plot[0] == Plot::View || $edit) $this->back = $edit ? array('Job', 'job/'.
		hash_route(null, $plot[1]->name, 'task')) : (/*$work*/false ? array('Workspace', 
		'my/design') : array('Jobs', ''));
		if ($edit) $row = new Token($plot[1]->row_id, 'row_id', 'hidden');
		$category = new Token('What Do You Need?', 'category', 'dropdown', 'unaryRel'.
		($plot[0] == Plot::View ? '|hidden' : '')); //What Are These Photos For?
		$frame = new Token('See all...', 'row_id', 'frame');
		if ($plot[0] == Plot::View) $prize = new Token('Prize', 'price', 'textPrice');
		/*if ($plot[0] == Plot::View && (!$member || !$client) && $plot[1]->live == 'open') 
		$sub = new Token('Submit Your Work', 'sub');*/
		//Briefly Describe The Photo You Need
		//Main ethnic groups of Singapore at a construction website
		$name = new Token('Briefly Describe What You Are Looking For.', 'name', 'text'.($edit ? 
		'|hidden' : ''), 'Redesigned app icon to look nothing like Instagram.');
		$name->addChild(new Token(false, 'person->profile', 'thumb'));
		$name->addChild(new Token(false, 'person->name', 'user'));
		$name->addChild(new Token(false, 'create_on', 'posted'));
		$legend = new Token('Details', 'legend');
		$needs = new Token('Tell Us About Your App. Include As Much Relevant Details as Possible.', 
		'needs', 'textArea', 'We require an icon that portrays the hipster culture. It should be '.
		'fresh with lots of green, with the exception of dollar bills.');
		//In More Detail, What Is Your Creative Direction For These Photo(s)?
		//'4 to 5 professionals in their late 20s or early 30s in work attire '.
		//'at a construction worksite. They should be looking in the distance. Subjects using props '.
		//'such as clipboards are preferred.'
		/*$scope = new Token('Select your price range. Submission deadline will vary '.
		'accordingly.', 'price', 'dropdown', 'unaryVal'.($plot[0] == Plot::View ? 
		'|hidden' : ''));
		$scope->addItem('$400 - $650 SGD', 'moderate');
		$scope->addItem('$900 - $1500 SGD', 'expensive');
		$target_audience = new Token('Who is the Target Audience For This Project?', 
		'target', 'text', 'e.g. Children 8 to 12, Hipsters into photography.');
		$imagery_to_use = new Token('Enter imagery that you would like in your icon.', 
		'art', 'textArea', 'e.g. Camera lens shutter, the letter "P".');
		$specific_style = new Token('Describe the style of these imagery you want in detail. '.
		'Be specific. You can leave this up to the designer if you are open to suggestions.', 
		'style', 'textArea', 'e.g. Skeumorphic. Use shades of red.');
		$things_to_avoid = new Token('Describe things/imagery that the designer must avoid.', 
		'hate', 'textArea', 'e.g. Do not use any imagery of pigs.');*/
		if ($plot[0] != Plot::View) {
			$deadline = new Token('What Is Your Deadline For The Submissions', 'deadline', 
			'dropdown', 'unaryVal');
			$deadline->addItem('3 days', 3);
			$deadline->addItem('5 days', 5);
			$deadline->addItem('1 week', 7);
			$deadline->addItem('2 weeks', 14);
		}
		if ($plot[0] != Plot::View) $price = new Token('How Much Would Your Pay For Each Item? '.
		'(Min: SGD $300)', 'price', 'textPrice');
		$select_photos = new Token('Upload sketches, images of Ideas that might assist '.
		'the Designer', 'files', 'upload', 3);
		$brief_status = new Token('Job Status', 'live', 'option', 'unaryVal');
		$brief_status->addItem('Open', 'open');
		$brief_status->addItem('Closed', 'closed');
		$brief_status->addItem('Archived', 'archived');
		if ($plot[0] == Plot::View) $submissions_end_on = new Token(true, 'deadline_on', 'date');
		$comments = new Token('TaskComments', 'row_id', 'zlist', 'active');
		$button = $this->submit(false, 'Save', 'Submit Brief');
		
		unset($plot, $client, $edit, $member, $work);
		$this->layout = get_defined_vars();
	}
}

class UploadPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->back = array('Job', 'hideUpload()');
		$this->title = 'Submit your design';
		$rule = new Token('by', 'name', 'rule');
		$crop = new Token('crop', null, 'jcrop');
		$thumb = new Token('jcrop-thumb', null, 'thumb');
		$design = new Token(false, 'row_id', 'design');
		$tip = new Token(array('1. Upload your file and use the crop tool to create '.
		'the preview (above) the client will see when browsing your submission(s) '.
		'for this job.', '2. When you are satisfied, submit below after '.
		'accepting the design transfer agreement'), null, 'span', 'inline');
		$agree = new Token('By submitting, I agree that I have read and '.
		'accepted the Design Transfer Agreement', null, 'span', 'center');
		$button = $this->submit(false, 'Submit Now');
		
		unset($plot);
		$this->layout = get_defined_vars();
	}
}

class CreditPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->title = 'Credit Card details';
		$safe = user()->person->member_safe ? substr(strstr(decrypt(user()->
		person->member_safe), '|'), 1) : ''; $edit = $safe > '';
		if ($edit && user()->person->valid_card) {
			$mask = 'Your credit card is: XXXX - XXXX - XXXX - '.substr($safe, 0, 4).
			' (expires '.substr($safe, 4, 2).' / 20'.substr($safe, 6).')';
		} else {
			$mask = !$edit ? 'No credit card listed for your account.' : 'Your '.
			'credit card was rejected, add another card or contact support.';
			//$edit = false; unset($safe);
		}
		$setting = new Token(false, 'setting', 'hidden', 'credit');
		$label = new Token(array('When you are assigned a job, this credit card '.
		'will be charged. Refer to Terms and Conditions for details.', 'mask'=>$mask, 
		'You can '.($edit ? 'change' : 'add').' your credit card details by clicking', 
		'expand'=>'Add New Card.'), null, 'span', 'inner');
		$name = new Token('Full Name', 'cardholderName', 'text', 'hidden');
		$card = new Token('Card No.', 'number', 'text', 'hidden');
		$cvc = new Token('CVC', 'cvv', 'text', 'hidden');
		$month = new Token('Expires->month', 'expirationMonth', 'dropdown', 'unaryVal|hidden');
		for ($i = 1; $i <= 12; $i++) {
			$month->addItem("$i - ".date('M', strtotime("1989-$i")), sprintf("%02d", $i));
		}
		$year = new Token('year', 'expirationYear', 'dropdown', 'unaryVal');
		$date = date('Y') + (date('m') == 12 ? 1 : 0);
		for ($i = $date; $i <= $date + 16; $i++) {
			$year->addItem($i, $i);
		}
		$month->addChild($year, false);
		$button = $this->submit(false, 'Save', 'hidden');
		
		unset($plot, $edit, $mask, $safe, $date, $i);
		$this->layout = get_defined_vars();
	}
}

class EmailPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->title = 'Email Management';
		$setting = new Token(false, 'setting', 'hidden', 'email');
		$route = user()->person->new_email ?  user()->person->new_email : user()->person->email;
		$route .= verify() ? ' <strong>(verify thanks)</strong>' : '';
		$label = new Token(array("Your current email is $route", 'To change your '.
		'primary email, please fill in the following:'), null, 'span', 'inner');
		$email = new Token('Enter New Email', 'new_email', 'text', 'inner');
		$button = $this->submit(false, 'Save', 'inner');
		
		unset($plot, $route);
		$this->layout = get_defined_vars();
	}
}

class PaypalPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->title = 'PayPal details';
		$edit = user()->person->paypal_email !== null;
		$mask = $edit ? 'Your PayPal email account is: '.user()->person->paypal_email : 
		'No PayPal account listed for your account.';
		$setting = new Token(false, 'setting', 'hidden', 'paypal');
		$label = new Token(array('This is the PayPal account we will use to pay you '.
		'for services you have procured.', 'mask'=>$mask, 'You can '.($edit ? 
		'change' : 'add').' your PayPal account '.($edit ? 'details ' : '').
		'by clicking', 'expand'=>'Add New Email.'), null, 'span', 'inner');
		$email = new Token('Email', 'paypal_email', 'text', 'hidden');
		$button = $this->submit(false, 'Save', 'hidden');
		
		unset($plot, $edit, $mask);
		$this->layout = get_defined_vars();
	}
}

class PasswordPlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->title = 'Password Management';
		$setting = new Token(false, 'setting', 'hidden', 'password');
		$label = new Token(array('To ensure your safety, do not give out your password '.
		'to anyone else.'), null, 'span', 'inner'); $reset = user()->person->reset_pass;
		if ($reset === null) $old = new Token('Enter Current Password', 'current_pass', 'pass', 'inner');
		$new_pass = new Token('Enter New Password', 'password', 'pass', 'inner');
		$repeat_pass = new Token('Repeat New Password', 'repeat_pass', 'pass', 'inner');
		$button = $this->submit(false, 'Save', 'inner');
		
		unset($plot, $reset);
		$this->layout = get_defined_vars();
	}
}

class PreparePlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$this->back = array('Store', 'store/'.hash_route(null, $plot[1]->task->name, 'task'));
		if ($plot[1]->ready) {
			$large = new Token(false, 'work', 'large');
		} else {
			$button = $this->submit(false, 'Crop Settled!');
			$crop = new Token('recrop', 'work', 'jcrop');
		}
		
		unset($plot);
		$this->layout = get_defined_vars();
	}
}

class ProfilePlan extends Plan
{
	public function __construct($plot)
	{
		parent::__construct($plot);
		$edit = $plot[0] == Plot::Edit;
		if ($edit) $this->back = array('Profile', 'my/profile');
		$profile = new Token('Profile picture', 'profile', 'upload', $edit ? 1 : null);
		$profile->addChild(new Token(false, 'name', 'text'));
		$first_name = new Token('First name', 'first_name', 'text', 'hidden');
		$last_name = new Token('Last name', 'last_name', 'text', 'hidden');
		$phone = new Token('Phone number', 'phone', 'text', 'hidden');
		$city = new Token('City name', 'city', 'text', 'hidden');
		$comments = new Token('ProfileComments', 'row_id', 'zlist', 'active');
		$button = $this->submit(false, 'Save Profile');
		/*$profile->addChild(new Token(false, 'valid_card', 'hire'));
		$profile->addChild(new Token(false, 'email', 'text'));
		$profile->addChild(new Token(false, 'phone', 'text'));
		$profile->addChild(new Token(false, 'create_on', 'dateNice'));*/
		
		unset($plot, $edit);
		$this->layout = get_defined_vars();
	}
}
