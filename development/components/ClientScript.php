<?php

/**
 * ClientScript is the customized base client script class.
 */
class ClientScript extends CClientScript
{
	private $core = null;
	
	public function assetPath($url, $abs=0)
	{
		$base = app()->request->baseUrl;
		if (strstr($url, 'assets')) {
			$url = str_replace($base, '', $url);
			$dir = param('baseDir');
			$base = $abs || !DEV ? $dir : '.';
		} else if ($abs) {
			return $url;
		}
		return $base.$url;
	}
	
	private function core()
	{
		if ($this->core === null) {
			$this->core = app()->controller->module ? false : true;
		}
		return $this->core;
	}
	
	public function registerCssFile($url, $media='')
	{
		if ($this->core()) {
			app()->dynamicRes->registerCssFile($this->assetPath($url));
		} else {
			parent::registerCssFile($url, $media);
		}
	}
	
	public function registerScriptFile($url, $position=self::POS_HEAD)
	{
		if ($this->core()) {
			app()->dynamicRes->registerScriptFile($this->assetPath($url));
		} else {
			parent::registerScriptFile($url, $position);
		}
	}
	
	public function registerCoreScript($name)
	{
		if (!param('export')) parent::registerCoreScript($name);
	}
	
	public function registerLinkTag($relation=null, $type=null, $href=null, 
	$media=null, $options=array())
	{
		if (param('export')) $href = ".$href";
		parent::registerLinkTag($relation, $type, $href, $media, $options);
	}
	
	public function mergeCssFile($url)
	{	
		$this->hasScripts=true;
		$url = $this->assetPath($url, 1);
		if (param('export') && $url[0] == '/') $url = ".$url";
		$this->cssFiles[$url] = '';
		$this->cssFiles = array_reverse($this->cssFiles);
	}
	
	public function mergeScriptFile($url)
	{
		$this->hasScripts=true;
		$url = $this->assetPath($url, 1);
		if (param('export') && $url[0] == '/') $url = ".$url";
		$this->scriptFiles[self::POS_HEAD][$url] = $url;
		return $this;
	}
}
