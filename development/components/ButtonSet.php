<?php

Yii::import('zii.widgets.jui.CJuiButton');

class ButtonSet extends CJuiButton
{
	public $class = null;
	
	public function init() {
		if ($this->class) {
			$this->htmlOptions['class'] = $this->class;
			parent::init();
		}
	}
	
	public function run() {
		$id = $this->getId();
		
		if ($this->class) {
			global $header;
			if (!is_array($header)) {
				$header = array();
			}
			echo CHtml::closeTag($this->htmlTag);
			if (!in_array($this->class, $header)) {
				cs()->registerScript($id, "function button$this->class() {jQuery('.$this->class').buttonset()}");
			}
			$header[] = $this->class;
		} else {
			cs()->registerScript($id, "jQuery('#$id').buttonset();");
		}
	}
}