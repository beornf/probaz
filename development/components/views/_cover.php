<?php
	$browse = $this->panel == 'Browse';
	$job = $this->panel == 'Space';
	$comment = $this->model == 'Wall';
	$grid = $this->model == 'Design';
	$task = $this->model == 'Task';
	$default = photoDefault();
	$member = !user()->isGuest;
	if ($comment) {
		$class = 'comment';
		$field = array('person', 'task', 'message', 'type', 'record_on');
		$artist = $data->task ? $data->person_id != $data->task->person_id : false;
	} else if ($grid) {
		$class = 'cell';
		$field = array('cart', 'message', 'ready', 'sold', 'mirror', 'person', 'task', 'work');
		$artist = $member && user()->id == $data->person_id;
		$client = $member && user()->id == $data->task->person_id;
	} else if ($task) {
		$class = 'task';
		$field = array('name', 'category', 'needs', 'designs', 'price', 'person', 
		'live', 'create_on', 'phase_on');
	}
	foreach ($field as $label) {
		$$label = $data->$label;
	}
	$brief = $this->filter == 'brief';
	$sub = $this->filter == 'sub';
	$profile = $this->member == 'person';
	if ($person === null || ($profile && $artist)) $person = $task->person;
	$photo = $person->profile ? $person->profile : $default;
	$openSpan = CHtml::openTag('span', array('class'=>'nowrap'));
	$closeSpan = CHtml::closeTag('span');
	$newline = '<br><br>';
	$options = $class != 'task' ? array('data-model'=>strtolower($this->model).
	$data->row_id) : ($job ? array('class'=>'job') : array());
	if ($profile) $class .= ' person';
	echo openDiv(".$class", $options);
	if ($comment) : ?>
	<?php if (!$profile) : ?>
		<div class="person">
			<?php echo thumbnail($photo); ?>
		</div>
	<?php endif; ?>
	<?php //if (!$profile || $artist) : ?>
	<div class="author">
		<a class="systemUser" data-model="person<?php echo $person->row_id; ?>" href="<?php 
		echo hash_route('', $person, 'profile'); ?>" ><?php echo $person->name; ?></a>
	</div>
	<div class="log">
		<?php if ($type == 'message') {
			$log = 'said:';
		} else {
			$sale = $type == 'sale';
			if ($sale) {
				$action = ($profile ? 'designed' : 'sold').' a work for';
				/*$action = ($data->work ? 'submitted' : ($profile ? 'designed' : 
				'sold')).' work for';*/
			} else {
				$action = $type.(substr($type, -1) == 'e' ? 'd' : 'ed');
			}
			if ($profile) {
				$log = ($sale ? $action : 'posted the brief').' “'.CHtml::link($task->name, 
				hash_route('job', $task, 'task'), array('class'=>'systemPage')).'”';
			} else {
				$log = "$action ".($sale ? '$'.$task->price : 'this brief');
			}
		}
		echo $log; ?>
	</div>
	<?php if ($type == 'message') : ?>
		<div class="message"><?php echo nl2br($message); ?></div>
	<?php endif; ?>
	<div class="entry">
		<?php echo nice_date($record_on); ?>
	</div>
<?php elseif ($grid) : ?>
	<?php $pick = array(); $info = $type = false; $href = '#'; $route = $artist || $client;
	if ($client && $control = productControl($cart, $sold, $task->status)) {
		list($desc, $type) = $control;
		if ($sold && $ready == 2) {
			$href = packUrl($task, $mirror);
			$pick = array('target'=>'_blank');
		}
	} else if ($artist) {
		$desc = 'My Submission';
		$info = true;
	} else {
		$desc = $person->name;
		$href = hash_route('', $person, 'profile');
	}
	$msg = ($artist && $message == 1) || ($client && $message == 2);
	$path = ImageHelper::thumb(Thumbs::Grid, $work, $data);
	$pick['class'] = $type ? $type : 'simple'; $add = strpos($type, 'add') !== false;
	$sub = ($route ? CHtml::openTag('a', array('href'=>hash_route('message', array($task, 
	$data), 'task'))) : '').glossThumb(Thumbs::Grid).($path ? CHtml::closeTag('a') : '');
	$sub .= CHtml::image($path, "Submission #$mirror", array('class'=>'icon'));
	$sub .= openDiv('.hook', array('class'=>($msg ? 'msg ' : '').(!$type ? 'link' : '')));
	$sub .= $msg ? CHtml::image('/images/msg.png', 'New PM', array('class'=>$add ? 'add' : '')) : '';
	$prepare = array('class'=>'simple '.($ready ? 'ready' : 'hold'));
	echo $sub.($info ? $desc : CHtml::link($desc, $href, $pick)).(user()->admin ? 
	CHtml::link('Prepare', hash_route('prepare', array($task, $data), 'task'), 
	$prepare) : '').closeDiv(); ?>
<?php elseif ($task) : ?>
	<?php if ($browse || $sub) : ?>
		<div class="photo"><div class="middle">
			<?php echo thumbnail($photo).$openSpan.$person->first_name.$closeSpan; ?>
		</div></div>
	<?php endif; ?>
	<div class="description">
		<a class="systemPage nowrap" href="<?php
			$title = $job ? $name.' - '.$category->name : $name;
			echo hash_route('job', $data, 'task'); ?>"><?php echo $title; ?></a>
		<br><br><span class="nowrap"><?php if ($browse) {
			echo $needs;
		} else if ($brief) {
			if ($live == 'archived') {
				$sold = array();
				foreach ($designs as $design) {
					if ($design->sold) {
						$sold[] = CHtml::openTag('span').$design->person->name.CHtml::closeTag('span');
					}
				}
				echo count($sold) ? 'You have purchased a design from '.implode(' and ', $sold) : 
				'Not bought.';
			} else {
				echo 'You can purchase between '.count($designs).' submissions.';
			}
		} else if ($sub) {
			if (Design::model()->count('task_id=:rid AND person_id=:pid AND sold', 
			array(':rid'=>$data->row_id, ':pid'=>user()->id))) {
				echo CHtml::openTag('span').$person->name.CHtml::closeTag('span').
				' purchased a design from you.';
			} else {
				echo 'Not sold.';
			}
		} ?></span>
	</div>
	<?php if ($browse) : ?>
		<div class="info"><div class="middle"><?php
			echo $openSpan.($person->city ? $person->city : $person->hub->countryCode->name);
			echo $closeSpan.$newline.$openSpan.nice_date($create_on).$closeSpan;
		?></div></div>
		<div class="price"><div class="middle"><?php
			echo CHtml::submitButton('$'.currency($price), array('class'=>'systemWork navigation'));
		?></div></div>
	<?php elseif ($job) : ?>
		<?php if ($brief) : ?><span class="total theme"><?php
			echo count($designs).' Submissions';
		?></span><?php endif; ?>
		<?php $class = $live == 'open' ? 'leaf' : ($live == 'closed' ? 'ocean' : ''); ?>
		<div class="mode"><a href="<?php echo hash_route('store', $data, 'task'); ?>" 
		class="<?php echo $class; ?>"><?php echo ucwords($live); ?></a></div>
		<div class="state"><div class="middle"><?php
			$phase = nice_date($phase_on);
			if ($live == 'open') {
				echo $phase;
			} else if ($live == 'closed') {
				echo substr($phase, 0, strrpos($phase, ' ')).' to '.($brief ? 'buy' : 'sell');
			} else {
				echo 'On '.date('j M Y', strtotime($phase_on));
			}
		?></div></div>
	<?php endif; ?>
<?php endif; echo closeDiv(); ?>
