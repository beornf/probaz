<?php

/**
 * PrivateIdentity represents the data needed to identity a hub.
 */
class PrivateIdentity extends CUserIdentity
{
	private $_id;
	
	/**
	 * Authenticates the user password.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$person = Person::model()->findByAttributes(array('email'=>$this->username, 
		'locked'=>0));
		if ($person === null || $this->password === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} else if (!authorize($this->password, $person)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$this->_id = $person->row_id;
			$this->errorCode = self::ERROR_NONE;
		}
		return $this->errorCode;
	}
	
	public static function createIdentity($id, $name)
	{
		$identity = new self($name, '');
		$identity->_id = $id;
		$identity->errorCode = self::ERROR_NONE;
		return $identity;
	}
	
	public function getId()
	{
		return $this->_id;
	}
}