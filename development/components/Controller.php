<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/design';
	
	/**
	 * Determine user data and region
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action)) return false;
		if (state_new('refine')) { /* || count(state('refine')) != count(multiRefine())*/
			$refine = array();
			foreach (multiRefine() as $select) {
				if (array_key_exists('model', $select)) {
					$refine[] = -1;
				} else {
					$keys = array_keys($select['values']);
					$refine[] = $keys[0];
				}
			}
			state('refine', $refine);
		}
		if (app()->request->isAjaxRequest) return true;
		try {
			// fetch location, make active and fb login
			if (state_new('customer')) fetchLocation();
			if (state_new('customer')) {
				throw new CHttpException(403, "Access denied. Probaz isn't available in your region right now.");
			}
			if ($ie = ie_version()) if ($ie < 9) $this->redirect(MS);
			if (!user()->isGuest) {
				user()->newActive();
			} else if ($data = facebook_api()) {
				$birthday = isset($data['birthday']) ? date('Y-m-d', strtotime($data['birthday'])) : null;
				$person = Person::model()->find('facebook_id=:id', array(':id'=>$data['id']));
				if ($person === null) {
					$person = Person::model()->find('email=:val', array(':val'=>$data['email']));
					if ($person === null) {
						$city = isset($data['location']) ? strstr($data['location']['name'], 
						',', true) : client('city');
						if (!$city) $city = $data['location']['name'];
						$save = array('first_name'=>false, 'last_name'=>false, 'email'=>false,
						'birthday'=>$birthday, 'city'=>$city, 'facebook_id'=>$data['id']);
						$post = array();
						foreach ($save as $id=>$key) {
							$post[$id] = $key ? $key : $data[$id];
						}
						registerProfile($post, Chain::Customer, true);
					} else {
						$person->facebook_id = $data['id'];
						$person->save();
					}
				}
				if ($person && !$person->locked) {
					$struct = array('first_name'=>false, 'last_name'=>false, 'birthday'=>$birthday); 
					$update = false;
					foreach ($struct as $id=>$key) {
						$value = $key ? $key : $data[$id];
						if ($value != $person[$id]) {
							$person[$id] = $value;
							$update = true;
						}
					}
					if ($update) {
						$person->name = $person->first_name.' '.$person->last_name[0];
						$person->save();
					}
					loginProfile($person);
				}
			}
			state_new('agent', Agent::Browse);
			state_new('ajax', array('index', null));
			state_new('hide', false);
			state_new('upload', false);
		} catch (CHttpException $ex) {
			$this->render('error', array('code'=>$ex->statusCode, 'message'=>$ex->getMessage()));
			return false;
		}
		return true;
	}
	
	/*
	 * Generates compressed javascript and css files.
	 */
	protected function afterRender($view, &$output)
	{
		Yii::app()->dynamicRes->saveScheme();
	}
	
	/*
	 * Creates access control filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	function filterAccessControl($filterChain) {
		$rules = $this->accessRules();
		$deny = implode(' || ', param('deny'));
		array_unshift($rules, array('deny', 'expression'=>$deny));
		
		$rules[] = array('allow', 'actions'=>array('error'));
		$rules[] = array('deny');
		$filter = new ControlFilter;
		$filter->setRules($rules);
		$filter->filter($filterChain);
	}
}