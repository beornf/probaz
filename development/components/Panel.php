<?php

abstract class Panel
{
	private $id;
	private $name;
	private $model;
	private $hashUrl;
	private $itemView;
	private $itemsCssClass;
	private $agent;
	private $bind;
	private $category;
	private $dataProvider;
	private $filter;
	private $member;
	private $panel;
	private $row;
	private $size;
	
	public function __construct($name, $model, $hash='', $list=true, $size=0)
	{
		$plan = strtolower($model); $task = $plan == 'task';
		$config = array('id'=>isset($this->row) ? $plan : 'system', 'name'=>$name, 'model'=>$model, 
		'hashUrl'=>$hash, 'itemView'=>'_cover', 'panel'=>get_class($this), 'size'=>$size, 
		'itemsCssClass'=>$task ? 'plan' : '', 'filter'=>false);
		foreach ($config as $key=>$val) {
			if (!isset($this->$key)) {
				$this->$key = $val;
			}
		}
		if ($task) {
			foreach (multiRefine() as $i=>$multi) {
				if ($multi['type'] == 'Channel' && $multi['panel'] == $this->panel) {
					$refine = state('refine');
					$this->filter = $refine[$i];
					break;
				}
			}
		}
		if ($list) $this->dataProvider = $this->provider();
	}
	
	public function __get($property)
	{
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	public function __set($property, $value)
	{
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
	}
	
	public function dbCriteria()
	{
		$criteria = new CDbCriteria();
		if ($this->model == 'Task') {
			if ($this->panel == 'Space' && $this->filter) {
				$relation = 'person_id=:pid';
				if ($this->filter == 'sub') {
					$criteria->join = 'INNER JOIN design ON t.row_id = design.task_id';
					$relation = "design.$relation";
				}
				if ($this->filter == 'sub') $criteria->join = 'INNER JOIN design ON '.
				't.row_id = design.task_id';
				$criteria->distinct = true;
				$criteria->addCondition($relation);
				$criteria->params = array(':pid'=>user()->id);
			}
			$criteria->addCondition('t.person_id IN (SELECT row_id FROM person WHERE '.
			'valid_email=1'.(!user()->admin ? ' AND member!="admin"': '').')');
		} else if ($this->model == 'Design') {
			$criteria->addCondition('task_id=:rid');
			$criteria->params = array(':rid'=>$this->row);
		} else if ($this->model == 'Wall') {
			$criteria->addCondition("{$this->member}_id=:rid");
			$criteria->addCondition('block=0');
			$criteria->params = array(':rid'=>$this->row);
			$action = 'type in ("post", "sale")';
			if ($this->member == 'person') $criteria->addCondition($action);
		}
		list($filter, $order) = $this->param(); $i = 0;
		foreach ($filter as $field=>$param) {
			if ($param != '-1') {
				$criteria->addCondition("$field=:val$i");
				$criteria->params[":val$i"] = $param;
				if ($model = strstr($field, '.', true)) {
					$criteria->with = $model;
				}
				$i++;
			}
		}
		$criteria->order = $order;
		return $criteria;
	}
	
	public function iterate()
	{
		$vars = array();
		foreach ($this as $key=>$val) {
			if ($val) {
				$vars[$key] = $val;
			}
		}
		return $vars;
	}
	
	private function param()
	{
		$bound = array(); $order = $root = ''; $multi = multiRefine();
		$state = $this->panel == 'Browse' ? state('refine') : array();
		foreach ($state as $i=>$refine) {
			if ($multi[$i]['type'] == 'Filter') {
				$bound[$multi[$i]['data']] = $refine;
			} else if ($multi[$i]['type'] == 'Order') {
				$ascend = $refine[0] != '-';
				$sort = $ascend ? $refine : substr($refine, 1);
				$order .= 't.'.$sort.' '.($ascend ? 'ASC' : 'DESC').', ';
			}
		}
		if ($this->panel == 'Space') {
			$root = 't.status, CASE WHEN t.status = "archived" THEN t.phase_on END DESC, '.
			'CASE WHEN t.status != "archived" THEN t.phase_on END';
		} else if ($this->model == 'Design') {
			$root = 't.sold_on DESC, t.create_on DESC';
		} else if ($this->model == 'Task') {
			$root = 't.create_on DESC, t.name';
		} else if ($this->model == 'Wall') {
			$root = 't.record_on DESC';
		}
		return array($bound, $order.$root);
	}
	
	private function provider()
	{
		return new CActiveDataProvider($this->model, array(
			'criteria'=>$this->dbCriteria(),
			'pagination'=>$this->size ? array('pageSize'=>$this->size) : false,
		));
	}
	
	public function subCount()
	{
		$item = $this->model;
		return $item::model()->count($this->dbCriteria());
	}
	
	public static function Agent($agent, $row=null, $size=0)
	{
		$type = strlen($agent) == 2 ? Agent::Model($agent) : $agent;
		$model = $row !== null ? new $type($row) : new $type(true, $size);
		$model->agent = constant("Agent::$type");
		return $model;
	}
	
	public static function Title($agent)
	{
		$type = Agent::Model($agent);
		$model = new $type(true);
		return $model->name;
	}
	
	public static function Group()
	{
		$reflection = new ReflectionClass('Agent');
		$enum = $reflection->getConstants();
		$panel = array();
		foreach ($enum as $type=>$val) {
			if ($val[0] == 'c') {
				$model = new $type(false);
				$model->agent = $val;
				$panel[] = $model;
			}
		}
		return array_reverse($panel);
	}
	
	public function display($hide=false, $scrape=false, $skin=false)
	{
		$base = new CController('base'); $none = 'display: none';
		$options = array('agent'=>$this->agent, 'hide'=>$hide, 'row'=>$this->row, 
		'scrape'=>$scrape, 'htmlOptions'=>$skin ? array('style'=>$none) : array());
		return $base->widget('application.components.ListView', $options, true);
	}
	
	public function menu($label=null, $focus=false)
	{
		$options = array('class'=>'navItem'.($focus ? ' focus' : ''), 
		'data-panel'=>$this->agent);
		$content = CHtml::Link($label ? $label : $this->name, 
		hash_route($this->hashUrl), $options);
		$action = $this->hashUrl != '' ? substr(strstr($this->hashUrl, '/'), 1) : '';
		return $action != '' ? (openDiv(".$action").$content.closeDiv()) : $content;
	}
}

class MessageComments extends Panel
{
	public function __construct($row)
	{
		$this->member = 'design';
		$this->row = $row;
		parent::__construct('Private message', 'Wall');
	}
}

class ProfileComments extends Panel
{
	public function __construct($row)
	{
		$this->member = 'person';
		$this->row = $row;
		parent::__construct(array('Activity'), 'Wall');
	}
}

class TaskComments extends Panel
{
	public function __construct($row)
	{
		$this->member = 'task';
		$this->row = $row;
		parent::__construct('Conversations', 'Wall');
	}
}

class Browse extends Panel
{
	public function __construct($list, $size=0)
	{
		parent::__construct('iPhone App Icons', 'Task', '', $list, $size);
	}
}

class Grid extends Panel
{
	public function __construct($row)
	{
		$this->row = $row;
		parent::__construct('Submissions', 'Design');
	}
}

class Accounts extends Panel
{
	public function __construct()
	{
		parent::__construct('Account', 'Account', 'my/account');
	}
}

class Profile extends Panel
{
	public function __construct()
	{
		parent::__construct('My Profile', 'Person', 'my/profile');
	}
}

class Space extends Panel
{
	public function __construct($list, $size=0)
	{
		parent::__construct('My Design', 'Task', 'my/design', $list, $size);
	}
}
