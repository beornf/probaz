<?php

Yii::import('zii.widgets.jui.CJuiDialog');

class Dialog extends CJuiDialog
{
	public $content = '';
	public $class = null;
	
	public function init() {
		$id = $this->getId();
		if (isset($this->htmlOptions['id'])) {
			$id = $this->htmlOptions['id'];
		} else {
			$this->htmlOptions['id'] = $id;
		}
		
		$options = empty($this->options) ? '' : CJavaScript::encode($this->options);
		if ($this->class) {
			global $header;
			if (!is_array($header)) {
				$header = array();
			}
			if (!in_array($this->class, $header)) {
				cs()->registerScript($id, "function dialog$this->class() {jQuery('.$this->class').dialog($options)}");
			}
			$header[] = $this->class;
			$this->htmlOptions['class'] = $this->class;
		} else {
			cs()->registerScript($id, "jQuery('#$id').dialog($options)");
		}
		echo CHtml::openTag($this->tagName, $this->htmlOptions);
	}
	
	public function run() {
		echo $this->content.CHtml::closeTag($this->tagName);
	}
}