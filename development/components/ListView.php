<?php

Yii::import('zii.widgets.CListView');

/**
 * ZListView extends the list view widget to render javascript
 * event handlers at the bottom of the hub list
 */
class ListView extends CListView
{
	public $template = "{header}\n{sorter}\n{jscript}\n{items}\n{pager}";
	public $agent = Agent::Browse;
	public $model = null;
	public $row = null;
	public $hide = false;
	public $scrape = false;
	public $state = -1;
	public $bind = '';
	public $emptyText = '';
	public $filter = '';
	public $name = '';
	public $member = '';
	public $paid = '';
	public $panel = '';
	public $route = '';
	public $update = '';
	public $val = '';
	public $loadingCssClass = 'loading';
	
	/**
	 * Set properties of panel
	 */
	public function init()
	{
		$panel = Panel::Agent($this->agent, $this->row, $this->scrape ? 40 : 15);
		foreach ($panel->iterate() as $key=>$val) {
			if (property_exists($this, $key) || $key == 'id') {
				$this->$key = $val;
			}
		}
		$system = $this->id == 'system';
		if ($system) {
			foreach (multiRefine() as $i=>$options) {
				if ($options['type'] == 'Order') {
					$this->bind .= "multirefine$i(); ";
					break;
				}
			}
			$this->bind .= 'hideMarket(); updateList()'; //hashAccount();
			/*$multi = array('month', 'year');
			foreach (multiRefine() as $i=>$options) {
				if ($options['type'] == 'Order') {
					$multi[] = "refine$i";
				}
			}
			foreach ($multi as $class) {
				$this->bind .= "multi$class(); ";
			}*/
		} else if ($this->id == 'wall') {
			$url = app()->createUrl('wall/list', array('agent'=>$this->agent, 'row'=>$this->row));
			$this->update = '$.fn.yiiListView.update("wall", {url: "'.$url.'"})';
			$this->bind = '$("#wall textarea").autosize()';
			/*$agent = $system ? Agent::TaskComments : $this->agent;
			$row = $system ? '' : $this->row; 
			($system ? ' + match' : '')*/
		}
		if ($this->itemsCssClass == 'plan') {
			$this->pager = array(
				'class'=>'ext.infiniteScroll.IasPager',
				'itemsSelector'=>".".$this->itemsCssClass,
				'listViewId'=>$this->id,
				'rowSelector'=>'.task',
				'header'=>'',
			);
		}
		/*$struct = array('paid'=>'finish', 'route'=>'send');
		foreach ($struct as $key=>$class) {
			$this->$key = dialogBox($class, array(510, 208), true);
		}
		if (!DEV) {
			$this->htmlOptions['class'] = 'touch';
		}*/
		if ($this->hide) {
			$this->htmlOptions['class'] = 'fixed';
		}
		if ($system) {
			$this->afterAjaxUpdate = 'js:function(id, data) {'.$this->bind.'}';
		}
		parent::init();
	}

	/**
	 * Render the header section
	 */
	public function renderHeader()
	{
		if ($this->id == 'system') {
			$dock = false;
			$caption = $this->name;
			$content = $headline = '';
			$space = $this->panel == 'Space';
			$refine = state('refine');
			foreach (multiRefine() as $i=>$options) {
				$desc = ''; $values = array();
				$type = $options['type'];
				if (array_key_exists('model', $options)) {
					$model = $options['model'];
					foreach ($model::model()->findAll(array('order'=>'name')) as $item) {
						$values[$item->row_id] = $item->name;
					}
					//$values[-1] = 'All '.str_replace('y', 'ies', $model);
				} else {
					//$desc = $type == 'Order' ? $options['desc'] : '';
					$desc = 'Sort:';
					$values = $options['values'];
				}
				if ($type == 'Order') {
					$html = array();
					$select = $refine[$i];
					if (array_key_exists($select, $values)) {
						$html['options'][$select]['selected'] = 'selected';
					}
					$desc = $desc != '' ? "<span>$desc</span>" : '';
					$multi = $desc.$this->widget('ext.multiselect.EMultiSelect', array('active'=>false, 
					'class'=>'refine'.$i, 'listModel'=>array('values'=>$values), 'type'=>$type, 
					'config'=>array('minWidth'=>134), 'multiple'=>false, 'call'=>true, 
					'click'=>"refineSystem(ui.value, null, $i)", 'html'=>$html), true);
					$headline .= $options['panel'] != $this->panel ? CHtml::openTag('span', 
					array('class'=>'hidden')).$multi.CHtml::closeTag('span') : $multi;
				} else if ($options['panel'] == $this->panel) {
					$this->val = $refine[$i];
					foreach ($values as $id=>$item) {
						$hash = '#';
						$link = array('class'=>'navRefine');
						if ($type != 'Filter') {
							$link['data-type'] = $type;
						} else {
							$hash = hash_route('category', $item);
						}
						$link['data-key'] = $id;
						$focus = $id == $this->val ? array('class'=>'focus') : array();
						if (count($focus)) $caption = $item;
						$content .= CHtml::openTag('li', $focus).CHtml::link($item, $hash, $link).CHtml::closeTag('li');
					}
					$dock = $space ? 'lead' : 'menu';
				}
			}
			$menu = $this->panel != 'Browse' ? '<h2>'.($this->panel == 'Browse' ? 'App Category' : 
			($space ? 'My Workspace' : $this->panel)).'</h2>'.CHtml::openTag('ul').$content.
			CHtml::closeTag('ul') : ''; if ($this->panel == 'Profile') {
				$caption = ''; $dock .= ' hide';
			}  else {
				$caption = 'General Projects';
			}
			if ($dock) echo openDiv(".$dock").openDiv('#post').CHtml::link('Post A Brief', hash_route(
			'', 'post')).closeDiv().$menu.closeDiv();
			if (!$space) echo openDiv('.headline').openDiv('.middle').'<h1>'.$caption.'</h1>'.closeDiv().
			openDiv('.refine', array('class'=>'middle')).$headline.closeDiv().closeDiv();
			//echo openDiv('.divider').closeDiv();
		} else if ($this->id == 'wall') {
			$heap = is_array($this->name);
			$header = openDiv('.'.($heap ? 'heap' : 'desc'));
			if ($heap) {
				foreach ($this->name as $tag) {
					$header .= CHtml::openTag('span', array('class'=>strtolower($tag))).
					$tag.CHtml::closeTag('span');
				}
			} else {
				$header .= $this->name;
			}
			echo openDiv('.light').closeDiv().$header.closeDiv();
			if (!($heap || user()->isGuest)) echo EHtml::beginForm().CHtml::activeTextArea(new Wall, 
			'message').EHtml::ajaxSubmitButton('REPLY', app()->createUrl('wall/message', 
			array('rid'=>$this->row, 'member'=>$this->member)), array('success'=>
			"function(data) {if (data.gain) {$this->update}}", 'beforeSend'=>'function() {'.
			'$("#wall textarea").val("")}'), array('class'=>'wallMessage')).EHtml::endForm();
		}
	}

	/**
	 * Render the data item list
	 */
	public function renderItems()
	{
		if ($this->panel == 'Accounts') {
			$menu = array();
			if ($this->val == 'security') {
				if (user()->person->reset_pass === null) $menu[] = 'email';
				if (user()->person->password) $menu[] = 'password';
			} else if ($this->val == 'payment') {
				$menu[] = 'paypal'; //'credit'
			} else if ($this->val == 'history') {
				
			}
			foreach ($menu as $plan) {
				echo openDiv('.track', array('class'=>$this->val)).Plan::Model(
				ucwords($plan), Plot::Edit, new Account)->html().closeDiv();
			}
		} else if ($this->panel == 'Profile') {
			echo openDiv('.profile').Plan::Model('Profile', Plot::View, 
			user()->person)->html().closeDiv();
		} else {
			parent::renderItems();
		}
	}

	/**
	 * Render javascript event handlers
	 */
	public function renderJScript()
	{
		if ($this->id == 'system') {
			// TODO move into separate file with loop and ajaxSuccess
			EHtml::ajaxSubmitButton('', app()->createUrl('design/add'), array('success'=>'function(data) {'.
			'if (data.gain) {hideDesign()} else if (data.error) {juiAlert(data.error)}}'), array('id'=>'addDesign'));
			EHtml::ajaxSubmitButton('', app()->createUrl('design/buy', array('gzip'=>0)), array('success'=>
			ajaxSuccess('window.location = data.url')), array('id'=>'buyDesign'));
			EHtml::ajaxSubmitButton('', app()->createUrl('design/crop'), array('success'=>'function(data) {'.
			'if (data.gain) {routePage($("a.path").attr("href"))}}'), array('id'=>'cropDesign'));
			EHtml::ajaxSubmitButton('', app()->createUrl('person/account'), array('success'=>ajaxSuccess(
			'updateDisplay()')), array('id'=>'saveAccount'));
			EHtml::ajaxSubmitButton('', app()->createUrl('person/update'), array('success'=>ajaxSuccess(
			'routePage($("a.path").attr("href"))')), array('id'=>'savePerson'));
			EHtml::ajaxSubmitButton('', app()->createUrl('task/add'), array('success'=>ajaxSuccess(
			(user()->isGuest ? 'signUp(); ' : '').'routePage()')), array('id'=>'addTask'));
			EHtml::ajaxSubmitButton('', app()->createUrl('task/update'), array('success'=>ajaxSuccess(
			'routePage($("a.path").attr("href"))')), array('id'=>'saveTask'));
			$assign = 'el.closest(".desk").fadeOut(fast, function() {$(this).html("").show()})';
			$work = 'var url = $(this).closest(".task").find(".description a").attr("href"); '.
			'$.bbq.pushState(url.slice(url.indexOf("#")))';
			$struct = array('assign'=>$assign, 'work'=>$work);
			foreach ($struct as $id=>$js) {
				$ajax = $id == 'assign' ? array('success'=>'js:'.ajaxSuccess('updateSystem()')) : array();
				$outer = in_array($id, array('assign'));
				$url = $outer ? "#/$id?rid=" : false; $select = '';
				EHtml::ajaxLink('#content', $js, array('id'=>$id, 'class'=>'system'.ucwords($id)), 
				$url ? app()->createUrl($url) : '', $select, $ajax);
			}
			$this->widget('ext.multiselect.EMultiSelect');
			$this->widget('ext.ajaxUpload.EAjaxUpload');
			cs()->registerScript($this->getId(), $this->bind);
			QTip::tip('');
		}
	}
	
	/**
	 * Override the widget class to use relative view
	 */
	function getOwner() {
    	return $this;
	}
}
